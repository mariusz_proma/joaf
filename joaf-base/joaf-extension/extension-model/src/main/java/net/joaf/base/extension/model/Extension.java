/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.model;

import net.joaf.base.core.db.StringUidEntity;
import net.joaf.base.core.db.TransientMarker;
import net.joaf.base.extension.model.enums.EExtensionAction;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cyprian.sniegota on 03.03.14.
 */
public class Extension implements Serializable, StringUidEntity {

	private String uid;
	private String name;
	private String version;

	@TransientMarker
	private String codeVersion;
	@TransientMarker
	private List<EExtensionAction> actions = new LinkedList<>();

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCodeVersion() {
		return codeVersion;
	}

	public void setCodeVersion(String codeVersion) {
		this.codeVersion = codeVersion;
	}

	public List<EExtensionAction> getActions() {
		return actions;
	}
}
