/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.model;

import net.joaf.base.core.db.StringUidEntity;

import java.io.Serializable;

/**
 * Available widget
 */
public class WidgetConfiguration implements Serializable, StringUidEntity {
	private static final long serialVersionUID = 4434781021234528132L;
	private String uid;
	private String widgetBeanName;
	private EWidgetScope scope;
	private String widgetTargetFile;
	private String widgetTargetUrlRegexp;
	private String templatePosition;
	private Integer ordering;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getWidgetBeanName() {
		return widgetBeanName;
	}

	public void setWidgetBeanName(String widgetBeanName) {
		this.widgetBeanName = widgetBeanName;
	}

	public EWidgetScope getScope() {
		return scope;
	}

	public void setScope(EWidgetScope scope) {
		this.scope = scope;
	}

	public String getWidgetTargetFile() {
		return widgetTargetFile;
	}

	public void setWidgetTargetFile(String widgetTargetFile) {
		this.widgetTargetFile = widgetTargetFile;
	}

	public String getWidgetTargetUrlRegexp() {
		return widgetTargetUrlRegexp;
	}

	public void setWidgetTargetUrlRegexp(String widgetTargetUrlRegexp) {
		this.widgetTargetUrlRegexp = widgetTargetUrlRegexp;
	}

	public Integer getOrdering() {
		return ordering;
	}

	public void setOrdering(Integer ordering) {
		this.ordering = ordering;
	}

	public String getTemplatePosition() {
		return templatePosition;
	}

	public void setTemplatePosition(String templatePosition) {
		this.templatePosition = templatePosition;
	}

	public enum EWidgetScope {
		TEMPLATE, EXTENSION
	}
}
