/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.repository.cassandra;

import com.datastax.driver.core.Row;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.base.extension.model.WidgetConfiguration;
import net.joaf.base.extension.repository.api.WidgetRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cyprian.Sniegota on 12.03.14.
 */
@Repository
public class WidgetRepositoryCassandra extends AbstractCrudRepositoryCassandra<WidgetConfiguration> implements WidgetRepository, RepositoryCassandra {
	@Override
	public List<WidgetConfiguration> findAll() {
		List<WidgetConfiguration> retList = new ArrayList<>();
		WidgetConfiguration widgetSpec;
/*
		widgetSpec = new WidgetConfiguration();
        widgetSpec.setOrdering(0);
        widgetSpec.setScope(WidgetConfiguration.EWidgetScope.EXTENSION);
        widgetSpec.setUid("111");
		widgetSpec.setWidgetBeanName("infaktClientSyncWidget");
		widgetSpec.setWidgetTargetFile("/freemarker/");
		widgetSpec.setTemplatePosition("none");
		retList.add(widgetSpec);
*/

		widgetSpec = new WidgetConfiguration();
		widgetSpec.setOrdering(0);
		widgetSpec.setScope(WidgetConfiguration.EWidgetScope.TEMPLATE);
		widgetSpec.setUid("11");
		widgetSpec.setWidgetBeanName("subjectCardSelectorWidget");
		widgetSpec.setWidgetTargetFile("/freemarker/");
		widgetSpec.setTemplatePosition("widgets_top");
		retList.add(widgetSpec);

		widgetSpec = new WidgetConfiguration();
		widgetSpec.setOrdering(1);
		widgetSpec.setScope(WidgetConfiguration.EWidgetScope.TEMPLATE);
		widgetSpec.setUid("12");
		widgetSpec.setWidgetBeanName("userProfileWidget");
		widgetSpec.setWidgetTargetFile("/freemarker/");
		widgetSpec.setTemplatePosition("widgets_top");
		retList.add(widgetSpec);

		widgetSpec = new WidgetConfiguration();
		widgetSpec.setOrdering(0);
		widgetSpec.setScope(WidgetConfiguration.EWidgetScope.TEMPLATE);
		widgetSpec.setUid("21");
		widgetSpec.setWidgetBeanName("mainMenuWidget");
		widgetSpec.setWidgetTargetFile("/freemarker/");
		widgetSpec.setTemplatePosition("widgets_left");
		retList.add(widgetSpec);

		return retList;
	}

	@Override
	public String collectionName() {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public Class<WidgetConfiguration> getEntityClass() {
		return WidgetConfiguration.class;
	}

	@Override
	public void store(WidgetConfiguration element) throws JoafDatabaseException {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public void insert(WidgetConfiguration element) throws JoafDatabaseException {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	protected WidgetConfiguration bindRow(Row row) {
		throw new IllegalStateException("Not implemented");
	}
}
