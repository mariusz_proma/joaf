/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.repository.mysql;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.repository.api.ExtensionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author Cyprian Śniegota <cyprian.sniegota@asseco.pl>
 * @since 2015-01-31
 */
@Repository
public class ExtensionRepositoryMysql extends AbstractRepositoryMysql<Extension> implements ExtensionRepository {

	private static final String TABLE_NAME = "extension";
	private static final RowMapper<Extension> ROW_MAPPER = new ExtensionRowMapper();

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Integer findCountExtensionByName(String name) {
		return null;
	}

	@Override
	public void insertByValue(String name, String version) {

	}

	@Override
	public void remove(String uid) throws JoafDatabaseException {

	}

	@Override
	public void insert(Extension element) throws JoafDatabaseException {
		String query = "INSERT INTO extension (uid, name, version) VALUES (?,?,?)";
		jdbcTemplate.update(query, element.getUid(), element.getName(), element.getVersion());
	}

	@Override
	public void store(Extension element) throws JoafDatabaseException {

	}

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<Extension> getRowMapper() {
		return ROW_MAPPER;
	}

	@Override
	public String collectionName() {
		return this.getTableName();
	}

	@Override
	public Class<Extension> getEntityClass() {
		return null;
	}

	@Override
	public Extension findByName(String extensionName) throws JoafDatabaseException {
		String query = "SELECT * FROM extension WHERE name = ?";
		List<Extension> result = jdbcTemplate.query(query, new Object[] { extensionName }, this.getRowMapper());
		if (result.isEmpty()) {
			return null;
		} else {
			return result.iterator().next();
		}
	}

	@Override
	public boolean tableExists() {
		return false;
	}

	@Override
	public String prepareId() {
		return null;
	}

	@Override
	public void runCommands(List<String> commands, String type) {
		if ("mysql".equals(type)) {
			for (String command : commands) {
				jdbcTemplate.execute(command);
			}
		}
	}

	@Override
	public void updateVersion(String name, String number) throws JoafDatabaseException {
		Extension byName = this.findByName(name);
		if (byName == null) {
			Extension extension = new Extension();
			extension.setUid(UUID.randomUUID().toString());
			extension.setName(name);
			extension.setVersion(number);
			this.insert(extension);
		} else {
			String query = "UPDATE extension SET version = ? WHERE uid = ?";
			jdbcTemplate.update(query, number, byName.getUid());
		}
	}

	@Override
	public boolean isInstalled() {
		List<Map<String, Object>> maps = jdbcTemplate.queryForList("SHOW TABLES LIKE 'extension'");
		return !maps.isEmpty();
	}

	private static class ExtensionRowMapper implements RowMapper<Extension> {
		@Override
		public Extension mapRow(ResultSet resultSet, int i) throws SQLException {
			Extension extension = new Extension();
			extension.setUid(resultSet.getString("uid"));
			extension.setName(resultSet.getString("name"));
			extension.setVersion(resultSet.getString("version"));
			return extension;
		}
	}
}
