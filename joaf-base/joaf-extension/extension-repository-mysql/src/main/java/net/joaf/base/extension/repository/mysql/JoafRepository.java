/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.repository.mysql;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.base.extension.model.WidgetConfiguration;
import net.joaf.base.extension.repository.api.WidgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cyprian Śniegota <cyprian.sniegota@asseco.pl>
 * @since 2015-01-31
 */
@Repository
public class JoafRepository extends AbstractRepositoryMysql<WidgetConfiguration> implements WidgetRepository {

	private static final RowMapper<WidgetConfiguration> ROW_MAPPER = new WidgetRowMapper();
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	protected String getTableName() {
		return "widgetConfiguration";
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<WidgetConfiguration> getRowMapper() {
		return ROW_MAPPER;
	}

	@Deprecated
	public List<WidgetConfiguration> findAll_Old() {
		List<WidgetConfiguration> retList = new ArrayList<>();
		WidgetConfiguration widgetSpec;

/*
		widgetSpec = new WidgetSpecyfication();
        widgetSpec.setOrdering(0);
        widgetSpec.setScope(WidgetSpecyfication.EWidgetScope.TEMPLATE);
        widgetSpec.setUid("11");
        widgetSpec.setWidgetBeanName("subjectCardSelectorWidget");
        widgetSpec.setWidgetTargetFile("/freemarker/");
        widgetSpec.setTemplatePosition("widgets_top");
        retList.add(widgetSpec);

        widgetSpec = new WidgetSpecyfication();
        widgetSpec.setOrdering(1);
        widgetSpec.setScope(WidgetSpecyfication.EWidgetScope.TEMPLATE);
        widgetSpec.setUid("12");
        widgetSpec.setWidgetBeanName("userProfileWidget");
        widgetSpec.setWidgetTargetFile("/freemarker/");
        widgetSpec.setTemplatePosition("widgets_top");
        retList.add(widgetSpec);
*/

		widgetSpec = new WidgetConfiguration();
		widgetSpec.setOrdering(0);
		widgetSpec.setScope(WidgetConfiguration.EWidgetScope.TEMPLATE);
		widgetSpec.setUid("21");
		widgetSpec.setWidgetBeanName("mainMenuWidget");
		widgetSpec.setWidgetTargetFile("/freemarker/");
		widgetSpec.setTemplatePosition("widgets_left");
		retList.add(widgetSpec);

		return retList;
	}

	@Override
	public String collectionName() {
		return getTableName();
	}

	@Override
	public Class<WidgetConfiguration> getEntityClass() {
		return WidgetConfiguration.class;
	}

	@Override
	public void store(WidgetConfiguration element) throws JoafDatabaseException {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public void insert(WidgetConfiguration element) throws JoafDatabaseException {
		throw new IllegalStateException("Not implemented");
	}

	private static class WidgetRowMapper implements RowMapper<WidgetConfiguration> {

		@Override
		public WidgetConfiguration mapRow(ResultSet resultSet, int i) throws SQLException {
			WidgetConfiguration widgetConfiguration = new WidgetConfiguration();
			widgetConfiguration.setUid(resultSet.getString("uid"));
			widgetConfiguration.setOrdering(resultSet.getInt("ordering"));
			widgetConfiguration.setScope(WidgetConfiguration.EWidgetScope.valueOf(resultSet.getString("scope")));
			widgetConfiguration.setTemplatePosition(resultSet.getString("templatePosition"));
			widgetConfiguration.setWidgetBeanName(resultSet.getString("widgetBeanName"));
			widgetConfiguration.setWidgetTargetFile(resultSet.getString("widgetTargetFile"));
			widgetConfiguration.setWidgetTargetUrlRegexp(resultSet.getString("widgetTargetUrlRegexp"));
			return widgetConfiguration;
		}
	}

}
