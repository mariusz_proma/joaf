/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.repository.mongo;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mongo.RepositoryMongo;
import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.repository.api.ExtensionRepository;

import java.util.List;

/**
 * Created by cyprian on 13.03.14.
 */
public class ExtensionRepositoryMongo implements ExtensionRepository, RepositoryMongo {
	@Override
	public Integer findCountExtensionByName(String name) {
		return null;
	}

	@Override
	public void insertByValue(String name, String version) {

	}

	@Override
	public void remove(String uid) throws JoafDatabaseException {

	}

	@Override
	public void insert(Extension element) throws JoafDatabaseException {

	}

	@Override
	public void store(Extension element) throws JoafDatabaseException {

	}

	@Override
	public List<Extension> findAll() throws JoafDatabaseException {
		return null;
	}

	@Override
	public Extension findOne(String uid) throws JoafDatabaseException {
		return null;
	}

	@Override
	public String collectionName() {
		return null;
	}

	@Override
	public Class<Extension> getEntityClass() {
		return null;
	}

	@Override
	public Extension findByName(String extensionName) throws JoafDatabaseException {
		return null;
	}

	@Override
	public boolean tableExists() {
		return true;
	}

	@Override
	public void runCommands(List<String> commands, String type) {

	}

	@Override
	public void updateVersion(String name, String number) {

	}

	@Override
	public boolean isInstalled() {
		return false;
	}
}
