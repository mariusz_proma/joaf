<#import "/templates/base.ftl" as page />
<@page.layout "basiccustomer.list.name">


<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        ${action_buttons!}
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <table class="table table-bordered admin-media ">
            <thead>
            <tr>
                <th>
                    <input type='checkbox' value='check1'/>
                </th>
                <th>Bean</th>
                <th>Scope</th>
                <th>Control</th>
            </tr>
            </thead>
            <tbody>

                <#list elements as element>
                <tr>
                    <td>
                        <input type='checkbox' value='check1'/>
                    </td>
                    <td>${element.widgetBeanName}</td>
                    <td>${element.scope}</td>
                    <td>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>