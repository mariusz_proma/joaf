/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.web.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.base.extension.ExtensionExtension;
import net.joaf.base.extension.commands.InstallExtensionCommand;
import net.joaf.base.extension.commands.UpdateExtensionCommand;
import net.joaf.base.extension.commands.handlers.InstallExtensionCommandHanlder;
import net.joaf.base.extension.commands.handlers.UpdateExtensionCommandHanlder;
import net.joaf.base.extension.dataproviders.ExtensionDataProvider;
import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.model.enums.EExtensionAction;
import net.joaf.base.extension.services.ExtensionService;
import net.joaf.base.extension.web.helpers.ExtensionActionHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 14.03.14.
 */
@Controller
@RequestMapping(ExtensionExtension.EXTENSION_ADMIN_BASE_PATH)
public class ExtensionAdminController {

	@Autowired
	private ExtensionService service;

	@Autowired
	private CommandGateway commandGateway;

	@Autowired
	private ExtensionDataProvider extensionDataProvider;

	private WrapperWDTO<Extension> createWrapper(Extension extension) {
		return new WrapperWDTO<>(extension);
	}

	@RequestMapping
	public String list(HttpSession session, Model model) {
		try {
			List<Extension> retList = extensionDataProvider.findAll();
			List<WrapperWDTO<Extension>> displayList = retList.stream().map(this::createWrapper).collect(Collectors.toList());
			displayList.forEach((x) -> {
				List<EExtensionAction> actions = x.getData().getActions();
				for (EExtensionAction action : actions) {
					x.getWebActions().add(ExtensionActionHelper.createActionDetails(action, x.getData().getName()));
				}
			});
			model.addAttribute("elements", displayList);
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		return getViewTemplate("/list");
	}

	@RequestMapping("/install/{name}")
	public String install(@PathVariable(value = "name") String name, RedirectAttributes redirectAttributes) {
		try {
			commandGateway
					.validateAndSend(new BeanPropertyBindingResult(name, "command"), new InstallExtensionCommand(name), InstallExtensionCommandHanlder.class);
		} catch (JoafException e) {
			WebFlashUtil.addError(redirectAttributes, "joaf.error.title", "joaf.error.body", e.getLocalizedMessage());
			return getRedirect("");
		}
		WebFlashUtil.addError(redirectAttributes, "pl.joaf.main.extension.message.installed.title", "pl.joaf.main.extension.message.installed.body", name);
		return getRedirect("");
	}

	@RequestMapping("/update/{name}")
	public String update(@PathVariable(value = "name") String name, RedirectAttributes redirectAttributes) {
		try {
			commandGateway
					.validateAndSend(new BeanPropertyBindingResult(name, "command"), new UpdateExtensionCommand(name), UpdateExtensionCommandHanlder.class);
		} catch (JoafException e) {
			WebFlashUtil.addError(redirectAttributes, "joaf.error.title", "joaf.error.body", e.getLocalizedMessage());
			return getRedirect("");
		}
		WebFlashUtil.addError(redirectAttributes, "pl.joaf.main.extension.message.updated.title", "pl.joaf.main.extension.message.updated.body", name);
		return getRedirect("");
	}

	private String getRedirect(String subPath) {
		return "redirect:" + ExtensionExtension.EXTENSION_ADMIN_BASE_PATH + "" + subPath;
	}

	private String getViewTemplate(String subPath) {
		return "freemarker" + ExtensionExtension.EXTENSION_ADMIN_BASE_PATH + StringUtils.trimToEmpty(subPath);
	}

}
