/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.helpers;

import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.model.extensioninfo.ExtensionMetadata;
import net.joaf.base.extension.model.extensioninfo.Version;
import net.joaf.base.extension.model.extensioninfo.Versions;
import net.joaf.base.extension.repository.api.ExtensionRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author Cyprian.Sniegota <cyprian.sniegota@asseco.pl>
 * @since 2015-02-02
 */
public class ExtensionHelperTest {

	public static final String EXTENSION_1_NAME = "ext1";
	public static final String EXTENSION_2_NAME = "ext2";
	private static final String VERSION_1 = "1.0.0";
	private static final String VERSION_2 = "1.0.2";
	@Mock
	private ExtensionRepository repository;

	@InjectMocks
	private ExtensionHelper objectUnderTest;

	@BeforeMethod
	public void setUp() throws Exception {
		objectUnderTest = new ExtensionHelper();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCompareVersions() throws Exception {
		//given
		//when
		int i = objectUnderTest.compareVersions(VERSION_1, VERSION_2);
		//then
		assertEquals(i, -1);
	}

	@Test
	public void testFindExtension() throws Exception {
		//given
		Extension extension1 = createExtension1();
		when(repository.findByName(EXTENSION_1_NAME)).thenReturn(extension1);
		//when
		Extension ext1 = objectUnderTest.findExtension(EXTENSION_1_NAME);
		//then
		assertNotNull(ext1);
		assertEquals(ext1, extension1);
	}

	@Test
	public void testReadExtensionMetadata() throws Exception {
		//given
		String xml = "<joaf:extensionmetadata xmlns:joaf=\"http://joaf.net/base/extension/extensionmetadata\">\n"
				+ "    <name>ext1</name>\n"
				+ "    <ordering>1</ordering>\n"
				+ "    <versions>\n"
				+ "        <version>\n"
				+ "            <number>1.0.0</number>\n"
				+ "            <updates>\n"
				+ "                <update type=\"dbschema\" dbtype=\"mysql\">\n"
				+ "                    <file>/updates/extension-schema-1.0.0.mysql</file>\n"
				+ "                </update>\n"
				+ "            </updates>\n"
				+ "        </version>\n"
				+ "    </versions>\n"
				+ "</joaf:extensionmetadata>";
		InputStream is = new ByteArrayInputStream(xml.getBytes());
		//when
		ExtensionMetadata extensionMetadata = objectUnderTest.readExtensionMetadata(is);
		//then
		assertNotNull(extensionMetadata);
		assertEquals(extensionMetadata.getName(), EXTENSION_1_NAME);
	}

	@Test
	public void testExecuteUpdate() throws Exception {
		//given
		List<String> commands = new ArrayList<>();
		String type = "cassandra";
		//when
		objectUnderTest.executeUpdate(commands, type);
		//then
		verify(repository, times(1)).runCommands(commands, type);
	}

	@Test
	public void testUpdateVersion() throws Exception {
		//given
		//when
		objectUnderTest.updateVersion(EXTENSION_1_NAME, VERSION_2);
		//then
		verify(repository, times(1)).updateVersion(EXTENSION_1_NAME, VERSION_2);
	}

	@Test
	public void testFindMaxVersion() throws Exception {
		//given
		ExtensionMetadata extensionMetadata = prepareExtensionMetadata();
		//when
		Optional<Version> maxVersion = objectUnderTest.findMaxVersion(extensionMetadata);
		//then
		assertTrue(maxVersion.isPresent());
		assertEquals(maxVersion.get().getNumber(), VERSION_2);
	}

	private ExtensionMetadata prepareExtensionMetadata() {
		ExtensionMetadata extensionMetadata = new ExtensionMetadata();
		extensionMetadata.setVersions(new Versions());
		Version version1 = new Version();
		version1.setNumber(VERSION_1);
		extensionMetadata.getVersions().getVersion().add(version1);
		Version version2 = new Version();
		version2.setNumber(VERSION_2);
		extensionMetadata.getVersions().getVersion().add(version2);
		return extensionMetadata;
	}

	private Extension createExtension1() {
		Extension extension = new Extension();
		extension.setName(EXTENSION_1_NAME);
		extension.setVersion(VERSION_1);
		extension.setUid("");
		extension.setCodeVersion(VERSION_2);
		return extension;
	}
}