/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.commands.handlers;

import net.joaf.base.extension.commands.InstallExtensionCommand;
import net.joaf.base.extension.services.ExtensionService;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DirectFieldBindingResult;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class InstallExtensionCommandHanlderTest {

	public static final String STRING = "STR";
	@Mock
	private ExtensionService extensionService;

	@InjectMocks
	private InstallExtensionCommandHanlder objectUnderTest;

	@BeforeTest
	public void setUp() throws Exception {
		objectUnderTest = new InstallExtensionCommandHanlder();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testValidatePositive() throws Exception {
		//given
		InstallExtensionCommand command = new InstallExtensionCommand(STRING);
		//when
		BindingResult bindingResult = objectUnderTest.validate(new DirectFieldBindingResult(command, command.getClass().getName()), command);
		//then
		Assert.assertFalse(bindingResult.hasErrors());
	}

	@Test
	public void testValidateNegative() throws Exception {
		//given
		InstallExtensionCommand command = new InstallExtensionCommand("");
		//when
		BindingResult bindingResult = objectUnderTest.validate(new DirectFieldBindingResult(command, command.getClass().getName()), command);
		//then
		Assert.assertTrue(bindingResult.hasErrors());
	}

	@Test
	public void testExecute() throws Exception {
		//given
		InstallExtensionCommand command = new InstallExtensionCommand(STRING);
		//when
		Object result = objectUnderTest.execute(command);
		//then
		Assert.assertNull(result);
		verify(extensionService, times(1)).install(STRING);
	}
}