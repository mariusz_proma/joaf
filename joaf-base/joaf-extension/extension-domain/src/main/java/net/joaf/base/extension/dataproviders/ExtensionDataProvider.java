/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.dataproviders;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.extension.JoafExtension;
import net.joaf.base.extension.helpers.ExtensionHelper;
import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.model.enums.EExtensionAction;
import net.joaf.base.extension.model.extensioninfo.Version;
import net.joaf.base.extension.repository.api.ExtensionRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by cyprian on 01.02.15.
 */
@Service
public class ExtensionDataProvider {

	@Autowired
	private ExtensionRepository repository;

	@Autowired
	private List<JoafExtension> extensions = Arrays.asList();

	@Autowired
	private ExtensionHelper extensionHelper;

	public List<Extension> findAll() throws JoafDatabaseException {
		List<Extension> all = repository.findAll();
		List<Extension> extra = new ArrayList<>();
		for (JoafExtension extension : extensions) {
			Optional<Extension> dbVersion = all.stream().filter(x -> x.getName().equals(extension.getName())).findFirst();
			if (dbVersion.isPresent()) {
				Extension dbExtension = dbVersion.get();
				if (extension.getExtensionMetadata() != null) {
					Optional<Version> maxVersion = extensionHelper.findMaxVersion(extension.getExtensionMetadata());
					dbExtension.setCodeVersion(maxVersion.isPresent() ? maxVersion.get().getNumber() : "unknown");
				}
			} else {
				Extension newExtension = new Extension();
				newExtension.setName(extension.getName());
				Optional<Version> maxVersion = extensionHelper.findMaxVersion(extension.getExtensionMetadata());
				newExtension.setCodeVersion(maxVersion.isPresent() ? maxVersion.get().getNumber() : "unknown");
				newExtension.setVersion("");
				extra.add(newExtension);
			}
		}
		all.addAll(extra);
		all.stream().forEachOrdered((x) -> {
			if (!x.getName().equals("unknown")) {
				if (StringUtils.isEmpty(x.getVersion())) {
					x.getActions().add(EExtensionAction.INSTALL);
				} else if (!StringUtils.isEmpty(x.getCodeVersion()) && !x.getCodeVersion().equals(x.getVersion())) {
					x.getActions().add(EExtensionAction.UPDATE);
				}
			}
		});
		return all;
	}

}
