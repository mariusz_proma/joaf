/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.extension.helpers.ExtensionHelper;
import net.joaf.base.extension.model.Extension;
import net.joaf.base.extension.model.extensioninfo.ExtensionMetadata;
import net.joaf.base.extension.model.extensioninfo.Update;
import net.joaf.base.extension.model.extensioninfo.Version;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 27.01.14.
 */
@Component
public abstract class AbstractExtension implements JoafExtension {

	private static final Logger logger = LoggerFactory.getLogger(AbstractExtension.class);
	@Autowired
	protected ExtensionHelper extensionHelper;
	protected ExtensionMetadata extensionMetadata = null;

	@Override
	public String getName() {
		return extensionMetadata != null ? extensionMetadata.getName() : "unknown";
	}

	public abstract String getExtensionMetadataFile();

	@PostConstruct
	public void initialize() throws JoafException, JAXBException, IOException, XMLStreamException {
		try {
			logger.debug(this.getClass().getName() + " initialize");
			if (getExtensionMetadataFile() != null) {
				extensionMetadata = extensionHelper.readExtensionMetadata(this.getClass().getResourceAsStream(getExtensionMetadataFile()));
			}
		} catch (IOException | JAXBException | XMLStreamException e) {
			throw new JoafException("Error loading extension [" + this.getClass().getName() + "]", e);
		}
	}

	@Override
	public void install() throws JoafDatabaseException {
		Extension extension = extensionHelper.findExtension(this.getName());
		installAgainstVersion(extension == null ? null : extension.getVersion());
	}

	protected void installAgainstVersion(String versionNumber) throws JoafDatabaseException {
		if (extensionMetadata == null) {
			logger.error("extension metadata is null [{}]", this.getClass());
			return;
		}
		List<Version> versions = new ArrayList<>(extensionMetadata.getVersions().getVersion());
		Collections.sort(versions, new Comparator<Version>() {
			@Override
			public int compare(Version o1, Version o2) {
				return extensionHelper.compareVersions(o1.getNumber(), o2.getNumber());
			}
		});
		versions = versions.stream().filter(x -> versionNumber == null || extensionHelper.compareVersions(x.getNumber(), versionNumber) > 0)
				.collect(Collectors.toList());
		for (Version version : versions) {
			if (version.getUpdates() != null) {
				for (Update update : version.getUpdates().getUpdate()) {
					if (update.getType().equals("dbschema") || update.getType().equals("dbdata")) {
						installDbFile(this.getClass().getResourceAsStream(update.getFile()), update.getDbtype());
					} else {
						// TODO: run class
					}
				}
			}
		}
		Optional<Version> max = extensionHelper.findMaxVersion(extensionMetadata);
		if (max.isPresent()) {
			extensionHelper.updateVersion(extensionMetadata.getName(), max.get().getNumber());
		}

	}

	private void installDbFile(InputStream resourceAsStream, String dbtype) {
		Pattern endCommandPattern = Pattern.compile(".*;");
		InputStreamReader inputStreamReader = new InputStreamReader(resourceAsStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		System.out.println("++++++ " + this.getName() + " ++++++");
		String line;
		List<String> commands = new ArrayList<>();
		StringBuilder currentCommand = new StringBuilder();
		try {
			while ((line = bufferedReader.readLine()) != null) {
				if (StringUtils.isEmpty(StringUtils.trimToEmpty(line)) || StringUtils.trimToEmpty(line).startsWith("--")) {
					continue;
				}
				currentCommand.append(line);
				Matcher matcher = endCommandPattern.matcher(line);
				if (matcher.find()) {
					commands.add(currentCommand.toString());
					currentCommand = new StringBuilder();
				}
			}
			if (currentCommand.length() != 0) {
				commands.add(currentCommand.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(commands);
		System.out.println(dbtype + " " + commands.size());
		extensionHelper.executeUpdate(commands, dbtype);
	}

	@Override
	public ExtensionMetadata getExtensionMetadata() {
		return extensionMetadata;
	}
}
