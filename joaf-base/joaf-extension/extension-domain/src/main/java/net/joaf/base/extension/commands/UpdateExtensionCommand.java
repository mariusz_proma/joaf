/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.BasicCommand;
import net.joaf.base.extension.commands.handlers.UpdateExtensionCommandHanlder;

/**
 * Created by cyprian on 01.02.15.
 */
@CommandDto(handlerClass = UpdateExtensionCommandHanlder.class)
public class UpdateExtensionCommand extends BasicCommand implements Command {
	String name;

	public UpdateExtensionCommand(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}