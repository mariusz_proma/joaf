/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension;

import net.joaf.base.extension.exceptions.WidgetException;
import net.joaf.base.extension.model.WidgetMAV;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by Cyprian.Sniegota on 12.03.14.
 */
public interface Widget {

	public boolean availableForUserAndSubject(String username, String subjectId);

	public WidgetMAV process(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws WidgetException;
}
