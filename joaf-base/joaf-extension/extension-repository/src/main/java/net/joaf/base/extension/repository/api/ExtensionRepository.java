/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.extension.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.extension.model.Extension;

import java.util.List;

/**
 * Created by cyprian.sniegota on 28.02.14.
 */
public interface ExtensionRepository extends NoSqlRepository, CrudRepository<Extension> {

	Integer findCountExtensionByName(String name);

	void insertByValue(String name, String version);

	List<Extension> findAll() throws JoafDatabaseException;

	Extension findByName(String extensionName) throws JoafDatabaseException;

	boolean tableExists();

	void runCommands(List<String> commands, String type);

	void updateVersion(String name, String number) throws JoafDatabaseException;

	boolean isInstalled();
}
