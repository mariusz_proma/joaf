/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.cqrs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Component for posting events and executing event handlers.
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
@Component
public class EventPublisher {

	@Autowired
	private ConfigurableListableBeanFactory beanFactory;

	private Map<Class<? extends Event>, List<EventHandler>> handlersMap = new HashMap<>();

	public void post(Event event) {
		List<EventHandler> eventHandlers = findHandlers(event.getClass());
		for (EventHandler eventHandler : eventHandlers) {
			eventHandler.execute(event);
		}

	}

	private List<EventHandler> findHandlers(Class<? extends Event> eventClass) {
		if (!handlersMap.containsKey(eventClass)) {
			handlersMap.put(eventClass, new ArrayList<>());
		}
		return handlersMap.get(eventClass);
	}

	public void register(Class<? extends EventHandler> eventHandlerClass, Class<? extends Event> eventClass) {
		EventHandler bean = beanFactory.getBean(eventHandlerClass);
		if (!handlersMap.containsKey(eventClass)) {
			handlersMap.put(eventClass, new ArrayList<>());
		}
		handlersMap.get(eventClass).add(bean);
	}
}
