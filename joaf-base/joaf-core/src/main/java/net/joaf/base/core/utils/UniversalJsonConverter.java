/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.DBObject;

/**
 * Generic JSON converter
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
public class UniversalJsonConverter<T> {

	public T objectFromString(String string, Class<T> tClass) {
		Gson gson = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
		//        Gson gson = new Gson();
		return gson.fromJson(string, tClass);
	}

	public T objectFromDBObject(DBObject object, Class<T> tClass) {
		return this.objectFromString(object.toString(), tClass);
	}

	public String stringFromObject(T obj) {
		Gson gson = new Gson();
		return gson.toJson(obj);
	}

	public DBObject dbObjectFromObject(T obj) {

		//        DBObject object = null;
		//        try {
		//            object = JongoUtils.getDbObject(obj);
		//        } catch (IOException e) {
		//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		//        }
		//        return object;
		Gson gson = new Gson();
		Object o = com.mongodb.util.JSON.parse(this.stringFromObject(obj));
		return (DBObject) o;
	}
}