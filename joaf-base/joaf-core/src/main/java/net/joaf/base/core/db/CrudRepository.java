/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.db;

import net.joaf.base.core.error.JoafDatabaseException;

import java.util.List;

/**
 * Default CRUD repository interface
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
public interface CrudRepository<T extends StringUidEntity> {

	void remove(String uid) throws JoafDatabaseException;

	void insert(T element) throws JoafDatabaseException;

	void store(T element) throws JoafDatabaseException;

	List<T> findAll() throws JoafDatabaseException;

	T findOne(String uid) throws JoafDatabaseException;

	String collectionName();

	Class<T> getEntityClass();
}
