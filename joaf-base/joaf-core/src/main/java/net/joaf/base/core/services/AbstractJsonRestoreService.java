/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.StringUidEntity;
import net.joaf.base.core.error.JoafDatabaseException;

import java.util.ArrayList;
import java.util.List;
//TODO: should be moved to backup-restore module

/**
 * Default restore from json behaviour
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
public abstract class AbstractJsonRestoreService<T extends StringUidEntity> implements JsonRestoreService {

	@Override
	public void restoreAll(String json, String collectionName, boolean removeOld) throws JoafDatabaseException {
		CrudRepository<T> repository = getRepository();
		if (removeOld) {
			List<T> all = repository.findAll();
			for (T element : all) {
				repository.remove(element.getUid());
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			List<T> importData = mapper
					.readValue(json.getBytes("UTF-8"), TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, repository.getEntityClass()));
			System.out.print("--- ");
			System.out.println(importData);
			if (importData == null) {
				return;
			}
			for (T element : importData) {
				T dbElement = repository.findOne(element.getUid());
				if (dbElement == null) {
					repository.insert(element);
				}
				repository.store(element);
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getCollectionName() {
		return getRepository().collectionName();
	}

	protected abstract CrudRepository<T> getRepository();
}
