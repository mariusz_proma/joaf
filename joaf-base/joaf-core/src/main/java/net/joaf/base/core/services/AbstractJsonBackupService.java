/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.StringUidEntity;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.ModuleBackupContainer;

import java.util.List;
//TODO: should be moved to backup-restore module

/**
 * Default backup to json behavior
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
public abstract class AbstractJsonBackupService<T extends StringUidEntity> implements JsonBackupService {

	@Override
	public ModuleBackupContainer backupAll() throws JoafDatabaseException {
		List<T> all = getRepository().findAll();
		ObjectMapper mapper = new ObjectMapper();
		try {
			return new ModuleBackupContainer(getModuleName(), getCollectionName(), mapper.writeValueAsString(all));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getCollectionName() {
		return getRepository().collectionName();
	}

	protected abstract CrudRepository<T> getRepository();
}
