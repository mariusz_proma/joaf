/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.session;

import net.joaf.base.subjectcard.model.SubjectCardData;

import javax.servlet.http.HttpSession;

/**
 * Context stored in web session.
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
public class SessionContext {
	private static final String JOAF_SESSION_CONTEXT = "net.joaf.base.SessionContext";

	/**
	 * keep in session
	 */
	private String subjectCardDataId;
	/**
	 * load every request
	 */
	private SubjectCardData subjectCardData;

	public static SessionContext loadFromSession(HttpSession session) {
		return (SessionContext) session.getAttribute(SessionContext.JOAF_SESSION_CONTEXT);
	}

	public void saveToSession(HttpSession session) {
		session.setAttribute(SessionContext.JOAF_SESSION_CONTEXT, this);
	}

	public String getSubjectCardDataId() {
		return subjectCardDataId;
	}

	public void setSubjectCardDataId(String subjectCardDataId) {
		this.subjectCardDataId = subjectCardDataId;
	}

	public SubjectCardData getSubjectCardData() {
		return subjectCardData;
	}

	public void setSubjectCardData(SubjectCardData subjectCardData) {
		this.subjectCardData = subjectCardData;
	}
}
