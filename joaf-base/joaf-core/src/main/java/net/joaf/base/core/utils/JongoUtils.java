/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import com.mongodb.BasicDBObject;
import com.mongodb.DBEncoder;
import com.mongodb.DBObject;
import com.mongodb.DefaultDBEncoder;
import com.mongodb.LazyWriteableDBObject;
import org.bson.LazyBSONCallback;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.OutputBuffer;
import org.jongo.marshall.jackson.bson4jackson.MongoBsonFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Utils for JSON conversions
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
public class JongoUtils {

	private final static ObjectMapper mapper = new ObjectMapper(MongoBsonFactory.createFactory());

	static {
		mapper.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(
				JsonAutoDetect.Visibility.ANY));
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static DBObject getDbObject(Object o) throws IOException {
		ObjectWriter writer = mapper.writer();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		writer.writeValue(baos, o);
		DBObject dbo = new LazyWriteableDBObject(baos.toByteArray(), new LazyBSONCallback());
		//turn it into a proper DBObject otherwise it can't be edited.
		DBObject result = new BasicDBObject();
		result.putAll(dbo);
		return result;
	}

	public static <T> T getPojo(DBObject o, Class<T> clazz) throws IOException {
		ObjectReader reader = mapper.reader(clazz);

		DBEncoder dbEncoder = DefaultDBEncoder.FACTORY.create();
		OutputBuffer buffer = new BasicOutputBuffer();
		dbEncoder.writeObject(buffer, o);

		T pojo = reader.readValue(buffer.toByteArray());

		return pojo;
	}

	public static <T> T getPojo(String json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, clazz);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> List<T> getPojos(String json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();

		CollectionType collectionType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
		try {
			return mapper.readValue(json, collectionType);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	public static <T> Set<T> getPojoSet(String json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();

		CollectionType collectionType = mapper.getTypeFactory().constructCollectionType(Set.class, clazz);
		try {
			return mapper.readValue(json, collectionType);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new HashSet<>();
	}

	public static <E, T> Map<E, T> getPojoMap(String json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();

		MapType collectionType = mapper.getTypeFactory().constructMapType(Map.class, Integer.class, clazz);
		try {
			return mapper.readValue(json, collectionType);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new HashMap<>();
	}
}