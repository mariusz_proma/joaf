/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.cqrs.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.cqrs.commands.BasicCommand;
import org.springframework.validation.BindingResult;

/**
 * Sample Command Handler Component. Required if no other is defined in application scope.
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
@CommandHandlerComponent
public class BasicCommandHandler implements CommandHandler<BasicCommand, Object> {
	@Override
	public BindingResult validate(BindingResult originalBindingResult, BasicCommand command) {
		return originalBindingResult;
	}

	@Override
	public Object execute(BasicCommand command) {
		return null;
	}
}
