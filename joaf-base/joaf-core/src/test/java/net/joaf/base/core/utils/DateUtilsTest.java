/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.core.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Map;

public class DateUtilsTest {

	@Before
	public void setUp() {

	}

	@Test
	public void testGenerateWeekdaysForMonthSeptember() throws Exception {
		//given
		LocalDate parsed = LocalDate.parse("2014-09-05");
		//when
		Map<Integer, Integer> result = DateUtils.generateWeekdaysForMonth(parsed);
		//then
		Assert.assertNotNull(result);
		Assert.assertEquals(30, result.keySet().size());
		Assert.assertEquals((Integer) 5, result.get(5));
		Assert.assertEquals((Integer) 7, result.get(7));
		Assert.assertEquals((Integer) 1, result.get(8));
	}

	@Test
	public void testGenerateWeekdaysForMonthNovember() throws Exception {
		//given
		LocalDate parsed = LocalDate.parse("2014-10-05");
		//when
		Map<Integer, Integer> result = DateUtils.generateWeekdaysForMonth(parsed);
		//then
		Assert.assertNotNull(result);
		Assert.assertEquals(31, result.keySet().size());
		Assert.assertEquals((Integer) 7, result.get(5));
		Assert.assertEquals((Integer) 2, result.get(7));
		Assert.assertEquals((Integer) 3, result.get(8));
	}
}