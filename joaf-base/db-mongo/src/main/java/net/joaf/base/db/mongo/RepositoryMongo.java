/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.db.mongo;

import net.joaf.base.core.db.ERepositoryType;
import net.joaf.base.core.db.JoafRepository;
import net.joaf.base.core.db.NoSqlRepository;
import org.bson.types.ObjectId;

/**
 * Main interface for all MongoDB repositories.
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
public interface RepositoryMongo extends NoSqlRepository, JoafRepository {
	@Override
	public default String prepareId() {
		return ObjectId.get().toHexString();
	}

	@Override
	public default ERepositoryType getType() {
		return ERepositoryType.MONGO;
	}
}
