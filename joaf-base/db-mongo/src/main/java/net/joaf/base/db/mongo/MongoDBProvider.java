/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.db.mongo;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import net.joaf.base.core.error.JoafDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.UnknownHostException;

/**
 * MongoDB database connection provider.
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
@Component
public class MongoDBProvider {

	private static final Logger log = LoggerFactory.getLogger(MongoDBProvider.class);
	private static final String DATABASE_NAME = "joaf";
	private DB database;
	private MongoClient mongoClient;

	@PostConstruct
	public void initialize() throws JoafDatabaseException {
		log.info("Creating database handler [" + DATABASE_NAME + "];");
		log.error("MongoDB Disabled in code");
		if (mongoClient == null) {
			return;
		}
		try {
			mongoClient = new MongoClient(); //new MongoClientURI(mongoURIString)
			database = mongoClient.getDB(DATABASE_NAME);
		} catch (UnknownHostException e) {
			String message = "Error creating database handler; database=[" + DATABASE_NAME + "]";
			log.error(message);
			throw new JoafDatabaseException(message, e);
		}
	}

	public DB getMongoDB() throws JoafDatabaseException {
		if (database == null) {
			throw new JoafDatabaseException("Error getting database handler.");
		}
		return database;
	}

	@PreDestroy
	public void close() {
		log.info("mongo close");
		if (mongoClient != null) {
			mongoClient.close();
		}
	}
}
