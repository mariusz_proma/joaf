/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.db.mysql;

import net.joaf.base.core.db.ERepositoryType;
import net.joaf.base.core.db.JoafRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cyprian on 01.02.15.
 */
public abstract class AbstractRepositoryMysql<T extends Serializable> implements JoafRepository {

	public static final String TABLE_NAME = "{tableName}";
	public static final String COLUMN_NAME = "{columnName}";
	private static final String FIND_ALL = "SELECT * FROM {tableName}";
	private static final String FIND_ONE = "SELECT * FROM {tableName} WHERE uid = ?";
	private static final String FIND_BY_FIELD = "SELECT * FROM {tableName} WHERE {columnName} = ?";
	private static final String REMOVE_ONE = "DELETE FROM {tableName} WHERE uid = ?";

	protected abstract String getTableName();

	protected abstract JdbcTemplate getJdbcTemplate();

	protected abstract RowMapper<T> getRowMapper();

	private String tableQuery(String query) {
		return query.replace(TABLE_NAME, getTableName());
	}

	private String fieldQuery(String query, String fieldName) {
		return query.replace(COLUMN_NAME, fieldName);
	}

	public List<T> findAll() throws JoafDatabaseException {
		return getJdbcTemplate().query(tableQuery(FIND_ALL), getRowMapper());
	}

	public T findOne(String uid) throws JoafDatabaseException {
		return getJdbcTemplate().queryForObject(tableQuery(FIND_ONE), new Object[] { uid }, getRowMapper());
	}

	public void remove(String uid) throws JoafDatabaseException {
		getJdbcTemplate().update(tableQuery(REMOVE_ONE), uid);
	}

	protected T findOneByField(String fieldName, String fieldValue) {
		List<T> ts = getJdbcTemplate().query(fieldQuery(tableQuery(FIND_BY_FIELD), fieldName), new Object[] { fieldValue }, this.getRowMapper());
		return ts.isEmpty() ? null : ts.iterator().next();
	}

	protected List<T> findListByField(String fieldName, String fieldValue) {
		return getJdbcTemplate().query(fieldQuery(tableQuery(FIND_BY_FIELD), fieldName), new Object[] { fieldValue }, this.getRowMapper());
	}

	@Override
	public ERepositoryType getType() {
		return ERepositoryType.MYSQL;
	}
}
