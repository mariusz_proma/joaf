/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.db.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import net.joaf.base.core.config.ExternalConfigurationHelper;
import net.joaf.base.core.error.JoafException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

/**
 * Provides session for connection to Cassandra database.
 *
 * @author Cyprian.Sniegota
 * @since 2014-12-30
 */
@Component
public class CassandraSessionProvider {

	private static final Logger logger = LoggerFactory.getLogger(CassandraSessionProvider.class);
	@Autowired
	private ExternalConfigurationHelper externalConfigurationHelper;
	private String schemaName = "";
	private Session session;

	public Session connect() {
		try {
			if (session == null) {
				synchronized (logger) {
					if (session == null) {
						session = this.createSession();
					}
				}
			}
		} catch (JoafException ignored) {
		}
		return session;
	}

	public void recreateSession(String contactPoint, String schemaName) throws JoafException {
		synchronized (logger) {
			destroy();
			this.session = this.createSessionWithCreditials(contactPoint, schemaName);
		}
	}

	private Session createSession() throws JoafException {
		String contactPoint = externalConfigurationHelper.getProperty("db.cassandra.node");
		String schemaName = externalConfigurationHelper.getProperty("db.cassandra.schema");
		return createSessionWithCreditials(contactPoint, schemaName);
	}

	private Session createSessionWithCreditials(String contactPoint, String schemaName) throws JoafException {
		Cluster cluster = Cluster.builder()
				.addContactPoint(contactPoint)
						// .withSSL() // uncomment if using client to node encryption
				.build();
		Metadata metadata = cluster.getMetadata();
		logger.info("Connected to cluster: " + metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			logger.info(String.format("Datatacenter: %s; Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack()));
		}
		Session session = cluster.connect();
		ResultSet describeKeyspace = session.execute("SELECT * from system.schema_keyspaces where keyspace_name = '" + schemaName + "';");
		if (describeKeyspace.all().size() == 0) {
			//			this.baseInstallation(session, schemaName);
			logger.error("no schema!");
		} else {
			logger.debug("switch to use keyspace [" + schemaName + "]");
			session.execute("USE " + schemaName + ";");
			this.schemaName = schemaName;
		}
		return session;
	}

	private void baseInstallation(Session session, String schemaName) {
		logger.info("creating keyspace [" + schemaName + "]");
		session.execute("CREATE KEYSPACE " + schemaName + " WITH replication = {'class':'SimpleStrategy', 'replication_factor':3};");
		logger.info("use keyspace [" + schemaName + "]");
		session.execute("USE " + schemaName + ";");
		logger.info("creating extension table");
		session.execute("CREATE TABLE extension ("
				+ "            uid uuid PRIMARY KEY,"
				+ "            name text,"
				+ "            version text,"
				+ "            );");
		logger.info("creating extension table index (name)");
		session.execute("CREATE INDEX ON extension (name);");
	}

	@PreDestroy
	public void destroy() {
		if (this.session != null) {
			//            synchronized (logger) {
			//                if (this.session != null) {
			logger.info("closing session");
			this.session.close();
			logger.info("closing cluster");
			this.session.getCluster().close();
			//                }
			//            }
		}
	}

	public String getSchemaName() {
		return schemaName;
	}
}
