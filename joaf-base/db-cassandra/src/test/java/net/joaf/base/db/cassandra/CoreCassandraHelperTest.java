/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.base.db.cassandra;

import com.datastax.driver.core.Session;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CoreCassandraHelperTest {

	@InjectMocks
	private CoreCassandraHelper objectUnderTest;

	@Mock
	private CassandraSessionProvider cassandraSessionProvider;

	@Mock
	private Session session;

	@BeforeMethod
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldRunExecuteBatch() throws Exception {
		//given
		String command = "create table;";
		List<String> data = Arrays.asList(command);
		when(cassandraSessionProvider.connect()).thenReturn(session);
		when(session.execute(anyString())).thenReturn(null);
		//when
		objectUnderTest.executeBatch(data);
		//then
		verify(cassandraSessionProvider).connect();
		verify(session, atLeastOnce()).execute(command);
	}
}