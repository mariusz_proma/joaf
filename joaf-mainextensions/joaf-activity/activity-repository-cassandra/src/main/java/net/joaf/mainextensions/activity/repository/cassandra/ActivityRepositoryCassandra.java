/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.activity.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.mainextensions.activity.model.ActivityEntry;
import net.joaf.mainextensions.activity.model.enums.ActivityType;
import org.springframework.stereotype.Repository;
import net.joaf.mainextensions.activity.repository.api.ActivityRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 19.05.14.
 */
@Repository
public class ActivityRepositoryCassandra extends AbstractCrudRepositoryCassandra<ActivityEntry> implements ActivityRepository {

	@Override
	public void insert(ActivityEntry activityEntry) {
		Session session = sessionProvider.connect();
		UUID subjectUid = activityEntry.getSubjectUid() != null ? UUID.fromString(activityEntry.getSubjectUid()) : null;
		UUID objectUid = activityEntry.getObjectUid() != null ? UUID.fromString(activityEntry.getObjectUid()) : null;
		UUID historyObjectUid = activityEntry.getHistoryObjectUid() != null ? UUID.fromString(activityEntry.getHistoryObjectUid()) : null;
		UUID userUid = activityEntry.getUserUid() != null ? UUID.fromString(activityEntry.getUserUid()) : null;
/*
		String sql = "INSERT INTO activity (uid, userUid, username, subjectUid, subjectShortName, objectUid, objectName" +
                ", created, bodyKey) VALUES (" +
                "?, ?, ?, ?, ?, ?, ?, ?, ?)";
        session.execute(sql, UUID.fromString(activityEntry.getUid()), userUid, activityEntry.getUsername()
                , subjectUid, activityEntry.getSubjectShortName(), objectUid
                , activityEntry.getObjectName(), activityEntry.getCreated()
                , activityEntry.getBodyKey());*/

		String sql = "INSERT INTO activity (uid, userUid, username, subjectUid, subjectShortName, objectUid, objectName" +
				", created, bodyKey, bodyParams, historyObjectUid, type, metadata) VALUES (" +
				"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		session.execute(sql, UUID.fromString(activityEntry.getUid()), userUid, activityEntry.getUsername()
				, subjectUid, activityEntry.getSubjectShortName(), objectUid
				, activityEntry.getObjectName(), activityEntry.getCreated()
				, activityEntry.getBodyKey(), new ArrayList<>(activityEntry.getBodyParams()), historyObjectUid
				, activityEntry.getType().toString(), activityEntry.getMetadata());
	}

	@Override
	public List<ActivityEntry> findForUser(String userUid, Date date) {
		Session session = sessionProvider.connect();
		String sql = "SELECT * FROM activity WHERE created > ? AND userUid = ?";
		ResultSet resultSet = session.execute(sql, date, userUid);
		List<Row> all = resultSet.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public List<ActivityEntry> findForSubject(String subjectUid, Date date) {
		Session session = sessionProvider.connect();
		String sql = "SELECT * FROM activity WHERE created > ? AND subjectUid = ?";
		ResultSet resultSet = session.execute(sql, date, subjectUid);
		List<Row> all = resultSet.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public List<ActivityEntry> findForObject(String objectUid, Date date) {
		Session session = sessionProvider.connect();
		String sql = "SELECT * FROM activity WHERE created > ? AND objectUid = ?";
		ResultSet resultSet = session.execute(sql, date, objectUid);
		List<Row> all = resultSet.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public List<ActivityEntry> findAll() {
		Session session = sessionProvider.connect();
		String sql = "SELECT * FROM activity";
		ResultSet resultSet = session.execute(sql);
		List<Row> all = resultSet.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public String collectionName() {
		return "activity";
	}

	@Override
	public Class<ActivityEntry> getEntityClass() {
		return ActivityEntry.class;
	}

	@Override
	public void store(ActivityEntry element) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		UUID subjectUid = element.getSubjectUid() != null ? UUID.fromString(element.getSubjectUid()) : null;
		UUID objectUid = element.getObjectUid() != null ? UUID.fromString(element.getObjectUid()) : null;
		UUID historyObjectUid = element.getHistoryObjectUid() != null ? UUID.fromString(element.getHistoryObjectUid()) : null;
		UUID userUid = element.getUserUid() != null ? UUID.fromString(element.getUserUid()) : null;
/*
		String sql = "INSERT INTO activity (uid, userUid, username, subjectUid, subjectShortName, objectUid, objectName" +
                ", created, bodyKey) VALUES (" +
                "?, ?, ?, ?, ?, ?, ?, ?, ?)";
        session.execute(sql, UUID.fromString(activityEntry.getUid()), userUid, activityEntry.getUsername()
                , subjectUid, activityEntry.getSubjectShortName(), objectUid
                , activityEntry.getObjectName(), activityEntry.getCreated()
                , activityEntry.getBodyKey());*/

		String sql = "UPDATE activity SET userUid = ?, username = ?, subjectUid = ?, subjectShortName = ?, objectUid = ?, objectName = ?" +
				", created = ?, bodyKey = ?, bodyParams = ?, historyObjectUid = ?, type = ?, metadata = ? WHERE uid = ?";
		session.execute(sql, userUid, element.getUsername()
				, subjectUid, element.getSubjectShortName(), objectUid
				, element.getObjectName(), element.getCreated()
				, element.getBodyKey(), new ArrayList<>(element.getBodyParams()), historyObjectUid
				, element.getType().toString(), element.getMetadata(), UUID.fromString(element.getUid()));
	}

	protected ActivityEntry bindRow(Row row) {
		ActivityEntry activityEntry = new ActivityEntry();
		UUID tmpUid;
		activityEntry.setUid(row.getUUID("uid").toString());
		activityEntry.setUsername(row.getString("username"));
		activityEntry.setSubjectShortName(row.getString("subjectShortName"));
		activityEntry.setObjectName(row.getString("objectName"));
		activityEntry.setCreated(row.getDate("created"));
		activityEntry.setBodyKey(row.getString("bodyKey"));
		activityEntry.setBodyParams(row.getList("bodyParams", String.class));
		activityEntry.setType(ActivityType.valueOf(row.getString("type")));
		activityEntry.setMetadata(row.getString("metadata"));
		tmpUid = row.getUUID("userUid");
		if (tmpUid != null) {
			activityEntry.setUserUid(tmpUid.toString());
		}
		tmpUid = row.getUUID("subjectUid");
		if (tmpUid != null) {
			activityEntry.setSubjectUid(tmpUid.toString());
		}
		tmpUid = row.getUUID("objectUid");
		if (tmpUid != null) {
			activityEntry.setObjectUid(tmpUid.toString());
		}
		tmpUid = row.getUUID("historyObjectUid");
		if (tmpUid != null) {
			activityEntry.setHistoryObjectUid(tmpUid.toString());
		}
		return activityEntry;
	}

	@Override
	public String prepareId() {
		return UUID.randomUUID().toString();
	}
}
