package net.joaf.mainextensions.activity.repository.mysql;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.activity.model.ActivityEntry;
import org.springframework.stereotype.Repository;
import net.joaf.mainextensions.activity.repository.api.ActivityRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by cyprian on 01.02.15.
 */
@Repository
public class ActivityRepositoryMysql implements ActivityRepository {
	@Override
	public void remove(String uid) throws JoafDatabaseException {

	}

	@Override
	public void insert(ActivityEntry activityEntry) {

	}

	@Override
	public void store(ActivityEntry element) throws JoafDatabaseException {

	}

	@Override
	public List<ActivityEntry> findForUser(String userUid, Date date) {
		return null;
	}

	@Override
	public List<ActivityEntry> findForSubject(String subjectUid, Date date) {
		return null;
	}

	@Override
	public List<ActivityEntry> findForObject(String objectUid, Date date) {
		return null;
	}

	@Override
	public List<ActivityEntry> findAll() {
		return null;
	}

	@Override
	public ActivityEntry findOne(String uid) throws JoafDatabaseException {
		return null;
	}

	@Override
	public String collectionName() {
		return null;
	}

	@Override
	public Class<ActivityEntry> getEntityClass() {
		return null;
	}

	@Override
	public String prepareId() {
		return UUID.randomUUID().toString();
	}
}
