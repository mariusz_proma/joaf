/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.content.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.content.model.Content;

import java.util.List;

/**
 * Created by cyprian.sniegota on 28.02.14.
 */
public interface ContentRepository extends NoSqlRepository, CrudRepository<Content> {

	public Content getContentByBasicCriteria(String module, String name, String category) throws JoafDatabaseException;

	public List<Content> findAll() throws JoafDatabaseException;

	public Content findOne(String id) throws JoafDatabaseException;

	public void store(Content command) throws JoafDatabaseException;

	public void remove(String id) throws JoafDatabaseException;
}
