/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.content.helpers;

import net.joaf.base.core.error.JoafDatabaseException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import net.joaf.mainextensions.content.model.Content;
import net.joaf.mainextensions.content.repository.api.ContentRepository;

import java.util.Map;

/**
 * Created by cyprian on 01.01.14.
 */

@Component
public class ContentHelper {

	@Autowired
	private ContentRepository contentRepository;

	public Content getContent(String module, String name, String category) throws JoafDatabaseException {
		return contentRepository.getContentByBasicCriteria(module, name, category);
	}

	public Content getContentReplaced(Map<String, String> model, String module, String name, String category) throws JoafDatabaseException {
		Content content = this.getContent(module, name, category);
		if (content != null) {
			for (String key : model.keySet()) {
				String value = model.get(key);
				content.setTitle(StringUtils.replace(content.getTitle(), "{" + key + "}", value));
				content.setBody(StringUtils.replace(content.getBody(), "{" + key + "}", value));
			}
		}
		return content;
	}
}
