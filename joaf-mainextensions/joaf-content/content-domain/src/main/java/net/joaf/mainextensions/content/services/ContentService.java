/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.content.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.content.model.Content;
import net.joaf.mainextensions.content.repository.api.ContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cyprian
 * Date: 06.01.14
 * Time: 01:13
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ContentService {

	@Autowired
	private ContentRepository repository;

	public List<Content> findAll() throws JoafDatabaseException {
		return repository.findAll();
	}

	public Content findOne(String id) throws JoafDatabaseException {
		return repository.findOne(id);
	}

	public void save(Content command) throws JoafDatabaseException {
		repository.store(command);
	}

	public Content create(Content command) throws JoafDatabaseException {
		command.setUid(repository.prepareId());
		repository.store(command);
		return command;
	}

	public void remove(String id) throws JoafDatabaseException {
		repository.remove(id);
	}
}
