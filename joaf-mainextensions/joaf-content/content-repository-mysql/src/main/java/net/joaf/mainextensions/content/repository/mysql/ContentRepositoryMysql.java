package net.joaf.mainextensions.content.repository.mysql;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.content.model.Content;
import net.joaf.mainextensions.content.repository.api.ContentRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Cyprian Śniegota <cyprian.sniegota@asseco.pl>
 * @since 2015-02-24
 */
@Repository
public class ContentRepositoryMysql extends AbstractRepositoryMysql<Content> implements ContentRepository {
	@Override
	protected String getTableName() {
		return null;
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return null;
	}

	@Override
	protected RowMapper<Content> getRowMapper() {
		return null;
	}

	@Override
	public Content getContentByBasicCriteria(String module, String name, String category) throws JoafDatabaseException {
		return null;
	}

	@Override
	public void insert(Content element) throws JoafDatabaseException {

	}

	@Override
	public void store(Content command) throws JoafDatabaseException {

	}

	@Override
	public String collectionName() {
		return null;
	}

	@Override
	public Class<Content> getEntityClass() {
		return null;
	}

	@Override
	public String prepareId() {
		return null;
	}
}
