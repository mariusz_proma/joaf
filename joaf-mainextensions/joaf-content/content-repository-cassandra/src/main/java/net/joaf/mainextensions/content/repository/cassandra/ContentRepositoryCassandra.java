/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.content.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.mainextensions.content.model.Content;
import net.joaf.mainextensions.content.repository.api.ContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian.sniegota on 28.02.14.
 */
@Repository
public class ContentRepositoryCassandra extends AbstractCrudRepositoryCassandra<Content> implements ContentRepository, RepositoryCassandra {

	@Autowired
	private CassandraSessionProvider sessionProvider;

	protected Content bindRow(Row row) {
		Content obj = new Content();
		obj.setUid(row.getUUID("uid").toString());
		obj.setBody(row.getString("body"));
		obj.setCategory(row.getString("category"));
		obj.setModule(row.getString("module"));
		obj.setName(row.getString("name"));
		obj.setTitle(row.getString("title"));
		return obj;
	}

	@Override
	public Content getContentByBasicCriteria(String module, String name, String category) {
		Session session = sessionProvider.connect();
		ResultSet rs = session.execute("select * from content where module = ? AND name = ? AND category = ? ALLOW FILTERING", module, name, category);
		List<Row> all = rs.all();
		if (all.size() > 0) {
			return this.bindRow(all.get(0));
		} else {
			return null;
		}
	}

	@Override
	public List<Content> findAll() throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		ResultSet rs = session.execute("select * from content");
		List<Row> all = rs.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public Content findOne(String id) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		ResultSet rs = session.execute("select * from content where uid = ?", UUID.fromString(id));
		List<Row> all = rs.all();
		if (all.size() > 0) {
			return this.bindRow(all.get(0));
		} else {
			return null;
		}
	}

	@Override
	public String collectionName() {
		return "content";
	}

	@Override
	public Class<Content> getEntityClass() {
		return Content.class;
	}

	@Override
	public void store(Content command) throws JoafDatabaseException {
		String sql = "UPDATE content SET body = ?, category = ?, module = ?, name = ?, title = ? WHERE uid = ?";
		Session session = sessionProvider.connect();
		session.execute(sql,
				command.getBody(), command.getCategory(), command.getModule(), command.getName(), command.getTitle(), UUID.fromString(command.getUid()));

	}

	@Override
	public void insert(Content element) throws JoafDatabaseException {
		String sql = "INSERT INTO content (body, category, module, name, title, uid) VALUES(?, ?, ?, ?, ? ,?)";
		Session session = sessionProvider.connect();
		session.execute(sql,
				element.getBody(), element.getCategory(), element.getModule(), element.getName(), element.getTitle(), UUID.fromString(element.getUid()));

	}

	@Override
	public void remove(String id) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String sql = "DELETE FROM content WHERE uid = ?";
		session.execute(sql, UUID.fromString(id));
	}

	@Override
	public String prepareId() {
		return UUID.randomUUID().toString();
	}
}
