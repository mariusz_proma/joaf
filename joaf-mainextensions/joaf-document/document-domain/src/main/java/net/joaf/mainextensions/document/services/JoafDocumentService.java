/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.document.services;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;
import net.joaf.mainextensions.document.model.DocumentMetadata;
import net.joaf.mainextensions.document.model.JoafDocument;
import net.joaf.mainextensions.document.repository.api.JoafDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by cyprian on 26.04.14.
 */
@Service
public class JoafDocumentService {

	@Autowired
	private JoafDocumentRepository repository;

	public JoafDocument create() {
		JoafDocument document = new JoafDocument();
		document.setUid(repository.prepareId());
		return document;
	}

	public void insert(JoafDocument document) {
		repository.insert(document);
	}

	public JoafDocument findByUid(String uid) {
		return repository.getDocument(uid);
	}

	public void render(InputStream inputFile, JoafDocument joafDocument, DocumentMetadata documentBody) {
		try {
/*            DocumentMetadata documentMetadata = new DocumentMetadata();
			documentMetadata.getFieldAsList().addAll(Arrays.asList("dayEntries.lp", "dayEntries.date", "dayEntries.source", "dayEntries.destination"
                    , "dayEntries.description", "dayEntries.length", "dayEntries.unitPrice", "dayEntries.totalPrice"));
            documentMetadata.getContext().put("command", command);
            documentMetadata.getContext().put("dayEntries", tripEntries);
            documentMetadata.getContext().put("owner", userCard);
            documentMetadata.getContext().put("vehicleCard", vehicleCard);

            return documentMetadata;*/

			joafDocument.setName("document.pdf");
			//create pdf
			IXDocReport report = XDocReportRegistry.getRegistry().loadReport(inputFile, TemplateEngineKind.Freemarker);
			// 2) Create fields metadata to manage lazy loop (#forech velocity)
			// for table row.
			FieldsMetadata metadata = new FieldsMetadata();
			for (String s : documentBody.getFieldAsList()) {
				metadata.addFieldAsList(s);
			}
			report.setFieldsMetadata(metadata);

			// 3) Create context Java model
			IContext context = null;
			context = report.createContext();
			// Register model
			context.putMap(documentBody.getContext());
			// X) Set PDF as format converter
			Options options = Options.getTo(ConverterTypeTo.PDF);
			// 4) Generate report by merging Java model with the Docx
			//            String tmpDir = "/home/cyprian/itmp/";//System.getProperty("java.io.tmpdir");
			ByteArrayOutputStream outputPdf = new ByteArrayOutputStream();
			//            OutputStream out = new FileOutputStream( new File(tmpDir +"ivoice.odt" ));
			//            OutputStream outPdf = new FileOutputStream(new File(tmpDir+"ivoice.pdf"));
			report.convert(context, options, outputPdf);
			//            report.convert(context, options, outPdf);
			//            report.process( context, out );
			joafDocument.setData(outputPdf.toByteArray());
			//			repository.updateInvoiceDocument(invoice.getSubjectId(), invoice.getUid(), joafDocument.getUid());
		} catch (XDocReportException | IOException e) {
			e.printStackTrace();
		}
	}
}
