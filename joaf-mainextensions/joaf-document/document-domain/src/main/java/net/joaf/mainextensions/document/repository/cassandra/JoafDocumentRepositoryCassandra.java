/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.document.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.mainextensions.document.model.JoafDocument;
import net.joaf.mainextensions.document.repository.api.JoafDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 26.04.14.
 */
@Repository
public class JoafDocumentRepositoryCassandra extends AbstractCrudRepositoryCassandra<JoafDocument> implements JoafDocumentRepository {

	@Autowired
	private CassandraSessionProvider sessionProvider;

	@Override
	public List<JoafDocument> findAll() throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String sql = "SELECT * FROM " + collectionName();
		ResultSet execute = session.execute(sql);
		List<Row> all = execute.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public String collectionName() {
		return "zsiodocument";
	}

	@Override
	public Class<JoafDocument> getEntityClass() {
		return JoafDocument.class;
	}

	@Override
	public void store(JoafDocument element) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String insertSql = "UPDATE " + collectionName() + " SET name = ?, data = ? WHERE uid = ? ";
		session.execute(insertSql, element.getName(), ByteBuffer.wrap(element.getData()), UUID.fromString(element.getUid()));
	}

	@Override
	public JoafDocument getDocument(String uid) {
		Session session = sessionProvider.connect();
		String sql = "SELECT * FROM " + collectionName() + " WHERE uid = ?";
		ResultSet execute = session.execute(sql, UUID.fromString(uid));
		List<Row> all = execute.all();

		return all.stream().map(this::bindRow).reduce((x, y) -> x).orElse(null);
	}

	@Override
	public void insert(JoafDocument document) {
		Session session = sessionProvider.connect();
		String insertSql = "INSERT INTO " + collectionName() + " (uid, name, data) VALUES (?, ?, ?) ";
		session.execute(insertSql, UUID.fromString(document.getUid()), document.getName(), ByteBuffer.wrap(document.getData()));
	}

	protected JoafDocument bindRow(Row row) {
		JoafDocument document = new JoafDocument();
		document.setName(row.getString("name"));
		document.setUid(row.getUUID("uid").toString());
		document.setData(row.getBytes("data").array());
		return document;
	}

	@Override
	public String prepareId() {
		return String.valueOf(UUID.randomUUID());
	}
}
