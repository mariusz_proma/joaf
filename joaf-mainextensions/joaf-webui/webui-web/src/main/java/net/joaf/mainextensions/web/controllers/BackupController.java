/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.web.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.services.FullJsonBackupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

//TODO: remove subject company references
//import net.joaf.base.core.net.joaf.mainextension.subjectcompany.model.SubjectCard;
//import net.joaf.base.core.net.joaf.mainextension.subjectcompany.services.SubjectCardService;
//import net.joaf.base.core.net.joaf.mainextension.subjectcompany.web.helpers.SubjectCardControllerHelper;

/**
 * Created by cyprian on 03.11.14.
 */
@Controller
public class BackupController {

	@Autowired
	private FullJsonBackupService backupService;

	//	@Autowired
	//	private SubjectCardService subjectCardService;

	@RequestMapping("/administration/backup")
	public void backup(HttpServletResponse response) {

		try {
			// get your file as InputStream
			ByteArrayOutputStream outputStream = backupService.backupAll();
			// copy it to response's OutputStream
			outputStream.writeTo(response.getOutputStream());
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename=backupall.zip");
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/administration/backupsubject")
	public String backupSubject(HttpSession session, Model model) {
		//		try {
		//			List<SubjectCard> elements = subjectCardService.findAllWithUsers();
		//			List<WrapperWDTO<SubjectCard>> displayList = elements.stream().map(SubjectCardControllerHelper::createWrapper)
		//					.collect(Collectors.<WrapperWDTO<SubjectCard>>toList());
		//			model.addAttribute("elements", displayList);
		//			return "admin/backupsubjectlist";
		//		} catch (JoafDatabaseException e) {
		//			e.printStackTrace();
		//			return null;
		//		}
		return null;
	}
}
