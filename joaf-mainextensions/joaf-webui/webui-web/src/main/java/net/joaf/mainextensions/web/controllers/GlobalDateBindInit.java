/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.web.controllers;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.WebRequest;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Cyprian.Sniegota
 * Date: 04.11.13
 * Time: 14:10
 * To change this template use File | Settings | File Templates.
 */
@ControllerAdvice
public class GlobalDateBindInit {

	private static class DateEditor extends PropertyEditorSupport {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		@Override
		public void setAsText(String text) throws IllegalArgumentException {
			if (!StringUtils.hasText(text)) {
				// Treat empty String as null value.
				setValue(null);
			} else {
				// Use default valueOf methods for parsing text.
				TemporalAccessor parse = formatter.parse(text);
				LocalDate dateTime = LocalDate.from(parse);
				setValue(dateTime);
			}
		}

		@Override
		public void setValue(Object value) {
			if (value instanceof Date) {
				super.setValue(value);
			} else {
				super.setValue(value);
			}
		}

		/**
		 * Format the Number as String, using the specified NumberFormat.
		 */
		@Override
		public String getAsText() {
			Object value = getValue();
			if (value == null) {
				return "";
			}
			// Use toString method for rendering value.
			return formatter.format((LocalDate) value);
		}
	}

	@InitBinder
	public void registerCustomEditors(WebDataBinder binder, WebRequest request) {
		binder.registerCustomEditor(LocalDate.class, new DateEditor());
	}
}
