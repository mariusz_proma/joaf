<#import "/templates/base.ftl" as page />
<@page.layout "zsio.main.administration">
<div class="row">
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/bgtask.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.bgtask.modulename' /></h3>
                
                <p><@spring.message 'net.joaf.mainextensions.bgtask.modulename.subtitle' /></p>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/content.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.contents.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.contents.modulename.subtitle' /></p>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/configuration.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.configuration.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.configuration.modulename.subtitle' /></p>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/conditionalconfiguration.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.conditionalconfiguration.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.conditionalconfiguration.modulename.subtitle' /></p>

            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/menu.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.menu.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.menu.modulename.subtitle' /></p>

            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/import.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.import.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.import.modulename.subtitle' /></p>

            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/extension.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.extension.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.extension.modulename.subtitle' /></p>

            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/widgetspecyfication.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.widget.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.widget.modulename.subtitle' /></p>

            </a>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/user.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.user.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.user.modulename.subtitle' /></p>

            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/backup.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.backup.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.backup.modulename.subtitle' /></p>

            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/restore.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.restore.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.restore.modulename.subtitle' /></p>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="text-center page-menu-element">
            <a href="${rc.contextPath}/administration/backupsubject.html">
                <h3><i class="fa fa-bolt"></i></h3>

                <h3><@spring.message 'net.joaf.mainextensions.backupsubject.modulename' /></h3>

                <p><@spring.message 'net.joaf.mainextensions.backupsubject.modulename.subtitle' /></p>

            </a>
        </div>
    </div>
</div>
</@page.layout>