<#import "/templates/base.ftl" as page />
<#import "/freemarker/zsio.ftl" as zsio />
<@page.layout "database">
<div class="awidget full-width">
<#--    <div class="awidget-head">
<h3>
    </div>-->
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post">
            <@zsio.formInput 'command.node' 'class="form-control" maxlength="50"' 'text' 'Node' 'node'/>
            <@zsio.formInput 'command.schema' 'class="form-control" maxlength="20"' 'text' 'Schema' 'schema'/>
            <div class="form-group">
                <hr/>
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-primary"><@spring.message 'zsio.save' /></button>
                </div>
            </div>
        </form>
    </div>
</div>
</@page.layout>