<#import "/templates/base.ftl" as page />
<@page.layout "xxx">
<#--<@spring.bind "registerForm" />-->
<div class="awidget-head">
<#--${registerForm.email}-->
    <#--<@spring.showErrors "*", "errors" />-->
</div>
<div class="awidget-body">
    <!-- Page title -->
<#--    <div class="page-title text-center">
        <h2><@spring.message 'zsio.user.profile' /></h2>
        <hr />
    </div>-->
    Installed: ${installed}
    <!-- Page title -->
    <form class="form-horizontal" role="form" action="${rc.contextPath}/zsioinstall/doInstall.html" method="post">
        <div class="form-group">
            <label for="email" class="col-lg-3 control-label"><@spring.message 'zsio.email.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.email.label') />
                <@spring.formInput 'command.email' 'placeholder="${placeholder}" class="form-control" maxlength="100"' />
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
            <label for="password" class="col-lg-3 control-label"><@spring.message 'zsio.password.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.password.label') />
                <@spring.formInput 'command.password' 'placeholder="${placeholder}" class="form-control" maxlength="100"' />
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-10">
                <button type="submit" class="btn btn-info"><@spring.message 'zsio.save' /></button>
            </div>
        </div>
    </form>
</div>
</@page.layout>