/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.web.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.extension.ExtensionExtension;
import net.joaf.base.extension.JoafExtension;
import net.joaf.base.extension.repository.api.ExtensionRepository;
import net.joaf.base.extension.services.ExtensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.joaf.mainextensions.web.model.InstallForm;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.enums.EUserCardStatus;
import net.joaf.mainextensions.user.services.UserService;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * @author Cyprian Śniegota <cyprian.sniegota@asseco.pl>
 * @since 2015-02-21
 */
@Service
public class JoafInstallService {

	Pattern endCommandPattern = Pattern.compile(".*;");

	@Autowired
	private ExtensionRepository repository;

	@Autowired
	private UserService userService;

	@Autowired
	private ExtensionService extensionService;

	@Autowired
	private List<JoafExtension> extensions = Arrays.asList();

	public boolean checkInstalled() {
		return repository.isInstalled();
	}

	private ExtensionExtension findExtensionExtension() {
		Optional<JoafExtension> first = extensions.stream().filter(x -> x instanceof ExtensionExtension).findFirst();
		if (first.isPresent()) {
			return (ExtensionExtension) first.get();
		} else {
			return null;
		}
	}

	public void installWithCreditials(InstallForm command) {
		ExtensionExtension extensionExtension = findExtensionExtension();
		try {
			extensionExtension.fullInstall();
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}

		Collections.sort(extensions, new Comparator<JoafExtension>() {
			@Override
			public int compare(JoafExtension o1, JoafExtension o2) {
				if (o1.getExtensionMetadata() != null && o2.getExtensionMetadata() != null && o1.getExtensionMetadata().getOrdering() != null) {
					return o1.getExtensionMetadata().getOrdering().compareTo(o2.getExtensionMetadata().getOrdering());
				} else {
					return 0;
				}
			}
		});
		for (JoafExtension extension : this.extensions) {
			try {
				extension.install();
			} catch (JoafDatabaseException e) {
				e.printStackTrace();
			}
		}
		UserCard userCard = new UserCard();
		userCard.setActive(true);
		userCard.setDeleted(false);
		userCard.setUsername(command.getEmail());
		userCard.setEmail(command.getEmail());
		userCard.setPassword(command.getPassword());
		userCard.setFirstName("");
		userCard.setLastName("");
		HashSet<String> roles = new HashSet<>(Arrays.asList("ROLE_USER", "ROLE_ADMIN"));
		userCard.setEffectiveRoles(roles);
		userCard.setRoles(roles);
		userCard.setStatus(EUserCardStatus.ACTIVE);
/*
		Extension extension = new Extension();
        extension.setName("AppManager");
        extension.setVersion("1.0.0");
*/
		try {
			userService.insertUser(userCard);
			//            extensionService.insert(extension);
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
	}
}
