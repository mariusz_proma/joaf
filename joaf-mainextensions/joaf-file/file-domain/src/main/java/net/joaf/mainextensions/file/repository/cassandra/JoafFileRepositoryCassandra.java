/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.file.repository.cassandra;

import com.datastax.driver.core.Row;
import net.joaf.mainextensions.file.model.JoafFile;
import net.joaf.mainextensions.file.repository.api.JoafFileRepository;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;

import java.util.List;
import java.util.UUID;

/**
 * Created by cyprian on 26.04.14.
 */
public class JoafFileRepositoryCassandra extends AbstractCrudRepositoryCassandra<JoafFile> implements JoafFileRepository {
	@Override
	public List<JoafFile> findAll(String ownerUid) {
		return null;
	}

	@Override
	public JoafFile findOne(String uid) {
		return null;
	}

	@Override
	public String collectionName() {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public Class<JoafFile> getEntityClass() {
		return JoafFile.class;
	}

	@Override
	public void store(JoafFile joafFile) {

	}

	@Override
	public void insert(JoafFile joafFile) {

	}

	@Override
	protected JoafFile bindRow(Row row) {
		throw new IllegalStateException("Not implemented");
	}

	@Override
	public String prepareId() {
		return String.valueOf(UUID.randomUUID());
	}
}
