/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.confirmation.repository.api;

import net.joaf.mainextensions.confirmation.model.EEmailActivationStatus;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by cyprian on 14.05.14.
 */
public interface ConfirmationRepository extends NoSqlRepository, CrudRepository<EmailActivation> {

	void insert(EmailActivation emailActivation);

	EmailActivation findByKeyAndCode(String key, String code);

	void updateStatusAndConfirmationDate(String uid, EEmailActivationStatus status, Date confirmationDate);

	void updateStatus(String uid, EEmailActivationStatus status);

	List<EmailActivation> findByKey(String key);

	void updateMailData(String reference_uid, String json);

	List<EmailActivation> findAllActive();

	void deprecate(EmailActivation x);

	List<EmailActivation> findAll();
}
