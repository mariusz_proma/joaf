/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.confirmation.services;

import com.google.gson.Gson;
import net.joaf.mainextensions.confirmation.callbacks.EmptyConfirmationCallback;
import net.joaf.mainextensions.confirmation.interfaces.ConfirmationCallback;
import net.joaf.mainextensions.confirmation.model.EEmailActivationStatus;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.confirmation.model.dto.ConfirmationResultDto;
import net.joaf.mainextensions.confirmation.repository.api.ConfirmationRepository;
import net.joaf.base.core.utils.TokenUtil;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by cyprian on 14.05.14.
 */
@Service
public class ConfirmationService {

	@Autowired
	private ConfirmationRepository confirmationRepository;

	@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
	@Autowired(required = false)
	@Qualifier("confirmationCallback")
	private List<ConfirmationCallback> callbacks = new ArrayList<>();

	@Produce(uri = "direct:sendEmailMessage")
	private ProducerTemplate template;

	@Autowired
	private EmptyConfirmationCallback emptyConfirmationCallback;

	public EmailActivation create(String key, String oldKey, String callbackName) {
		EmailActivation emailActivation = new EmailActivation();
		emailActivation.setUid(confirmationRepository.prepareId());
		emailActivation.setCode(TokenUtil.generateToken());
		emailActivation.setRegisterDate(LocalDateTime.now());
		emailActivation.setEmail(key);
		emailActivation.setKey(key);
		emailActivation.setOriginalAccountEmail(oldKey);
		emailActivation.setConfirmed(false);
		emailActivation.setCallbackName(callbackName);
		emailActivation.setStatus(EEmailActivationStatus.READY);
		confirmationRepository.insert(emailActivation);
		return emailActivation;
	}

	public EmailActivation create(String key, String callbackName) {
		return create(key, key, callbackName);
	}

	public ConfirmationResultDto confirm(String key, String code, String remoteAddr) {
		if (StringUtils.isEmpty(key) || StringUtils.isEmpty(code)) {
			return null;
		}
		EmailActivation emailActivation = confirmationRepository.findByKeyAndCode(key, code);
		if (emailActivation == null) {
			return new ConfirmationResultDto(false, null, remoteAddr);
		}
		if (canConfirm(emailActivation)) {
			confirmationRepository.updateStatusAndConfirmationDate(emailActivation.getUid(), EEmailActivationStatus.ACTIVATED, new Date());
			ConfirmationResultDto confirmationResultDto = new ConfirmationResultDto(true, emailActivation, remoteAddr);
			confirmationResultDto = onConfirmSuccess(confirmationResultDto, emailActivation);
			return confirmationResultDto;
		}
		ConfirmationResultDto confirmationResultDto = new ConfirmationResultDto(false, emailActivation, remoteAddr);
		confirmationResultDto = onConfirmFailure(confirmationResultDto);
		return confirmationResultDto;
	}

	private ConfirmationResultDto onConfirmFailure(ConfirmationResultDto confirmationResultDto) {
		return confirmationResultDto;
	}

	private ConfirmationResultDto onConfirmSuccess(ConfirmationResultDto confirmationResultDto, EmailActivation emailActivation) {
		ConfirmationCallback callback = this.findCallback(confirmationResultDto.getEmailActivation().getCallbackName());
		return callback.callback(emailActivation, confirmationResultDto);
	}

	private ConfirmationCallback findCallback(String callbackName) {
		if (callbacks != null) {
			for (ConfirmationCallback confirmationCallback : callbacks) {
				if (confirmationCallback.getName().equals(callbackName)) {
					return confirmationCallback;
				}
			}
		}
		return emptyConfirmationCallback;
	}

	public boolean canConfirm(String key, String code) {
		EmailActivation emailActivation = confirmationRepository.findByKeyAndCode(key, code);
		return canConfirm(emailActivation);
	}

	public boolean canConfirm(EmailActivation emailActivation) {
		return emailActivation != null;
	}

	public String buildUrl(EmailActivation emailActivation) {
		//TODO: UriUtils spring-web
		try {
			return "/confirmation.html?key="
					+ URLEncoder.encode(emailActivation.getEmail(), "UTF-8")
					+ "&code=" + URLEncoder.encode(emailActivation.getCode(), "UTF-8");
		} catch (UnsupportedEncodingException ignored) {
		}
		return null;
	}

	public void resend(String email) {
		List<EmailActivation> availables = confirmationRepository.findByKey(email);
		for (EmailActivation emailActivation : availables) {
			Gson gson = new Gson();
			Map<String, Object> mailData = gson.fromJson(emailActivation.getMailData(), Map.class);
			template.sendBody(mailData);
		}
	}

	public void updateMailData(Map<String, Object> mailData) {
		Map<String, String> model = (Map<String, String>) mailData.get("model");
		if (model.containsKey("reference_uid")) {
			String reference_uid = model.get("reference_uid");
			Gson gson = new Gson();
			String json = gson.toJson(mailData);
			confirmationRepository.updateMailData(reference_uid, json);
		}

	}

	public void cleanupOldConfirmation() {
		List<EmailActivation> allActive = confirmationRepository.findAllActive();
		final Integer hoursToLive = 24;
		final LocalDateTime minDateTime = LocalDateTime.now().minusHours(hoursToLive);
		allActive.stream().filter((x) -> x.getRegisterDate().isBefore(minDateTime)).forEach(confirmationRepository::deprecate);
	}
}
