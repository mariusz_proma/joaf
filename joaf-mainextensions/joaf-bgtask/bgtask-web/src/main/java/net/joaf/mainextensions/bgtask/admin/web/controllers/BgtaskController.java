/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.bgtask.admin.web.controllers;

import com.sun.org.apache.xerces.internal.impl.PropertyManager;
import com.sun.org.apache.xerces.internal.impl.XMLStreamReaderImpl;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.formmodel.Form;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.mainextensions.bgtask.services.BgtaskService;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xml.sax.SAXException;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.ETriggerType;
import net.joaf.mainextensions.bgtask.services.JobDefinitionService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.validation.SchemaFactory;
import java.security.Principal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cyprian on 09.08.14.
 */

@Controller
@RequestMapping("/administration/bgtask")
public class BgtaskController {

	private static final String EXTENSION_BASE_PATH = "/administration/bgtask";

	@Autowired
	private BgtaskService service;

	@Autowired
	private JobDefinitionService jobDefinitionService;

	@RequestMapping
	public String list(HttpSession session, Model model) {
		try {
			List<Bgtask> all = service.findAll();
			model.addAttribute("command", all);
		} catch (JoafDatabaseException | SchedulerException e) {
			e.printStackTrace();
		}
		return "freemarker/bgtask/admin/list";
	}

	@RequestMapping("/create")
	public String create(HttpSession session, Model model) {
		Bgtask command = null;
		try {
			command = service.create();
		} catch (JoafDatabaseException e) {
			//            log.debug("error", e);
			return "/freemarker/bgtask/admin/list";
		}
		return "redirect:/administration/bgtask/edit/" + command.getUid() + "";
	}

	@RequestMapping("/edit/{id}")
	public String newItem(@PathVariable("id") String id, HttpSession session, Model model) {
		Bgtask command = null;
		try {
			command = service.findOne(id);
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		try {
			Form form = readForm();
			model.addAttribute("form", form);
			HashMap<String, Object> formModel = new HashMap<>();
			Map<String, String> jobDefinitionList = jobDefinitionService.findOptions();
			formModel.put("jobDefinitionList", jobDefinitionList);
			formModel.put("triggerTypeList", Arrays.asList(ETriggerType.values()));
			model.addAttribute("formModel", formModel);
		} catch (JAXBException | SAXException | XMLStreamException | JoafDatabaseException e) {
			e.printStackTrace();
		}
		model.addAttribute("command", command);

		return "/freemarker/bgtask/admin/edit";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String save(@PathVariable("id") String id, @Valid @ModelAttribute("command") Bgtask command, BindingResult result, Model model,
			Principal principal, HttpSession session, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return getViewTemplate("/edit");
		}
		try {
			//            if (true)
			//            throw new JoafDatabaseException("test exception");
			service.save(command);
			SessionContext sessionContext = SessionContext.loadFromSession(session);
			String subjectCardDataId = sessionContext.getSubjectCardDataId();
			if (subjectCardDataId == null) {
				sessionContext.setSubjectCardDataId(command.getUid());
			}
		} catch (JoafDatabaseException e) {
			//            result.addError(new FieldError("command","id","Error saving"));
			//            redirectAttributes.addFlashAttribute("error","basiccustomer.error.1");
			WebFlashUtil.addMessage(redirectAttributes, "joaf.formsaveerror.title", "joaf.subjectcard.formsaveerror.body");
			return getViewTemplate("/edit");
		}
		WebFlashUtil.addMessage(redirectAttributes, "joaf.formsavesuccess.title", "joaf.subjectcard.formsavesuccess.body");
		return getRedirect(".html");
	}

	private Form readForm() throws JAXBException, SAXException, XMLStreamException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Form.class);
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		PropertyManager propertyManager = new PropertyManager(PropertyManager.CONTEXT_READER);
		XMLStreamReader xmlStreamReader = new XMLStreamReaderImpl(getClass().getResourceAsStream("/forms/bgtask.xml"), propertyManager);
		return jaxbUnmarshaller.unmarshal(xmlStreamReader, Form.class).getValue();
	}

	@RequestMapping("/cancel/{id}")
	public String cancel(@PathVariable("id") String id, HttpSession session, Model model) throws JoafNoSubjectContextException {
		try {
			service.cancel(id);
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		return getRedirect(".html");
	}

	@RequestMapping("/schedule/{id}")
	public String schedule(@PathVariable("id") String id, HttpSession session, Model model, RedirectAttributes redirectAttributes)
			throws JoafNoSubjectContextException {
		try {
			service.schedule(id); //id
		} catch (ClassNotFoundException | SchedulerException | JoafException e) {
			WebFlashUtil.addError(redirectAttributes, "joaf.error.title", "joaf.error.unknown.withmessage.body", e.getLocalizedMessage());
			return getRedirect(".html");
		}
		return getRedirect(".html");
	}

	@RequestMapping("/unschedule/{id}")
	public String unschedule(@PathVariable("id") String id, HttpSession session, Model model) throws JoafNoSubjectContextException {
		try {
			service.unschedule(id); //id
		} catch (JoafDatabaseException | SchedulerException e) {
			e.printStackTrace();
		}
		return getRedirect(".html");
	}

	@RequestMapping("/remove/{id}")
	public String remove(@PathVariable("id") String id, HttpSession session, Model model) throws JoafNoSubjectContextException {
		try {
			service.remove(id); //id
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		return getRedirect(".html");
	}

	private String getRedirect(String subPath) {
		return "redirect:" + EXTENSION_BASE_PATH + "" + subPath;
	}

	private String getViewTemplate(String subPath) {
		return "freemarker" + EXTENSION_BASE_PATH + StringUtils.trimToEmpty(subPath);
	}

}
