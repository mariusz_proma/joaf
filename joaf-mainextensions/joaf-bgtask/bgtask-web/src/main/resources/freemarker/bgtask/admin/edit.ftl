<#import "/templates/base.ftl" as page />
<#import "/freemarker/zsio.ftl" as zsio />

<#--
 field types
-->


<@page.layout "Invoices">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        <#--<a href="/remoteconnections/edit/0.html" class="btn btn-primary">Create</a>-->
            <#--<a href="/invoice/syncInfakt.html" class="btn btn-info">Infakt sync</a>-->
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post"
              action="">

        <#--
            <#assign placeholder = springMacroRequestContext.getMessage('zsio.bgtask.jobModule') />
            <@zsio.formInput 'command.jobModule' 'class="form-control"' 'text' 'zsio.bgtask.jobModule' '${placeholder}'/>

            <#list .vars['zsio']?keys as prop>
                ${prop}
            </#list>
        -->
            <@zsio.renderForm form=form formModel=formModel />

            <@spring.formHiddenInput 'command.uid' '' />
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}/administration/bgtask/cancel/${command.uid}.html');$(this.form).submit();"
                    class="btn btn-default">Cancel
            </button>
        </form>
    </div>

</div>
</@page.layout>