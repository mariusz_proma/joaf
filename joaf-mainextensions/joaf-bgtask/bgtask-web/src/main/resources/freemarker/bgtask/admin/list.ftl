<#import "/templates/base.ftl" as page />
<@page.layout "xxx">


<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
            <a href="${rc.contextPath}/administration/bgtask/create.html" class="btn btn-primary">Create</a>
        </div>
        <div class="pull-right">
            <a href="${rc.contextPath}/administration.html" class="btn btn-primary"><i
                    class="fa fa-gear fa-fw"></i> <@spring.message 'zsio.back' /></a>
        </div>
    </div>
    <div class="awidget-body">
        <table class="table table-bordered admin-media ">
            <thead>
            <tr>
                <th>
                    <input type='checkbox' value='check1'/>
                </th>
                <th>Name</th>
                <th>jobClassName</th>
                <th>State</th>
                <th>Uid</th>
            </tr>
            </thead>
            <tbody>

                <#list command as element>
                <tr>
                    <td>
                        <input type='checkbox' value='check1'/>
                    </td>
                    <td>${element.name}</td>
                    <td>${element.jobClassName}</td>
                    <td>
                    ${element.scheduleState}
                        <#if element.active><#assign facolor='text-success' /><#else><#assign facolor='text-danger' /></#if>
                        <i class="fa fa-lightbulb-o ${facolor}"></i>
                    </td>
                    <td>${element.uid}</td>
                    <td>
                        <a href="${rc.contextPath}/administration/bgtask/edit/${element.uid}.html"
                           class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> </a>
                        <a href="${rc.contextPath}/administration/bgtask/remove/${element.uid}.html"
                           class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> </a>
                        <a href="${rc.contextPath}/administration/bgtask/schedule/${element.uid}.html"
                           class="btn btn-xs btn-danger"><i class="fa fa-bolt"></i> </a>
                        <a href="${rc.contextPath}/administration/bgtask/unschedule/${element.uid}.html"
                           class="btn btn-xs btn-danger"><i class="fa fa-pause"></i> </a>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>