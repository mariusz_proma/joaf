package net.joaf.mainextensions.bgtask.repository;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.JongoUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import net.joaf.mainextensions.bgtask.model.Bgtask;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by cyprian on 01.02.15.
 */
@Repository
public class BgtaskRepositoryMysql extends AbstractRepositoryMysql<Bgtask> implements BgtaskRepository {
	private static final BgtaskRowMapper ROW_MAPPER = new BgtaskRowMapper();
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	protected String getTableName() {
		return "bgTask";
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<Bgtask> getRowMapper() {
		return ROW_MAPPER;
	}

	@Override
	public void store(Bgtask command) throws JoafDatabaseException {

	}

	@Override
	public String collectionName() {
		return getTableName();
	}

	@Override
	public Class<Bgtask> getEntityClass() {
		return Bgtask.class;
	}

	@Override
	public void insert(Bgtask command) throws JoafDatabaseException {
		String sql = "insert into bgTask (uid, scheduleState, name, description, jobClassName, jobModule, triggerType, params) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			jdbcTemplate.update(sql, command.getUid(), command.getScheduleState().toString(), command.getName(), command.getDescription()
					, command.getJobClassName(), command.getJobModule(), command.getTriggerType().toString(), packMap(command.getParams()));
		} catch (Exception e) {
			throw new JoafDatabaseException("Error database", e);
		}
	}

	private Object packMap(Map<String, String> params) throws IOException {
		return JongoUtils.getDbObject(params).toString();
	}

	@Override
	public void updateScheduleState(String id, EBgtaskState active) {

	}

	private static class BgtaskRowMapper implements RowMapper<Bgtask> {

		@Override
		public Bgtask mapRow(ResultSet resultSet, int i) throws SQLException {
			Bgtask bgtask = new Bgtask();
			bgtask.setUid(resultSet.getString("uid"));
			return bgtask;
		}
	}
}
