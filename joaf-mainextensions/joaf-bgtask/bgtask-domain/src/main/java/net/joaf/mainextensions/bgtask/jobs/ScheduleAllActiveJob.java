/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.bgtask.jobs;

import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.services.BgtaskService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import net.joaf.mainextensions.bgtask.model.Bgtask;

import java.util.List;

/**
 * Created by cyprian on 01.09.14.
 */
@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class ScheduleAllActiveJob implements Job {
	private static final Logger logger = LoggerFactory.getLogger(ScheduleAllActiveJob.class);

	public static final String JOB_NAME = "BGTASK_START_JOB";

	@Autowired
	private BgtaskService bgtaskService;

	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		try {
			logger.error("executed job " + JOB_NAME);
			List<Bgtask> all = bgtaskService.findAll();
			for (Bgtask task : all) {
				if (EBgtaskState.ACTIVE.equals(task.getScheduleState())) {
					bgtaskService.schedule(task.getUid());
				}
			}
		} catch (SchedulerException | ClassNotFoundException | JoafException e) {
			e.printStackTrace();
		}
	}
}
