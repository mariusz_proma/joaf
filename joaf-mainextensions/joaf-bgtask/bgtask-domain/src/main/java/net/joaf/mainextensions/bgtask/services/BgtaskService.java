/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.bgtask.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.core.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.stereotype.Service;
import net.joaf.mainextensions.bgtask.PersistableCronTriggerFactoryBean;
import net.joaf.mainextensions.bgtask.jobs.ScheduleAllActiveJob;
import net.joaf.mainextensions.bgtask.model.Bgtask;
import net.joaf.mainextensions.bgtask.model.enums.EBgtaskState;
import net.joaf.mainextensions.bgtask.model.enums.ETriggerType;
import net.joaf.mainextensions.bgtask.repository.api.BgtaskRepository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by cyprian on 09.08.14.
 */
@Service
public class BgtaskService implements ApplicationContextAware {

	public static final String BGTASK = "BGTASK";
	public static final String BGTASK_START = "BGTASK_START";
	public static final int FIVE_MINUTES = 5;
	@Autowired
	private BgtaskRepository repository;

	@Autowired
	private Scheduler quartzScheduler;

	private ApplicationContext applicationContext;

	@PostConstruct
	public void postConstruct() throws SchedulerException {
		String jobName = ScheduleAllActiveJob.JOB_NAME;
		String groupName = BGTASK_START; // Your Job Group
		Trigger trigger = TriggerBuilder.newTrigger()
				.withIdentity(jobName, groupName)
				.startAt(DateUtils.localDateTimeToDate(LocalDateTime.now().plusMinutes(FIVE_MINUTES)))
				.build();

		JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
		jobDetailFactoryBean.setApplicationContext(applicationContext);
		jobDetailFactoryBean.setApplicationContextJobDataKey("applicationContext");
		jobDetailFactoryBean.setDurability(true);
		jobDetailFactoryBean.setGroup(groupName);
		jobDetailFactoryBean.setJobClass(ScheduleAllActiveJob.class);
		jobDetailFactoryBean.setName(jobName);

		jobDetailFactoryBean.afterPropertiesSet();
		JobDetail jobDetail = jobDetailFactoryBean.getObject();

		quartzScheduler.scheduleJob(jobDetail, trigger);
	}

	public List<Bgtask> findAll() throws JoafDatabaseException, SchedulerException {
		List<Bgtask> all = repository.findAll();
		Set<JobKey> jobKeys = quartzScheduler.getJobKeys(GroupMatcher.jobGroupEquals(BGTASK));
		for (Bgtask el : all) {
			JobKey jobKey = new JobKey(el.getName(), BGTASK);
			if (jobKeys.contains(jobKey)) {
				el.setActive(Boolean.TRUE);
			}
		}
		return all;
	}

	public Bgtask create() throws JoafDatabaseException {
		Bgtask command = new Bgtask();
		command.setUid(UUID.randomUUID().toString());
		command.setScheduleState(EBgtaskState.INACTIVE);
		command.setTriggerType(ETriggerType.CRON);
		repository.insert(command);
		return command;
	}

	public Bgtask findOne(String id) throws JoafDatabaseException {
		return repository.findOne(id);
	}

	public void cancel(String id) throws JoafDatabaseException {
		Bgtask bgtask = repository.findOne(id);
		if (bgtask != null && StringUtils.isEmpty(bgtask.getJobModule())) {
			repository.remove(id);
		}
	}

	public void save(Bgtask command) throws JoafDatabaseException {
		Bgtask dbBgtask = repository.findOne(command.getUid());
		command.setScheduleState(dbBgtask.getScheduleState());
		repository.store(command);
	}

	public void schedule(String id) throws ClassNotFoundException, SchedulerException, JoafException {
		Bgtask dbBgtask = repository.findOne(id);
		if (StringUtils.isEmpty(dbBgtask.getParams().get("cronExpression"))) {
			throw new JoafException("Brak parametru cronExpression");
		}
		dbBgtask.setScheduleState(EBgtaskState.ACTIVE);
		repository.updateScheduleState(id, EBgtaskState.ACTIVE);
		List<String> jobGroupNames = quartzScheduler.getJobGroupNames();
		List<JobExecutionContext> currentlyExecutingJobs = quartzScheduler.getCurrentlyExecutingJobs();
		Set<JobKey> jobKeys = quartzScheduler.getJobKeys(GroupMatcher.jobGroupEquals(BGTASK));
		JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
		jobDetailFactoryBean.setApplicationContext(applicationContext);
		jobDetailFactoryBean.setApplicationContextJobDataKey("applicationContext");
		jobDetailFactoryBean.setDurability(true);
		jobDetailFactoryBean.setGroup(BGTASK);
		jobDetailFactoryBean.setJobClass(Class.forName(dbBgtask.getJobClassName()));
		jobDetailFactoryBean.setName(dbBgtask.getName());

		jobDetailFactoryBean.afterPropertiesSet();
		JobDetail jobDetail = jobDetailFactoryBean.getObject();

		PersistableCronTriggerFactoryBean persistableCronTriggerFactoryBean = new PersistableCronTriggerFactoryBean();
		persistableCronTriggerFactoryBean.setName(dbBgtask.getName() + "Trigger");
		persistableCronTriggerFactoryBean.setJobDetail(jobDetail);
		persistableCronTriggerFactoryBean.setCronExpression(dbBgtask.getParams().get("cronExpression"));
		persistableCronTriggerFactoryBean.setGroup(BGTASK);
		persistableCronTriggerFactoryBean.afterPropertiesSet();
		CronTrigger trigger = persistableCronTriggerFactoryBean.getObject();
		quartzScheduler.scheduleJob(jobDetail, trigger);
/*
		JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setDurability(true);
        jobDetailFactoryBean.setJobClass(Class.forName(dbBgtask.getJobClassName()));
*/

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public void unschedule(String id) throws JoafDatabaseException, SchedulerException {
		Bgtask dbBgtask = repository.findOne(id);
		dbBgtask.setScheduleState(EBgtaskState.INACTIVE);
		repository.updateScheduleState(id, EBgtaskState.INACTIVE);
		JobKey jobKey = new JobKey(dbBgtask.getName(), BGTASK);
		quartzScheduler.deleteJob(jobKey);
	}

	public void remove(String id) throws JoafDatabaseException {
		repository.remove(id);
	}
}
