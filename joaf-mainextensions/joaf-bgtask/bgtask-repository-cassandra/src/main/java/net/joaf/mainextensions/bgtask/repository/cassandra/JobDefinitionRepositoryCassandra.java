/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.bgtask.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.base.db.cassandra.RepositoryCassandra;
import net.joaf.mainextensions.bgtask.model.JobDefinition;
import net.joaf.mainextensions.bgtask.repository.api.JobDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 09.08.14.
 */
@Repository
public class JobDefinitionRepositoryCassandra extends AbstractCrudRepositoryCassandra<JobDefinition> implements JobDefinitionRepository, RepositoryCassandra {

	@Autowired
	private CassandraSessionProvider sessionProvider;

	@Override
	public JobDefinition findByClassName(String className) throws JoafDatabaseException {
		String sql = "SELECT * FROM jobdefinition WHERE className = ?";
		Session session = sessionProvider.connect();
		ResultSet rs = session.execute(sql, className);
		List<Row> all = rs.all();
		if (all.size() > 0) {
			return this.bindRow(all.get(0));
		} else {
			return null;
		}
	}

	protected JobDefinition bindRow(Row row) {
		JobDefinition jobDefinition = new JobDefinition();
		jobDefinition.setUid(row.getUUID("uid").toString());
		jobDefinition.setClassName(row.getString("className"));
		jobDefinition.setModule(row.getString("Module"));
		return jobDefinition;
	}

	@Override
	public List<JobDefinition> findAll() throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		ResultSet rs = session.execute("select * from jobdefinition");
		List<Row> all = rs.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public JobDefinition findOne(String id) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		ResultSet rs = session.execute("select * from jobdefinition where uid = ? ", UUID.fromString(id));
		List<Row> all = rs.all();
		if (all.size() > 0) {
			return this.bindRow(all.get(0));
		} else {
			return null;
		}
	}

	@Override
	public String collectionName() {
		return "jobdefinition";
	}

	@Override
	public Class<JobDefinition> getEntityClass() {
		return JobDefinition.class;
	}

	@Override
	public void store(JobDefinition command) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String sql = "UPDATE jobdefinition SET module= ?, className = ? WHERE uid = ?";
		session.execute(sql, command.getModule(), command.getClassName(), UUID.fromString(command.getUid()));
	}

	@Override
	public void insert(JobDefinition command) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String sql = "INSERT INTO jobdefinition (uid, module, className) VALUES" +
				" (?,?,?)";
		UUID uuid = UUID.fromString(command.getUid());
		session.execute(sql, uuid, command.getModule(), command.getClassName());
	}

	@Override
	public void remove(String id) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String sql = "DELETE FROM jobdefinition WHERE uid = ?";
		session.execute(sql, UUID.fromString(id));
	}

	public String prepareId() {
		return UUID.randomUUID().toString();
	}

}
