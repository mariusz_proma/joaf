/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.notification.consumers;

import net.joaf.mainextensions.notification.helpers.MailHelper;
import org.apache.camel.Consume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by Cyprian.Sniegota on 2014-03-25.
 */
@Component
public class SendEmailDirect {

	@Autowired
	private MailHelper mailHelper;

	@Consume(uri = "direct:sendEmail")
	public void sendEmail(Map<String, Object> data) {
		Map<String, String> model = (Map<String, String>) data.get("model");
		String name = (String) data.get("name");
		String destination = (String) data.get("destination");
		mailHelper.sendContentEmail(model, name, destination);
	}
}
