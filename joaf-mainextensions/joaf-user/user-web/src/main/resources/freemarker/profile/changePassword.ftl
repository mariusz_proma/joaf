<#import "/templates/base.ftl" as page />
<@page.layout "zsio.user.profile">
<#--<@spring.bind "registerForm" />-->
<div class="awidget-head">
<#--${registerForm.email}-->
    <#--<@spring.showErrors "*", "errors" />-->
</div>
<div class="awidget-body">
    <!-- Page title -->
<#--    <div class="page-title text-center">
        <h2><@spring.message 'zsio.user.profile' /></h2>
        <hr />
    </div>-->
    <!-- Page title -->
    <form class="form-horizontal" role="form" action="${rc.contextPath}/profile/changePassword.html" method="post">
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-3 control-label"><@spring.message 'zsio.email.label' /></label>

            <div class="col-lg-8">
                <p class="form-control-static">${command.email}</p>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-3 control-label"><@spring.message 'zsio.password.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.password.label') />
                <@spring.formPasswordInput 'command.password[0]' 'placeholder="${placeholder}" class="form-control" maxlength="100"' />
                <@spring.bind 'command.password'/>
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-3 control-label"><@spring.message 'zsio.repassword.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.repassword.label') />
                <@spring.formPasswordInput 'command.password[1]' 'placeholder="${placeholder}" class="form-control" maxlength="100"' />
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-10">
                <button type="submit" class="btn btn-info"><@spring.message 'zsio.save' /></button>
                <a href="${rc.contextPath}/profile.html" class="btn btn-default"><@spring.message 'zsio.cancel' /></a>
            </div>
        </div>
    </form>
</div>
</@page.layout>