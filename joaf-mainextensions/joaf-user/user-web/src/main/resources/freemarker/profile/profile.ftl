<#import "/templates/base.ftl" as page />
<@page.layout "zsio.user.profile">

<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a data-toggle="tab" href="#home"><@spring.message 'zsio.user.profile.main' /></a></li>
    <li><a data-toggle="tab" href="#account"><@spring.message 'zsio.user.profile.account' /></a></li>

</ul>
<div class="tab-content" id="tabContent">
    <div id="home" class="tab-pane fade in active">
        <#include "profile_main.ftl" />
    </div>
    <div id="account" class="tab-pane fade">
        <#include "profile_account.ftl" />
    </div>
</div>

</@page.layout>