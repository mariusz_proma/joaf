<#import "/templates/base.ftl" as page />
<@page.layout "zsio.user.profile">
<#--<@spring.bind "registerForm" />-->
<div class="awidget-head">
<#--${registerForm.email}-->
    <#--<@spring.showErrors "*", "errors" />-->
</div>
<div class="awidget-body">
    <!-- Page title -->
<#--    <div class="page-title text-center">
        <h2><@spring.message 'zsio.user.profile' /></h2>
        <hr />
    </div>-->
    <!-- Page title -->
    <form class="form-horizontal" role="form" action="${rc.contextPath}/profile/edit.html" method="post">
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-3 control-label"><@spring.message 'zsio.email.label' /></label>

            <div class="col-lg-8">
                <p class="form-control-static">${command.email}</p>

            <#--<#assign placeholder = springMacroRequestContext.getMessage('zsio.password.label') />
            <@spring.formInput 'command.email' 'placeholder="${placeholder}" class="form-control"' />
            <#if spring.status.error>
                <p><@spring.showErrors "<br>", "color:red" /></p>
            </#if>-->
            </div>
        </div>
        <div class="form-group">
            <label for="firstName" class="col-lg-3 control-label"><@spring.message 'zsio.user.firstname' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.user.firstname') />
                <@spring.formInput 'command.firstName' 'placeholder="${placeholder}" class="form-control" maxlength="50"' />
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <label for="lastName" class="col-lg-3 control-label"><@spring.message 'zsio.user.lastname' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.user.lastname') />
                <@spring.formInput 'command.lastName' 'placeholder="${placeholder}" class="form-control" maxlength="50"' />
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-10">
                <button type="submit" class="btn btn-info"><@spring.message 'zsio.save' /></button>
                <a href="${rc.contextPath}/profile.html" class="btn btn-default"><@spring.message 'zsio.cancel' /></a>
            </div>
        </div>
    </form>
</div>
</@page.layout>