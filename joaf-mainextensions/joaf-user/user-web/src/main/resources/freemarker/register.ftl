<#import "/templates/login.ftl" as page />
<@page.layout "xxx">
<#--<@spring.bind "registerForm" />-->
<div class="awidget-head">
<#--${registerForm.email}-->
    <#--<@spring.showErrors "*", "errors" />-->
</div>
<div class="awidget-body">
    <!-- Page title -->
    <div class="page-title text-center">
        <h2><@spring.message 'zsio.user.register.header' /></h2>
        <hr/>
    </div>
    <!-- Page title -->

    <form class="form-horizontal" id="mainForm" role="form" action="${rc.contextPath}/register.html" method="post">
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-3 control-label"><@spring.message 'zsio.email.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.email.label') />
                <@spring.formInput 'registerForm.email' 'placeholder="${placeholder}" class="form-control" maxlength="100"' />
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword1" class="col-lg-3 control-label"><@spring.message 'zsio.password.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.password.label') />
                <@spring.formPasswordInput 'registerForm.password[0]' 'placeholder="${placeholder}" class="form-control" maxlength="100"' />
                <@spring.bind 'registerForm.password'/>
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
    <#--        <div class="col-sm-10 col-sm-offset-2" style="padding-top: 30px;">
                <div class="pwstrength_viewport_progress"></div>
            </div>-->
        <div class="form-group">
            <label for="inputPassword2"
                   class="col-lg-3 control-label"><@spring.message 'zsio.repassword.label' /></label>

            <div class="col-lg-8">
                <#assign placeholder = springMacroRequestContext.getMessage('zsio.repassword.label') />
                <@spring.formPasswordInput 'registerForm.password[1]' 'placeholder="${placeholder}" class="form-control" maxlength="100"' />
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <div class="checkbox">


                    <label for="acceptRegulations">
                        <@spring.formCheckbox 'registerForm.acceptRegulations' 'value="on"' /><@spring.message 'zsio.user.register.agreement.label' />
                    </label>
                    <a target="_blank"
                       href="/site/regulaminy/regulamin-korzystania-z-serwisu"><@spring.message 'zsio.user.register.viewregulation.label' /></a>
                </div>
                <#if spring.status.error>
                    <p><@spring.showErrors "<br>", "color:red" /></p>
                </#if>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-10">
                <button type="submit" class="btn btn-primary"><@spring.message 'zsio.user.register' /></button>
                <a href="${rc.contextPath}/loginPage.html"
                   class="btn btn-default"><@spring.message 'zsio.user.signin' /></a>
            </div>
        </div>
    </form>
</div>
<script>
    App_inactive = function () {
        this.init = function () {
            var options = {};
            options = {
                ui: {
                    container: "#mainForm",
                    showVerdictsInsideProgressBar: true,
                    viewports: {
                        progress: ".pwstrength_viewport_progress"
                    }
                },
                ruleScores: {
                    wordNotEmail: -100,
                    wordLength: -10,
                    wordSimilarToUsername: -100,
                    wordLowercase: 10,
                    wordUppercase: 15,
                    wordOneNumber: 10,
                    wordThreeNumbers: 20,
                    wordOneSpecialChar: 30,
                    wordTwoSpecialChar: 40,
                    wordUpperLowerCombo: 50,
                    wordLetterNumberCombo: 30,
                    wordLetterNumberCharCombo: 39
                },
                rules: {
                    wordNotEmail: false,
                    wordLength: false,
                    wordSimilarToUsername: false,
                    wordLowercase: true,
                    wordUppercase: true,
                    wordOneNumber: true,
                    wordThreeNumbers: true,
                    wordOneSpecialChar: true,
                    wordTwoSpecialChar: true,
                    wordUpperLowerCombo: true,
                    wordLetterNumberCombo: true,
                    wordLetterNumberCharCombo: true
                }
            };
            options.common = {
                debug: true,
                onLoad: function () {

                }
            };
            $('#password0').pwstrength(options);
        };
    };
</script>
</@page.layout>