<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
        class="fa fa-user"></i> ${principal.user.username}<#--<@spring.message 'zsio.main.setting' />--> <b
        class="caret"></b></a>
<ul class="dropdown-menu animated fadeInUp">
<#--<li><a href="${rc.contextPath}/settings.html"><@spring.message 'zsio.main.configuration' /></a></li>-->
    <li><a href="${rc.contextPath}/profile.html"><@spring.message 'zsio.user.profile' /></a></li>
    <li><a href="${rc.contextPath}/logout.html"><@spring.message 'zsio.user.logout' /></a></li>
</ul>