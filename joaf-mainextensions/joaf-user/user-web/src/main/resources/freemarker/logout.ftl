<#import "/templates/login.ftl" as page />
<@page.layout "xxx">
<div class="awidget-head">
</div>
<div class="awidget-body">
    <!-- Page title -->
    <div class="page-title text-center">
        <h2>Logout</h2>
        <hr/>
    </div>
    <!-- Page title -->
    <p><@spring.message 'zsio.user.successlogout' /></p>
    <br/>
    <hr/>
    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10">
            <a href="${rc.contextPath}/loginPage.html" class="btn btn-success">Login</a>
        </div>
    </div>
</div>
</@page.layout>