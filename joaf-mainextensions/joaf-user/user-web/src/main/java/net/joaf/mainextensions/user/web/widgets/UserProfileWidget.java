/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.web.widgets;

import net.joaf.base.extension.Widget;
import net.joaf.base.extension.model.WidgetMAV;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import net.joaf.mainextensions.user.model.BasicUserDetails;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by cyprian on 02.05.14.
 */
@Component
public class UserProfileWidget implements Widget {

	@Override
	public boolean availableForUserAndSubject(String username, String subjectId) {
		return StringUtils.trimToNull(username) != null;
	}

	@Override
	public WidgetMAV process(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		WidgetMAV widgetMAV = new WidgetMAV("freemarker/user/widget/profileWidget.ftl");
		Map<String, Object> dataModel = widgetMAV.getModel();
		//custom model
		Authentication authentication = getAuthentication(request);
		BasicUserDetails principal = (BasicUserDetails) authentication.getPrincipal();
		dataModel.put("principal", principal);
		return widgetMAV;
	}

	private Authentication getAuthentication(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Object spring_security_context = session.getAttribute("SPRING_SECURITY_CONTEXT");
		if (spring_security_context != null && spring_security_context instanceof SecurityContext) {
			SecurityContext securityContext = (SecurityContext) spring_security_context;
			Authentication authentication = securityContext.getAuthentication();
			//            log.debug("found authentication: ["+ authentication +"] ["+authentication.getName()+"]");
			return authentication;
		} else {
			//            log.debug("authentication not found");
			return null;
		}
	}

}
