/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.model;

import net.joaf.mainextensions.user.model.dto.SimpleAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cyprian on 28.03.14.
 */
public class BasicUserDetails implements UserDetails {

	private static final long serialVersionUID = -2210852853894601838L;
	private UserCard user;

	public BasicUserDetails(UserCard u) {
		this.user = u;
	}

	public UserCard getUser() {
		return user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<>();
		for (String role : user.getEffectiveRoles()) {
			authorities.add(new SimpleAuthority(role));
		}
		//            authorities.add(new SimpleAuthority("ROLE_USER"));
		return authorities;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public boolean isEnabled() {
		return true;  //To change body of implemented methods use File | Settings | File Templates.
	}
}
