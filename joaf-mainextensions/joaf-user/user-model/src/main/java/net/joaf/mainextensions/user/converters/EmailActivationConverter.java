/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.converters;

import com.mongodb.DBObject;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.base.core.utils.UniversalJsonConverter;
import org.bson.types.ObjectId;

/**
 * Created by cyprian on 02.01.14.
 */
public class EmailActivationConverter extends UniversalJsonConverter<EmailActivation> {

	@Override
	public DBObject dbObjectFromObject(EmailActivation obj) {
		DBObject dbObject = super.dbObjectFromObject(obj);
		dbObject.put("registerDate", obj.getRegisterDate());
		if (obj.getUid() != null) {
			dbObject.put("_id", new ObjectId(obj.getUid()));
		}
		return dbObject;
	}

	@Override
	public EmailActivation objectFromDBObject(DBObject object, Class<EmailActivation> emailActivationClass) {
		EmailActivation emailActivation = new EmailActivation();
		emailActivation.setConfirmed(Boolean.valueOf(String.valueOf(object.get("activated"))));
		emailActivation.setEmail(String.valueOf(object.get("email")));
		emailActivation.setCode(String.valueOf(object.get("activationCode")));
		//        emailActivation.setConfirmationDate((Date) object.get("activationDate"));
		emailActivation.setOriginalAccountEmail(String.valueOf(object.get("originalAccountEmail")));
		emailActivation.setUid(String.valueOf(object.get("_id")));
		//        emailActivation.setRegisterDate((Date) object.get("registerDate"));
		return emailActivation;
	}
}
