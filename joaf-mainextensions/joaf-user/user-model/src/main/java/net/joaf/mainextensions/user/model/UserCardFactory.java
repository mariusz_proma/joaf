/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.model;

import net.joaf.mainextensions.user.model.dto.RegisterForm;
import net.joaf.mainextensions.user.model.enums.EUserCardStatus;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by cyprian on 23.11.14.
 */
@Component
public class UserCardFactory {

	public UserCard fromRegisterForm(RegisterForm form) {
		UserCard userCard = new UserCard();
		userCard.setUid(UUID.randomUUID().toString());
		userCard.setEmail(form.getEmail());
		userCard.setUsername(form.getEmail());
		ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(512);
		userCard.setPassword(shaPasswordEncoder.encodePassword(form.getPassword()[0], null));
		Set<String> roles = new HashSet<>();
		roles.add("ROLE_USER");
		userCard.setRoles(roles);
		userCard.setEffectiveRoles(roles);
		// if email activation
		userCard.setActive(false);
		userCard.setRegistered(LocalDateTime.now());
		userCard.setDeleted(false);
		userCard.setStatus(EUserCardStatus.INACTIVE);
		return userCard;
	}
}
