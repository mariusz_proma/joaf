/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.user.repository.mysql;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.JongoUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.user.model.RequestResetPassword;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.enums.EUserCardStatus;
import net.joaf.mainextensions.user.repository.api.UserCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import net.joaf.mainextensions.user.model.ERequestResetPasswordState;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by cyprian on 31.01.15.
 */
@Repository
public class UserCardRepositoryMysql extends AbstractRepositoryMysql<UserCard> implements UserCardRepository {

	private static final UserCardRowMapper ROW_MAPPER = new UserCardRowMapper();

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public UserCard findByUsername(String username) throws JoafDatabaseException {
		String query = "SELECT * FROM user WHERE username = ?";
		return jdbcTemplate.queryForObject(query, new Object[] { username }, getRowMapper());
	}

	private static Set<String> unpackSet(String roles) {
		return new HashSet<>(JongoUtils.getPojoMap(roles, String.class).values());
	}

	@Override
	public UserCard findByUid(String id) throws JoafDatabaseException {
		return findOne(id);
	}

	@Override
	public void store(UserCard obj) throws JoafDatabaseException {

	}

	@Override
	public void updatePassword(String uid, String password) throws JoafDatabaseException {

	}

	@Override
	public void storeRequestResetPassword(RequestResetPassword requestResetPassword) throws JoafDatabaseException {

	}

	@Override
	public RequestResetPassword getRequestResetPassword(String uid, ERequestResetPasswordState state) throws JoafDatabaseException {
		return null;
	}

	@Override
	protected String getTableName() {
		return "user";
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<UserCard> getRowMapper() {
		return ROW_MAPPER;
	}

	@Override
	public String collectionName() {
		return getTableName();
	}

	@Override
	public Class<UserCard> getEntityClass() {
		return UserCard.class;
	}

	@Override
	public void updateActivation(String uid, boolean active) throws JoafDatabaseException {

	}

	@Override
	public List<UserCard> findInactive() {
		return null;
	}

	@Override
	public void deprecate(UserCard userCard) {

	}

	@Override
	public void remove(String uid) {

	}

	@Override
	public void insert(UserCard element) throws JoafDatabaseException {
		String SQL = "INSERT INTO user (uid, username, password, firstName, lastName, deleted, email, registered, status," +
				" roles, effectiveRoles, active) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
		jdbcTemplate.update(SQL, element.getUid(), element.getUsername(), element.getPassword(), element.getFirstName(), element.getLastName(),
				element.getDeleted(), element.getEmail(), element.getRegistered(), element.getStatus().toString(),
				packSet(element.getRoles()), packSet(element.getEffectiveRoles()), element.getActive());
	}

	private static String packSet(Set<String> roles) {
		try {
			return JongoUtils.getDbObject(roles).toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static class UserCardRowMapper implements RowMapper<UserCard> {

		@Override
		public UserCard mapRow(ResultSet resultSet, int i) throws SQLException {
			UserCard userCard = new UserCard();
			userCard.setUsername(resultSet.getString("username"));
			userCard.setPassword(resultSet.getString("password"));
			userCard.setStatus(EUserCardStatus.valueOf(resultSet.getString("status")));
			userCard.setUid(resultSet.getString("uid"));
			userCard.setDeleted(resultSet.getBoolean("deleted"));
			userCard.setActive(resultSet.getBoolean("active"));
			userCard.setEmail(resultSet.getString("email"));
			userCard.setRoles(unpackSet(resultSet.getString("roles")));
			userCard.setEffectiveRoles(unpackSet(resultSet.getString("effectiveRoles")));
			return userCard;
		}
	}
}
