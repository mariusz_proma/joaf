package net.joaf.mainextensions.user.repository.mysql;

import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.base.core.error.JoafDatabaseException;
import org.springframework.stereotype.Repository;
import net.joaf.mainextensions.user.repository.api.EmailActivationRepository;

import java.util.List;

/**
 * Created by cyprian on 31.01.15.
 */
@Repository
public class EmailActivationRepositoryMysql implements EmailActivationRepository {
	@Override
	public void store(EmailActivation obj) throws JoafDatabaseException {

	}

	@Override
	public List<EmailActivation> findAll() throws JoafDatabaseException {
		return null;
	}

	@Override
	public EmailActivation findOne(String uid) throws JoafDatabaseException {
		return null;
	}

	@Override
	public String collectionName() {
		return null;
	}

	@Override
	public Class<EmailActivation> getEntityClass() {
		return null;
	}

	@Override
	public void remove(String uid) throws JoafDatabaseException {

	}

	@Override
	public void insert(EmailActivation obj) throws JoafDatabaseException {

	}

	@Override
	public EmailActivation findByEmailAndCode(String email, String code) throws JoafDatabaseException {
		return null;
	}

	@Override
	public String prepareId() {
		return null;
	}
}
