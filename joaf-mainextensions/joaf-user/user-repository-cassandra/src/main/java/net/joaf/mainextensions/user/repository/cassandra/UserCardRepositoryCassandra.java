/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.repository.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.base.db.cassandra.CassandraSessionProvider;
import net.joaf.mainextensions.user.model.ERequestResetPasswordState;
import net.joaf.mainextensions.user.model.RequestResetPassword;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.enums.EUserCardStatus;
import net.joaf.mainextensions.user.repository.api.UserCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by cyprian.sniegota on 28.02.14.
 */

@Repository
public class UserCardRepositoryCassandra extends AbstractCrudRepositoryCassandra<UserCard> implements UserCardRepository {
	@Autowired
	private CassandraSessionProvider sessionProvider;

	@Override
	public UserCard findByUsername(String username) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		ResultSet execute = session.execute("select * from user where username = ? and deleted = false ALLOW FILTERING", username);
		List<Row> all = execute.all();
		if (all.size() == 1) {
			return this.bindRow(all.iterator().next());
		} else {
			return null;
		}
	}

	protected UserCard bindRow(Row row) {
		UserCard user = new UserCard();
		user.setActive(row.getBool("active"));
		user.setEmail(row.getString("email"));
		user.setUid(row.getUUID("uid").toString());
		user.setEffectiveRoles(row.getSet("roles", String.class));
		user.setPassword(row.getString("password"));
		user.setUsername(row.getString("username"));
		user.setRoles(row.getSet("roles", String.class));
		user.setFirstName(row.getString("firstName"));
		user.setLastName(row.getString("lastName"));
		Date registered = row.getDate("registered");
		if (registered != null) {
			user.setRegistered(LocalDateTime.ofInstant(registered.toInstant(), ZoneId.systemDefault()));
		}
		// TODO: teporary
		String status = row.getString("status");
		if (status != null) {
			user.setStatus(EUserCardStatus.valueOf(status));
		}
		user.setDeleted(row.getBool("deleted"));
		return user;
	}

	public UserCard findByUid(String id) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		ResultSet execute = session.execute("select * from user where uid = ?", UUID.fromString(id));
		List<Row> all = execute.all();
		if (all.size() == 1) {
			return this.bindRow(all.get(0));
		} else {
			return null;
		}
	}

	public void store(UserCard obj) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String updateQuery = "UPDATE user SET active = ?, effectiveRoles = ?, email = ?" +
				", groups = ?, password = ?, roles = ?, username = ? "
				+ ", firstName = ?, lastName = ?, status = ?, deleted = ?, registered = ? WHERE uid = ?;";
		if (obj.getStatus() == null) {
			obj.setStatus(EUserCardStatus.ACTIVE);
		}
		if (obj.getRegistered() == null) {
			obj.setRegistered(LocalDateTime.now());
		}
		if (obj.getUid() != null) {
			session.execute(updateQuery, obj.getActive(), obj.getEffectiveRoles(), obj.getEmail(), obj.getGroups()
					, obj.getPassword(), obj.getRoles(), obj.getUsername(), obj.getFirstName(), obj.getLastName(), obj.getStatus().toString(), obj.getDeleted()
					, Date.from(obj.getRegistered().atZone(ZoneId.systemDefault()).toInstant()), UUID.fromString(obj.getUid()));
		} else {
			obj.setUid(UUID.randomUUID().toString());
			insert(obj);
		}
	}

	@Override
	public void insert(UserCard element) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String insertQuery =
				"INSERT INTO user (uid, active, effectiveRoles, email, groups, password, roles, username, firstName, lastName, registered, status, deleted) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? );";
		session.execute(insertQuery, UUID.fromString(element.getUid()), element.getActive(), element.getEffectiveRoles(), element.getEmail(),
				element.getGroups()
				, element.getPassword(), element.getRoles(), element.getUsername(), element.getFirstName(), element.getLastName(),
				Date.from(element.getRegistered().atZone(ZoneId.systemDefault()).toInstant()), element.getStatus().toString(), false);
	}

	public void updatePassword(String uid, String password) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		String updateQuery = "UPDATE user SET password = ? WHERE uid = ?;";
		session.execute(updateQuery, password, UUID.fromString(uid));
	}

	@Override
	public void storeRequestResetPassword(RequestResetPassword requestResetPassword) {
		Session session = sessionProvider.connect();
		String insertQuery = "INSERT INTO requestresetpassword (uid, email, code, state, requestDate) "
				+ "VALUES (?, ?, ?, ?, ?);";
		session.execute(insertQuery, UUID.fromString(requestResetPassword.getUid()), requestResetPassword.getEmail()
				, requestResetPassword.getCode()
				, requestResetPassword.getState().toString(), requestResetPassword.getRequestDate());
	}

	public RequestResetPassword getRequestResetPassword(String uid, ERequestResetPasswordState state) throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		ResultSet rs = session.execute("SELECT * FROM requestresetpassword WHERE uid = ? AND state = ?", UUID.fromString(uid), state.toString());
		List<Row> all = rs.all();
		if (all.size() > 0) {
			Row row = all.get(0);
			RequestResetPassword requestResetPassword = new RequestResetPassword();
			requestResetPassword.setEmail(row.getString("email"));
			requestResetPassword.setUid(row.getUUID("uid").toString());
			return requestResetPassword;
		}
		return null;
	}

	@Override
	public List<UserCard> findAll() throws JoafDatabaseException {
		Session session = sessionProvider.connect();
		ResultSet execute = session.execute("select * from user");
		List<Row> all = execute.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public String collectionName() {
		return "user";
	}

	@Override
	public Class<UserCard> getEntityClass() {
		return UserCard.class;
	}

	@Override
	public void updateActivation(String uid, boolean active) {
		Session session = sessionProvider.connect();
		EUserCardStatus status = active ? EUserCardStatus.ACTIVE : EUserCardStatus.INACTIVE;
		String updateQuery = "UPDATE user SET active = ?, status = ? WHERE uid = ?;";
		session.execute(updateQuery, active, status.name(), UUID.fromString(uid));
	}

	@Override
	public List<UserCard> findInactive() {
		Session session = sessionProvider.connect();
		ResultSet execute = session.execute("select * from user where status = ?", EUserCardStatus.INACTIVE.toString());
		List<Row> all = execute.all();
		return all.stream().map(this::bindRow).collect(Collectors.toList());
	}

	@Override
	public void deprecate(UserCard userCard) {
		userCard.setActive(false);
		userCard.setDeleted(true);
		userCard.setStatus(EUserCardStatus.DELETED);
		Session session = sessionProvider.connect();
		String updateQuery = "UPDATE user SET active = ?, status = ?, deleted = ? WHERE uid = ?;";
		session.execute(updateQuery, userCard.getActive(), userCard.getStatus().toString(), userCard.getDeleted(), UUID.fromString(userCard.getUid()));
	}

	@Override
	public void remove(String uid) {
		Session session = sessionProvider.connect();
		session.execute("delete from user where uid = ?", UUID.fromString(uid));
	}
}
