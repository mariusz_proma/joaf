/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.services;

import net.joaf.base.core.error.JoafDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAttribute;
import org.springframework.security.openid.OpenIDAuthenticationToken;
import net.joaf.mainextensions.user.model.dto.RegisterForm;

import java.util.List;
import java.util.UUID;

/**
 * Created by cyprian on 15.03.14.
 */
public class SecurityOpenIdAuthUserService implements AuthenticationUserDetailsService<OpenIDAuthenticationToken>, UserDetailsService {

	@Autowired
	private SecurityAuthUserService securityAuthUserService;

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserDetails(OpenIDAuthenticationToken token) throws UsernameNotFoundException {
		List<OpenIDAttribute> attributes = token.getAttributes();
		OpenIDAttribute emailAttribute = attributes.stream().filter((x) -> x.getName().equals("email")).reduce((x, y) -> x).orElse(null);
		if (emailAttribute == null) {
			throw new UsernameNotFoundException("No email attribute");
		}
		String email = emailAttribute.getValues().get(0);
		UserDetails userDetails = securityAuthUserService.loadUserByUsername(email);
		if (userDetails == null) {
			RegisterForm registerForm = new RegisterForm();
			registerForm.setAcceptRegulations("1");
			registerForm.setEmail(email);
			UUID uuid = UUID.randomUUID();
			registerForm.setPassword(new String[] { uuid.toString(), uuid.toString() });
			try {
				userService.register(registerForm);
			} catch (JoafDatabaseException e) {
				throw new UsernameNotFoundException("Registration error");
			}
			userDetails = securityAuthUserService.loadUserByUsername(email);
		}
		if (userDetails == null) {
			throw new UsernameNotFoundException("Null userdetails");
		}
		return userDetails;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		throw new UsernameNotFoundException("Can't load user email from url username [" + username + "]");
	}
}
