/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.commands.handlers;

import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.confirmation.services.ConfirmationService;
import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.EventPublisher;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.UserCardFactory;
import net.joaf.mainextensions.user.repository.api.UserCardRepository;
import net.joaf.mainextensions.user.callbacks.UserRegistrationConfirmationCallback;
import net.joaf.mainextensions.user.commands.RegisterUserCommand;
import net.joaf.mainextensions.user.events.UserRegisteredEvent;
import net.joaf.mainextensions.user.services.UserService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cyprian on 22.11.14.
 */
@CommandHandlerComponent
public class RegisterUserCommandHandler implements CommandHandler<RegisterUserCommand, Object> {

	@Autowired
	private UserService userService;

	@Autowired
	private UserCardRepository repository;

	@Autowired
	private UserCardFactory factory;

	@Autowired
	private EventPublisher eventPublisher;

	@Autowired
	private ConfirmationService confirmationService;

	@Autowired
	private MessageSource messageSource;

	@Produce(uri = "direct:sendEmailMessage")
	private ProducerTemplate template = null;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, RegisterUserCommand command) throws JoafException {
		if (!originalBindingResult.hasErrors()) {
			if (userService.checkUsernameRemoved(command.getRegisterForm().getEmail())) {
				originalBindingResult.addError(new FieldError("registerForm", "email",
						messageSource.getMessage("joaf.user.accountremoved.body", new Object[] { }, LocaleContextHolder.getLocale())));
			} else if (userService.checkUsernameExists(command.getRegisterForm().getEmail())) {
				originalBindingResult.addError(new FieldError("registerForm", "email",
						messageSource.getMessage("joaf.email.alreadyexists", new Object[] { }, LocaleContextHolder.getLocale())));
			}
		}
		return originalBindingResult;
	}

	@Override
	public Object execute(RegisterUserCommand command) throws JoafException {
		UserCard userCard = factory.fromRegisterForm(command.getRegisterForm());
		repository.store(userCard);
		EmailActivation emailActivation = confirmationService.create(command.getRegisterForm().getEmail(), UserRegistrationConfirmationCallback.CALLBACK_NAME);
		this.sendEmail(userCard, emailActivation, "registration");

		eventPublisher.post(new UserRegisteredEvent(userCard, command.getAggregateId()));
		//        userService.register(command.getRegisterForm());
		return null;
	}

	private void sendEmail(UserCard user, EmailActivation emailActivation, String content) {
		Map<String, String> model = new HashMap<>();
		this.sendEmail(user, emailActivation, content, model);
	}

	private void sendEmail(UserCard user, EmailActivation emailActivation, String content, Map<String, String> model) {
		model.put("username", user.getUsername());
		model.put("email", user.getEmail());
		if (emailActivation != null) {
			model.put("confirmation_url", confirmationService.buildUrl(emailActivation));
			model.put("reference_uid", emailActivation.getUid());
		}
		this.sendContentEmail(model, content, user.getEmail());
	}

	public void sendContentEmail(Map<String, String> model, String name, String destination) {
		Map<String, Object> mailData = new HashMap<>();
		mailData.put("model", model);
		mailData.put("name", name);
		mailData.put("destination", destination);
		confirmationService.updateMailData(mailData);
		template.sendBody(mailData);
	}
}
