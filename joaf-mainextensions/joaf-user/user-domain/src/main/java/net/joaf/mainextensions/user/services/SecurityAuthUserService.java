/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.services;

import net.joaf.mainextensions.user.exceptions.AccountRemovedException;
import net.joaf.mainextensions.user.exceptions.EmailNotActiveException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import net.joaf.mainextensions.user.model.BasicUserDetails;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.enums.EUserCardStatus;

/**
 * Created with IntelliJ IDEA.
 * User: Cyprian.Sniegota
 * Date: 18.10.13
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */
@Service
public class SecurityAuthUserService implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserCard user = null;
		try {
			user = userService.findOneUserDetails(username);
		} catch (Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
		//if removed
		if (user != null && EUserCardStatus.DELETED.equals(user.getStatus())) {
			throw new AccountRemovedException("Konto zostało usunięte");
		}
		//if not activated
		if (user != null && !user.getActive()) {
			throw new EmailNotActiveException("Email not active");
			//            return null;
		}
		if (user != null) {
			return new BasicUserDetails(user);
		} else {
			return null;
		}
	}
}
