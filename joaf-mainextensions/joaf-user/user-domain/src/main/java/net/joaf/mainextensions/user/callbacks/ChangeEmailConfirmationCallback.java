/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.callbacks;

import net.joaf.mainextensions.confirmation.interfaces.ConfirmationCallback;
import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.confirmation.model.dto.ConfirmationResultDto;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.user.activity.UserActivityEntryProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.repository.api.UserCardRepository;
import net.joaf.mainextensions.user.services.UserService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cyprian on 17.05.14.
 */
@Component
@Qualifier("confirmationCallback")
public class ChangeEmailConfirmationCallback implements ConfirmationCallback {

	public static final String CALLBACK_NAME = "changeEmail";

	@Autowired
	private UserCardRepository userCardRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private UserActivityEntryProducer userActivityEntryProducer;

	@Override
	public ConfirmationResultDto callback(EmailActivation emailActivation, ConfirmationResultDto confirmationResultDto) {
		try {
			UserCard userCard = userCardRepository.findByUsername(emailActivation.getOriginalAccountEmail());
			userCard.setEmail(emailActivation.getEmail());
			userCard.setUsername(emailActivation.getEmail());
			UserCard newUser = userCardRepository.findByUsername(emailActivation.getEmail());
			if (newUser != null) {
				return null;
			}
			//            userCard.setActive(true);
			userCardRepository.store(userCard);
			confirmationResultDto.setRedirectUrl("/");
			confirmationResultDto.setInvalidateSession(Boolean.TRUE);

			Map<String, String> model = new HashMap<>();
			model.put("username", userCard.getUsername());
			model.put("email", userCard.getEmail());
			model.put("old_email", emailActivation.getOriginalAccountEmail());
			userActivityEntryProducer.createChangeEmailConfirm(confirmationResultDto.getRemoteAddr(), userCard);
			userService.sendContentEmail(model, "infoemailchange", emailActivation.getOriginalAccountEmail());
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}

		return confirmationResultDto;
	}

	@Override
	public String getName() {
		return CALLBACK_NAME;
	}
}
