/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.user.admin.web.helpers;

import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.mainextensions.user.model.dto.EUserCardAdminAction;

/**
 * Created by cyprian on 26.04.14.
 */
public class UserCardAdminActionHelper {
	public static WebActionDetails createActionDetails(EUserCardAdminAction action, String uid) {
		WebActionDetails webActionDetails = new WebActionDetails();
		switch (action) {
		case EDIT:
			webActionDetails.setButtonStyle("btn-primary");
			webActionDetails.setActionUrl("/administration/user/edit/" + uid + ".html");
			webActionDetails.setIconName("fa-pencil");
			webActionDetails.setTooltip("joaf.edit");
			break;
		case BLOCK:
			webActionDetails.setButtonStyle("btn-danger");
			webActionDetails.setActionUrl("/administration/user/block/" + uid + ".html");
			webActionDetails.setIconName("fa-trash-o");
			webActionDetails.setTooltip("joaf.tooltip.trash");
			break;
		case ACTIVATE:
			webActionDetails.setButtonStyle("btn-danger");
			webActionDetails.setActionUrl("/administration/user/activate/" + uid + ".html");
			webActionDetails.setIconName("fa-check-circle-o");
			webActionDetails.setTooltip("joaf.user.admin.activate");
			break;
		default:
			webActionDetails = null;
			break;
		}
		return webActionDetails;
	}
}
