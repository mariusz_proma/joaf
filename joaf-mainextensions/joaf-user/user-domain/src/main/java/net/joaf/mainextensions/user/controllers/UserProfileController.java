/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.user.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.base.core.utils.WebFlashUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.dto.EmailChangeForm;
import net.joaf.mainextensions.user.model.dto.PasswordChangeForm;
import net.joaf.mainextensions.user.model.dto.ProfileForm;
import net.joaf.mainextensions.user.activity.UserActivityEntryProducer;
import net.joaf.mainextensions.user.services.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by cyprian on 03.05.14.
 */
@Controller
@RequestMapping(UserProfileController.BASE_URL)
public class UserProfileController {

	public static final String BASE_URL = "/profile";
	public static final String BASE_PATH = "/profile";

	@Autowired
	private UserService userService;

	@Autowired
	private UserActivityEntryProducer userActivityEntryProducer;

	@RequestMapping
	public String viewProfile(Model model, Principal principal) {
		String email = principal.getName();
		ProfileForm profileForm = new ProfileForm();
		try {
			UserCard oneUserDetails = userService.findOneUserDetails(email);
			profileForm.setEmail(StringUtils.trimToEmpty(oneUserDetails.getEmail()));
			profileForm.setFirstName(StringUtils.trimToEmpty(oneUserDetails.getFirstName()));
			profileForm.setLastName(StringUtils.trimToEmpty(oneUserDetails.getLastName()));
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		model.addAttribute("mainprofile", profileForm);
		return getViewTemplate("/profile");
	}

	@RequestMapping(value = "/edit", method = { RequestMethod.GET })
	public String profile(Model model, Principal principal,
			final RedirectAttributes redirectAttributes) {

		String email = principal.getName();
		ProfileForm profileForm = new ProfileForm();
		try {
			UserCard oneUserDetails = userService.findOneUserDetails(email);
			profileForm.setEmail(StringUtils.trimToEmpty(oneUserDetails.getEmail()));
			profileForm.setFirstName(StringUtils.trimToEmpty(oneUserDetails.getFirstName()));
			profileForm.setLastName(StringUtils.trimToEmpty(oneUserDetails.getLastName()));
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		model.addAttribute("command", profileForm);
		return getViewTemplate("/profileEdit");
	}

	@RequestMapping(value = "/edit", method = { RequestMethod.POST })
	public String profileSave(@Valid @ModelAttribute("command") ProfileForm form, HttpServletRequest request
			, BindingResult result, Authentication authentication, Model model,
			final RedirectAttributes redirectAttributes) {
		try {
			//check user exists
			if (!result.hasErrors()) {
			}
			if (result.hasErrors()) {
				//            model.addAllAttributes(result.getModel());
				//                return "activate";
			}
			form.setEmail(authentication.getName());
			userService.updateProfile(form);
			userActivityEntryProducer.createProfileChange(request, authentication);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
			return getRedirect("");
		} catch (JoafDatabaseException e) {
			e.printStackTrace();

		}
		return getViewTemplate("/profileEdit");
	}

	@RequestMapping(value = "/changePassword", method = { RequestMethod.GET })
	public String changePassword(Model model, Principal principal, HttpSession session) {
		String email = principal.getName();
		PasswordChangeForm command = new PasswordChangeForm();
		command.setEmail(email);
		model.addAttribute("command", command);
		return getViewTemplate("/changePassword");
	}

	@RequestMapping(value = "/changePassword", method = { RequestMethod.POST })
	public String changePasswordPost(@Valid @ModelAttribute("command") PasswordChangeForm command, HttpServletRequest request
			, BindingResult result, Model model, Authentication authentication, HttpSession session, final RedirectAttributes redirectAttributes) {
		String email = authentication.getName();
		command.setEmail(email);
		if (result.hasErrors()) {
			return getViewTemplate("/changePassword");
		}
		try {
			userService.changePassword(command);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.profile.successchangepassword");
			userActivityEntryProducer.createChangePassword(request, authentication);
			return getRedirect("");
		} catch (JoafException e) {
			e.printStackTrace();
		}
		return getViewTemplate("/changePassword");
	}

	@RequestMapping(value = "/changeEmail", method = { RequestMethod.GET })
	public String changeEmail(Model model, Principal principal, HttpSession session) {
		String email = principal.getName();
		EmailChangeForm command = new EmailChangeForm();
		command.setOldEmail(email);
		model.addAttribute("command", command);
		return getViewTemplate("/changeEmail");
	}

	@RequestMapping(value = "/changeEmail", method = { RequestMethod.POST })
	public String changeEmailPost(@Valid @ModelAttribute("command") EmailChangeForm command, BindingResult result, HttpServletRequest request
			, Model model, Authentication authentication, HttpSession session, final RedirectAttributes redirectAttributes) {
		String email = authentication.getName();
		command.setOldEmail(email);
		if (result.hasErrors()) {
			return getViewTemplate("/changeEmail");
		}
		try {
			userService.requestChangeEmail(command);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.profile.successrequestchangeemail");
			userActivityEntryProducer.createChangeEmailRequest(request, authentication);
			return getRedirect("");
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		} catch (JoafException e) {
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.profile.emailalreadyexists");
			return getRedirect("/changeEmail");
		}
		return getViewTemplate("/changeEmail");
	}

	@RequestMapping(value = "/removeAccount", method = { RequestMethod.GET })
	public String removeAccount(Model model, Authentication authentication, HttpServletRequest request, HttpSession session,
			final RedirectAttributes redirectAttributes) {
		String email = authentication.getName();
		try {

			userService.requestRemoveAccount(email);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.profile.successrequestremoveaccount");
			userActivityEntryProducer.createRemoveAccountRequest(request, authentication);
			return getRedirect("");
		} catch (JoafDatabaseException e) {
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.profile.failurerequestremoveaccount");
			return getRedirect("/removeAccount");
		}
		//        return getViewTemplate("");
	}

	private String getRedirect(String subPath) {
		return "redirect:" + BASE_URL + "" + subPath;
	}

	private String getViewTemplate(String subPath) {
		return "freemarker" + BASE_PATH + StringUtils.trimToEmpty(subPath);
	}
}
