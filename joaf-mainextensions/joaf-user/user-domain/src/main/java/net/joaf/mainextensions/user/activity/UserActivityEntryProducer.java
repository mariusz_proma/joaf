/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.user.activity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import net.joaf.mainextensions.activity.model.ActivityEntry;
import net.joaf.mainextensions.activity.services.ActivityService;
import net.joaf.mainextensions.user.model.BasicUserDetails;
import net.joaf.mainextensions.user.model.UserCard;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by cyprian on 19.05.14.
 */
@Component
public class UserActivityEntryProducer {
	@Autowired
	private ActivityService activityService;

	private ActivityEntry createRequestAuthentication(HttpServletRequest request, Authentication authentication, String bodyKey) {
		if (authentication == null) {
			return null;
		}
		if (authentication.getPrincipal() == null) {
			return null;
		}
		UserCard userCard = ((BasicUserDetails) authentication.getPrincipal()).getUser();
		return createRequestUserCard(request.getRemoteAddr(), userCard, bodyKey);
	}

	private ActivityEntry createRequestUserCard(String remoteAddr, UserCard userCard, String bodyKey) {
		if (userCard == null) {
			return null;
		}
		ActivityEntry activityEntry = new ActivityEntry();
		activityEntry.setUsername(userCard.getUsername());
		activityEntry.setUserUid(userCard.getUid());
		activityEntry.setBodyKey(bodyKey);
		activityEntry.setBodyParams(Arrays.asList(remoteAddr));
		activityEntry.setCreated(new Date());
		activityService.insert(activityEntry);
		return activityEntry;
	}

	public ActivityEntry createLogin(HttpServletRequest request, Authentication authentication) {
		return createRequestAuthentication(request, authentication, "joaf.activityentry.login.success");
	}

	public ActivityEntry createBadLogin(HttpServletRequest request, UserCard userCard) {
		return createRequestUserCard(request.getRemoteAddr(), userCard, "joaf.activityentry.login.failure");
	}

	public ActivityEntry createLogout(HttpServletRequest request, Authentication authentication) {
		return createRequestAuthentication(request, authentication, "joaf.activityentry.logout.success");
	}

	public ActivityEntry createRegisterRequest(HttpServletRequest request, UserCard userCard) {
		return createRequestUserCard(request.getRemoteAddr(), userCard, "joaf.activityentry.registerform");
	}

	public ActivityEntry createRegisterConfirm(String remoteAddr, UserCard userCard) {
		return createRequestUserCard(remoteAddr, userCard, "joaf.activityentry.registerconfirmation");
	}

	public ActivityEntry createChangePassword(HttpServletRequest request, Authentication authentication) {
		return createRequestAuthentication(request, authentication, "joaf.activityentry.changepassword");
	}

	public ActivityEntry createChangeEmailRequest(HttpServletRequest request, Authentication authentication) {
		return createRequestAuthentication(request, authentication, "joaf.activityentry.changeemail.request");
	}

	public ActivityEntry createRemoveAccountRequest(HttpServletRequest request, Authentication authentication) {
		return createRequestAuthentication(request, authentication, "joaf.activityentry.removeaccount.request");
	}

	public ActivityEntry createProfileChange(HttpServletRequest request, Authentication authentication) {
		return createRequestAuthentication(request, authentication, "joaf.activityentry.changeprofile");
	}

	public ActivityEntry createChangeEmailConfirm(String remoteAddr, UserCard userCard) {
		return createRequestUserCard(remoteAddr, userCard, "joaf.activityentry.changeemail.confirmation");
	}

	public ActivityEntry createRemoveAccountConfirm(String remoteAddr, UserCard userCard) {
		return createRequestUserCard(remoteAddr, userCard, "joaf.activityentry.removeaccount.confirmation");
	}

	public ActivityEntry createResetPasswordConfirm(String remoteAddr, UserCard userCard) {
		return createRequestUserCard(remoteAddr, userCard, "joaf.activityentry.resetpassword.confirmation");
	}
}
