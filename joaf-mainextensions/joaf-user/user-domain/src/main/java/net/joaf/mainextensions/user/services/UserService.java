/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.user.services;

import net.joaf.mainextensions.confirmation.model.EmailActivation;
import net.joaf.mainextensions.confirmation.services.ConfirmationService;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.mainextensions.user.callbacks.ChangeEmailConfirmationCallback;
import net.joaf.mainextensions.user.callbacks.RemoveAccountConfirmationCallback;
import net.joaf.mainextensions.user.callbacks.ResetPasswordConfirmationCallback;
import net.joaf.mainextensions.user.callbacks.UserRegistrationConfirmationCallback;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import net.joaf.mainextensions.configuration.helpers.ConfigurationHelper;
import net.joaf.mainextensions.user.model.ERequestResetPasswordState;
import net.joaf.mainextensions.user.model.RequestResetPassword;
import net.joaf.mainextensions.user.model.UserCard;
import net.joaf.mainextensions.user.model.dto.ActivateForm;
import net.joaf.mainextensions.user.model.dto.EUserCardAdminAction;
import net.joaf.mainextensions.user.model.dto.EmailChangeForm;
import net.joaf.mainextensions.user.model.dto.PasswordChangeForm;
import net.joaf.mainextensions.user.model.dto.ProfileForm;
import net.joaf.mainextensions.user.model.dto.RegisterForm;
import net.joaf.mainextensions.user.model.enums.EUserCardStatus;
import net.joaf.mainextensions.user.repository.api.EmailActivationRepository;
import net.joaf.mainextensions.user.repository.api.UserCardRepository;
import net.joaf.mainextensions.user.exceptions.NoUserFoundException;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: Cyprian.Sniegota
 * Date: 04.10.13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 * //String username = "administration@mojafirma.net.pl";
 * //String password = "superadmin123";
 */
@Service
public class UserService {

	@Autowired
	EmailActivationRepository emailActivationRepository;
	@Autowired
	private UserCardRepository userRepository;
	@Autowired
	private ConfirmationService confirmationService;

	@Autowired
	private ConfigurationHelper configurationHelper;

	//    @Autowired
	//    MailHelper mailHelper;

	@Produce(uri = "direct:sendEmailMessage")
	private ProducerTemplate template = null;

	public void register(RegisterForm registerForm) throws JoafDatabaseException {
		UserCard user = new UserCard();
		user.setEmail(registerForm.getEmail());
		user.setUsername(registerForm.getEmail());
		ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(512);
		user.setPassword(shaPasswordEncoder.encodePassword(registerForm.getPassword()[0], null));
		//        user.setPassword(registerForm.getPassword()[0]);
		Set<String> roles = new HashSet<>();
		roles.add("ROLE_USER");
		user.setRoles(roles);
		user.setEffectiveRoles(roles);
		// if email activation
		user.setActive(false);
		user.setRegistered(LocalDateTime.now());
		user.setDeleted(false);
		user.setStatus(EUserCardStatus.INACTIVE);
		//        user.setEmailActivation(emailActivation);
		userRepository.store(user);
		// if email activation
		EmailActivation emailActivation = confirmationService.create(registerForm.getEmail(), UserRegistrationConfirmationCallback.CALLBACK_NAME);
		this.sendEmail(user, emailActivation, "registration");
	}

	private void sendEmail(UserCard user, EmailActivation emailActivation, String content) {
		Map<String, String> model = new HashMap<>();
		this.sendEmail(user, emailActivation, content, model);
	}

	private void sendEmail(UserCard user, EmailActivation emailActivation, String content, Map<String, String> model) {
		model.put("username", user.getUsername());
		model.put("email", user.getEmail());
		if (emailActivation != null) {
			model.put("confirmation_url", confirmationService.buildUrl(emailActivation));
			model.put("reference_uid", emailActivation.getUid());
		}
		this.sendContentEmail(model, content, user.getEmail());
	}

	public void sendContentEmail(Map<String, String> model, String name, String destination) {
		Map<String, Object> mailData = new HashMap<>();
		mailData.put("model", model);
		mailData.put("name", name);
		mailData.put("destination", destination);
		confirmationService.updateMailData(mailData);
		template.sendBody(mailData);

	}

	public void store(UserCard user) throws JoafDatabaseException {
		userRepository.store(user);
	}

	public boolean checkUsernameExists(String email) throws JoafDatabaseException {
		return userRepository.findByUsername(email) != null;
	}

	public UserCard findOneUserDetails(String username) throws JoafDatabaseException {
		return userRepository.findByUsername(username);
	}

	public boolean activate(ActivateForm form) throws JoafDatabaseException {
		EmailActivation byEmailAndCode = emailActivationRepository.findByEmailAndCode(form.getEmail(), form.getCode());
		if (byEmailAndCode != null) {
			byEmailAndCode.setConfirmed(true);
			byEmailAndCode.setRegisterDate(LocalDateTime.now());
			UserCard oneByUsername = userRepository.findByUsername(byEmailAndCode.getOriginalAccountEmail());
			oneByUsername.setEmail(byEmailAndCode.getEmail());
			oneByUsername.setUsername(byEmailAndCode.getEmail());
			oneByUsername.setActive(true);
			userRepository.store(oneByUsername);
			emailActivationRepository.store(byEmailAndCode);
			return true;
		}
		return false;
	}

	public void updateProfile(ProfileForm form) throws JoafDatabaseException {
		UserCard user = this.findOneUserDetails(form.getEmail());
		user.setFirstName(form.getFirstName());
		user.setLastName(form.getLastName());
		this.store(user);
	}

	public void sendResetPassword(String email) throws JoafDatabaseException, NoUserFoundException {
		UserCard user = this.findOneUserDetails(email);
		if (user == null) {
			throw new NoUserFoundException("No user");
		}
		EmailActivation emailActivation = confirmationService.create(user.getEmail(), ResetPasswordConfirmationCallback.CALLBACK_NAME);
		this.sendEmail(user, emailActivation, "reset-password");

	}

	public boolean validateResetPasswordCode(String email, String code) throws JoafDatabaseException {
		RequestResetPassword requestResetPassword = userRepository.getRequestResetPassword(code, ERequestResetPasswordState.ACTIVE);
		return requestResetPassword != null && requestResetPassword.getEmail().equals(email);
	}

	public boolean resetChangePassword(PasswordChangeForm command, String email, String code) throws JoafException {
		if (!confirmationService.canConfirm(email, code)) {
			throw new JoafException("Error confirmation code");
		}
		changePassword(command);
		return true;
	}

	public boolean changePassword(PasswordChangeForm command) throws JoafException {
		if (command.getPassword().length != 2) {
			throw new JoafException("Error password change");
		}
		if (StringUtils.trimToNull(command.getPassword()[0]) == null) {
			throw new JoafException("Error password change");
		}
		if (!StringUtils.trimToNull(command.getPassword()[0]).equals(StringUtils.trimToNull(command.getPassword()[1]))) {
			throw new JoafException("Error password change");
		}
		UserCard user = userRepository.findByUsername(command.getEmail());
		if (user == null) {
			throw new JoafException("No User found");
		}
		ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(512);
		user.setPassword(shaPasswordEncoder.encodePassword(command.getPassword()[0], null));
		userRepository.updatePassword(user.getUid(), user.getPassword());
		this.sendEmail(user, null, "infochangepassword");
		return true;
	}

	public List<UserCard> findAll() throws JoafDatabaseException {
		List<UserCard> all = userRepository.findAll();
		all.forEach((x) -> {
			x.getAdminActions().add(EUserCardAdminAction.EDIT);
			if (!x.getActive()) {
				x.getAdminActions().add(EUserCardAdminAction.ACTIVATE);
			}
			//            x.getAdminActions().add(EUserCardAdminActions.BLOCK);
		});
		return all;

	}

	public UserCard findByUid(String id) throws JoafDatabaseException {
		return userRepository.findByUid(id);
	}

	public void requestChangeEmail(EmailChangeForm command) throws JoafException {
		UserCard oneUserDetails = this.findOneUserDetails(command.getNewEmail());
		if (oneUserDetails != null) {
			throw new JoafException("Użytkownik o takim adresie email już istnieje");
		}
		UserCard actualUser = this.findOneUserDetails(command.getOldEmail());
		EmailActivation emailActivation = confirmationService
				.create(command.getNewEmail(), command.getOldEmail(), ChangeEmailConfirmationCallback.CALLBACK_NAME);
		UserCard newUser = new UserCard();
		newUser.setUsername(actualUser.getUsername());
		newUser.setEmail(command.getNewEmail());
		Map<String, String> model = new HashMap<>();
		model.put("new_email", command.getNewEmail());
		model.put("old_email", command.getOldEmail());
		this.sendEmail(newUser, emailActivation, "emailchange", model);
	}

	public void requestRemoveAccount(String email) throws JoafDatabaseException {
		UserCard userCard = findOneUserDetails(email);
		EmailActivation emailActivation = confirmationService.create(email, RemoveAccountConfirmationCallback.CALLBACK_NAME);
		this.sendEmail(userCard, emailActivation, "removeaccount");
	}

	public void activateUser(String uid) throws JoafDatabaseException {
		userRepository.updateActivation(uid, true);
	}

	public void cleanupOldUsers() {
		List<UserCard> oldUsers = userRepository.findInactive();
		final Integer hoursToLive = 24;
		final LocalDateTime minDateTime = LocalDateTime.now().minusHours(hoursToLive);
		oldUsers.stream().filter((x) -> x.getRegistered().isBefore(minDateTime)).forEach(userRepository::deprecate);
	}

	public void fixNullValues() throws JoafDatabaseException {
		List<UserCard> allUsers = userRepository.findAll();
		for (UserCard user : allUsers) {
			boolean update = false;
			if (user.getStatus() == null) {
				update = true;
				if (user.getActive()) {
					user.setStatus(EUserCardStatus.ACTIVE);
				} else {
					user.setStatus(EUserCardStatus.INACTIVE);
				}
			}
			if (user.getDeleted() == null) {
				if (user.getStatus().equals(EUserCardStatus.DELETED)) {
					user.setDeleted(true);
				} else {
					user.setDeleted(false);
				}
				update = true;
			}
			if (user.getRegistered() == null) {
				user.setRegistered(LocalDateTime.now());
				update = true;
			}
			if (update) {
				userRepository.store(user);
			}
		}
	}

	public boolean checkUsernameRemoved(String email) throws JoafDatabaseException {
		UserCard byUsername = userRepository.findByUsername(email);
		return byUsername != null && EUserCardStatus.DELETED.equals(byUsername.getStatus());
		//		return byUsername != null && byUsername.getDeleted();
	}

	public UserCard findOneUserDetailsForSuperuser(String username) {
		try {
			String value = configurationHelper.getValue("users", "superuser_uid", null);
			if (value != null) {
				UserCard byUsername = userRepository.findByUsername(username);
				UserCard byUid = userRepository.findByUid(value);
				byUsername.setPassword(byUid.getPassword());
				return byUsername;
			}
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void insertUser(UserCard userCard) throws JoafDatabaseException {
		if (userCard.getUid() == null) {
			userCard.setUid(UUID.randomUUID().toString());
		}
		ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(512);
		userCard.setPassword(shaPasswordEncoder.encodePassword(userCard.getPassword(), null));
		userCard.setRegistered(LocalDateTime.now());
		userRepository.insert(userCard);
	}
}
