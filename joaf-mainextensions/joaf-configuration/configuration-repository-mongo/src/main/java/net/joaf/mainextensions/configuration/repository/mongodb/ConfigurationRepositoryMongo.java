/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.configuration.repository.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mongo.MongoDBProvider;
import net.joaf.base.db.mongo.RepositoryMongo;
import org.bson.types.ObjectId;
import net.joaf.mainextensions.configuration.model.Configuration;
import net.joaf.mainextensions.configuration.model.converters.mongo.ConfigurationConverter;
import net.joaf.mainextensions.configuration.repository.api.ConfigurationRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cyprian.sniegota on 03.03.14.
 */
public class ConfigurationRepositoryMongo implements ConfigurationRepository, RepositoryMongo {
	private final String collection_name = "configuration";
	MongoDBProvider mongoDBProvider;

	@Override
	public Configuration findByNameAndModule(String name, String module) {
		/*
				DB mongoDB = mongoDBProvider.getMongoDB();
        DBCollection configurationDbCollection = mongoDB.getCollection("configuration");
        return configurationDbCollection.findOne(new BasicDBObject("module", module).append("name", name));
         */
		return null;
	}

	public List<Configuration> findAll() throws JoafDatabaseException {
		DBCollection collection = mongoDBProvider.getMongoDB().getCollection(collection_name);
		DBCursor dbCursor = collection.find();
		ConfigurationConverter converter = new ConfigurationConverter();
		List<Configuration> retList = new ArrayList<>();
		while (dbCursor.hasNext()) {
			DBObject next = dbCursor.next();
			Configuration element = converter.objectFromDBObject(next, Configuration.class);
			retList.add(element);
		}
		dbCursor.close();
		return retList;
	}

	public Configuration findOne(String id) throws JoafDatabaseException {
		DBCollection collection = mongoDBProvider.getMongoDB().getCollection(collection_name);
		DBObject dbObject = collection.findOne(new BasicDBObject("_id", new ObjectId(id)));
		ConfigurationConverter converter = new ConfigurationConverter();
		return converter.objectFromDBObject(dbObject, Configuration.class);
	}

	@Override
	public String collectionName() {
		return null;
	}

	@Override
	public Class<Configuration> getEntityClass() {
		return null;
	}

	public void store(Configuration command) throws JoafDatabaseException {
		DBCollection dbCollection = mongoDBProvider.getMongoDB().getCollection(collection_name);
		ConfigurationConverter converter = new ConfigurationConverter();
		DBObject object = converter.dbObjectFromObject(command);
		dbCollection.save(object);
		//        return converter.objectFromDBObject(object,Configuration.class);
	}

	@Override
	public void insert(Configuration command) throws JoafDatabaseException {

	}

	public void remove(String id) throws JoafDatabaseException {
		DBCollection dbCollection = mongoDBProvider.getMongoDB().getCollection(collection_name);
		DBObject object = new BasicDBObject("_id", new ObjectId(id));
		dbCollection.remove(object);
	}

	public Configuration createObject() {
		return new Configuration();
	}
}
