/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.configuration.repository.cassandra;

import com.datastax.driver.core.Row;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.cassandra.AbstractCrudRepositoryCassandra;
import net.joaf.mainextensions.configuration.model.ConditionalConfiguration;
import org.springframework.stereotype.Repository;
import net.joaf.mainextensions.configuration.repository.api.ConditionalConfigurationRepository;

import java.util.Arrays;
import java.util.List;

/**
 * Created by cyprian on 21.12.14.
 */
@Repository
public class ConditionalConfigurationRepositoryCassandra extends AbstractCrudRepositoryCassandra<ConditionalConfiguration>
		implements ConditionalConfigurationRepository {

	@Override
	public void store(ConditionalConfiguration element) throws JoafDatabaseException {

	}

	@Override
	public String collectionName() {
		return null;
	}

	@Override
	public Class<ConditionalConfiguration> getEntityClass() {
		return null;
	}

	@Override
	public void insert(ConditionalConfiguration element) throws JoafDatabaseException {

	}

	@Override
	protected ConditionalConfiguration bindRow(Row row) {
		return null;
	}

	@Override
	public List<ConditionalConfiguration> findAllVersions(String country, String key) {
		ConditionalConfiguration conditionalConfiguration = new ConditionalConfiguration();
		conditionalConfiguration.setUid("fake");
		conditionalConfiguration.setAvailableFrom(null);
		conditionalConfiguration.setAvailableTo(null);
		conditionalConfiguration.setKey("tripsRate");
		conditionalConfiguration.setValue("[{\"engineCapacityMax\":null, \"tripsRate\":\"0.8358\"}]");
		/*engineCapacityMax;
	private BigDecimal tripsRate*/
		return Arrays.asList(conditionalConfiguration);
	}
}
