/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.configuration.repository;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.configuration.model.ConditionalConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import net.joaf.mainextensions.configuration.repository.api.ConditionalConfigurationRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * Created by cyprian on 02.02.15.
 */
@Repository
public class ConditionalConfigurationRepositoryMysql extends AbstractRepositoryMysql<ConditionalConfiguration> implements ConditionalConfigurationRepository {
	private static final ConditionalConfigurationRowMapper ROW_MAPPER = new ConditionalConfigurationRowMapper();
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	protected String getTableName() {
		return "configuration";
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<ConditionalConfiguration> getRowMapper() {
		return ROW_MAPPER;
	}

	@Override
	public void insert(ConditionalConfiguration element) throws JoafDatabaseException {

	}

	@Override
	public void store(ConditionalConfiguration element) throws JoafDatabaseException {

	}

	@Override
	public String collectionName() {
		return getTableName();
	}

	@Override
	public Class<ConditionalConfiguration> getEntityClass() {
		return ConditionalConfiguration.class;
	}

	@Override
	public String prepareId() {
		return UUID.randomUUID().toString();
	}

	@Override
	public List<ConditionalConfiguration> findAllVersions(String country, String key) {
		return null;
	}

	private static class ConditionalConfigurationRowMapper implements RowMapper<ConditionalConfiguration> {

		@Override
		public ConditionalConfiguration mapRow(ResultSet resultSet, int i) throws SQLException {
			ConditionalConfiguration configuration = new ConditionalConfiguration();
			configuration.setUid(resultSet.getString("uid"));
			configuration.setModule(resultSet.getString("module"));
			configuration.setValue(resultSet.getString("value"));
			return configuration;
		}
	}
}