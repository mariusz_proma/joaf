/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.configuration.helpers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import net.joaf.mainextensions.configuration.model.Configuration;
import net.joaf.mainextensions.configuration.repository.api.ConfigurationRepository;

/**
 * Created by cyprian on 01.01.14.
 */
@Component
public class ConfigurationHelper {

	@Autowired
	private ConfigurationRepository repository;

	public String getValue(String module, String name) throws JoafDatabaseException {
		return this.getValue(module, name, ""); //default empty value
	}

	public String getValue(String module, String name, String def) throws JoafDatabaseException {
		Configuration one = repository.findByNameAndModule(name, module);
		if (one != null) {
			return one.getValue();
		} else {
			return def;
		}
	}

	public String getValueNotEmpty(String module, String name) throws JoafException {
		Configuration one = repository.findByNameAndModule(name, module);
		if (one != null) {
			return one.getValue();
		} else {
			throw new JoafException("Configuration not found [" + module + "," + name + "]");
		}
	}

}
