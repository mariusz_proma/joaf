/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.menu;

import net.joaf.base.core.error.JoafDatabaseException;
import org.junit.Assert;
import org.junit.Test;
import net.joaf.mainextensions.menu.helpers.MenuHelper;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.repository.api.MenuElementRepository;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Cyprian.Sniegota on 24.01.14.
 */
public class MenuHelperTest {

	public static class MenuElementRepositoryMock implements MenuElementRepository {
		private Map<String, MenuElement> retMap;

		@Override
		public Map<String, MenuElement> findByMenuName(String menuName) throws JoafDatabaseException {
			return retMap;
		}

		public void setRetMap(Map<String, MenuElement> retMap) {
			this.retMap = retMap;
		}

		@Override
		public void remove(String uid) throws JoafDatabaseException {

		}

		@Override
		public void insert(MenuElement element) throws JoafDatabaseException {

		}

		@Override
		public void store(MenuElement element) throws JoafDatabaseException {

		}

		@Override
		public List<MenuElement> findAll() throws JoafDatabaseException {
			return null;
		}

		@Override
		public MenuElement findOne(String uid) throws JoafDatabaseException {
			return null;
		}

		@Override
		public String collectionName() {
			return null;
		}

		@Override
		public Class<MenuElement> getEntityClass() {
			return null;
		}

		@Override
		public String prepareId() {
			return null;
		}
	}

	@Test
	public void getElementsForRoles() throws JoafDatabaseException {

		Map<String, MenuElement> testMap = new HashMap<>();
		MenuElement menuElement = new MenuElement();
		menuElement.setActive(true);
		menuElement.setAccessRole("ROLE_TEST");
		menuElement.setName("first");
		testMap.put(menuElement.getName(), menuElement);

		menuElement = new MenuElement();
		menuElement.setActive(true);
		menuElement.setAccessRole("ROLE_OTHER");
		menuElement.setName("second");
		testMap.put(menuElement.getName(), menuElement);

		MenuHelper menuHelper = new MenuHelper();
		MenuElementRepositoryMock menuElementRepositoryMock = new MenuElementRepositoryMock();
		menuElementRepositoryMock.setRetMap(testMap);
		menuHelper.setMenuElementRepository(menuElementRepositoryMock);
		List<MenuElement> elementsTreeForMenu = menuHelper.getElementsTreeForMenu("test", Arrays.asList("ROLE_TEST"));
		Assert.assertEquals(1, elementsTreeForMenu.size());
	}
}
