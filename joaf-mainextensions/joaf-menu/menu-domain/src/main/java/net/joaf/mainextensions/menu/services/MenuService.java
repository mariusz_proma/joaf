/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.menu.services;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mongo.MongoDBProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.repository.api.MenuElementRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by Cyprian.Sniegota on 08.01.14.
 */
@Service
public class MenuService {

	private final String collection_name = "menuelements";

	@Autowired
	private MongoDBProvider mongoDBProvider;

	@Autowired
	private MenuElementRepository repository;

	public List<MenuElement> findAll() throws JoafDatabaseException {
		List<MenuElement> all = repository.findAll();
		all = all.stream().sorted((x, y) -> x.getOrdering().compareTo(y.getOrdering())).collect(Collectors.toList());
		return all;
	}

	public MenuElement findOne(String uid) throws JoafDatabaseException {
		return repository.findOne(uid);
	}

	public MenuElement create() throws JoafDatabaseException {
		MenuElement command = new MenuElement();
		command.setUid(UUID.randomUUID().toString());
		repository.insert(command);
		return command;
	}

	public void store(MenuElement command) throws JoafDatabaseException {
		repository.store(command);
	}

	public void remove(String uid) throws JoafDatabaseException {
		repository.remove(uid);
	}

	public MenuElement createObject() {
		return new MenuElement();
	}

}
