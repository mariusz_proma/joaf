/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.menu.helpers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.menu.repository.api.MenuElementRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import net.joaf.mainextensions.menu.model.MenuElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Cyprian.Sniegota on 08.01.14.
 */
@Component
public class MenuHelper {

	private static final Logger logger = LoggerFactory.getLogger(MenuHelper.class);

	@Autowired
	private MenuElementRepository menuElementRepository;

	public List<MenuElement> getElementsTreeForMenu(String menuName, List<String> roles) throws JoafDatabaseException {
		logger.debug("Find menu tree");
		Map<String, MenuElement> menuElementMap = menuElementRepository.findByMenuName(menuName);
		//2nd iteration with full filled map
		List<MenuElement> retList = new ArrayList<>();
		for (MenuElement menuElement : menuElementMap.values()) {
			if (!roles.contains(menuElement.getAccessRole())) {
				continue;
			}
			menuElement.setLevel(0);
			menuElement.setActive(false);
			if (StringUtils.trimToNull(menuElement.getParentName()) != null) {
				menuElement.setLevel(1);
				MenuElement parent = menuElementMap.get(menuElement.getParentName());
				parent.getSubelements().add(menuElement);
			} else {
				retList.add(menuElement);
			}
		}
		retList = retList.stream().sorted((x, y) -> x.getOrdering().compareTo(y.getOrdering())).collect(Collectors.toList());
		return retList;
	}

	public void registerMenuElement(MenuElement menuElement) throws JoafDatabaseException {
		menuElementRepository.store(menuElement);
	}

	public void setMenuElementRepository(MenuElementRepository menuElementRepository) {
		this.menuElementRepository = menuElementRepository;
	}
}
