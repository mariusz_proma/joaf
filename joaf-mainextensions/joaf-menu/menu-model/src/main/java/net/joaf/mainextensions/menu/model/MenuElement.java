/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.menu.model;

import net.joaf.base.core.db.StringUidEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Cyprian.Sniegota
 * Date: 22.10.13
 * Time: 12:33
 * To change this template use File | Settings | File Templates.
 */
public class MenuElement implements Serializable, StringUidEntity {

	private static final long serialVersionUID = 1594179880950258302L;
	private String uid;
	private String url;
	private String name;
	private String displayName;
	private Integer level = 0;
	private Boolean hasSubelements = false;
	private List<MenuElement> subelements = new ArrayList<>();
	private String extraStyleClass;
	private String iconName;
	private Boolean active;
	private String parentName;
	private String accessRole;
	private String menuName;
	private Integer ordering;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Boolean getHasSubelements() {
		return hasSubelements;
	}

	public void setHasSubelements(Boolean hasSubelements) {
		this.hasSubelements = hasSubelements;
	}

	public List<MenuElement> getSubelements() {
		return subelements;
	}

	public void setSubelements(List<MenuElement> subelements) {
		this.subelements = subelements;
	}

	public String getExtraStyleClass() {
		return extraStyleClass;
	}

	public void setExtraStyleClass(String extraStyleClass) {
		this.extraStyleClass = extraStyleClass;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getAccessRole() {
		return accessRole;
	}

	public void setAccessRole(String accessRole) {
		this.accessRole = accessRole;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public Integer getOrdering() {
		return ordering;
	}

	public void setOrdering(Integer ordering) {
		this.ordering = ordering;
	}
}
