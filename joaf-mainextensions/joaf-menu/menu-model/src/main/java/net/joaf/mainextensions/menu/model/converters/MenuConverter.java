/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.menu.model.converters;

import com.mongodb.DBObject;
import net.joaf.base.core.utils.UniversalJsonConverter;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import net.joaf.mainextensions.menu.model.MenuElement;

/**
 * Created by Cyprian.Sniegota on 08.01.14.
 */
public class MenuConverter extends UniversalJsonConverter<MenuElement> {

	@Override
	public MenuElement objectFromDBObject(DBObject object, Class<MenuElement> menuElementClass) {
		MenuElement menuElement = super.objectFromDBObject(object, menuElementClass);
		menuElement.setUid(String.valueOf(object.get("_id")));
		return menuElement;
	}

	@Override
	public DBObject dbObjectFromObject(MenuElement obj) {
		DBObject dbObject = super.dbObjectFromObject(obj);
		if (StringUtils.trimToNull(obj.getUid()) != null) {
			dbObject.put("_id", new ObjectId(obj.getUid()));
		}
		return dbObject;
	}
}
