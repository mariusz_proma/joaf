/*
 * Licensed to the HMail.pl under one or more* contributor license agreements.
 * See the NOTICE file distributed with this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.menu.repository.mysql;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mainextensions.menu.repository.api.MenuElementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import net.joaf.mainextensions.menu.model.MenuElement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by cyprian on 01.02.15.
 */
@Repository
public class MenuElementRepositoryMysql extends AbstractRepositoryMysql<MenuElement> implements MenuElementRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final RowMapper<MenuElement> ROW_MAPPER = new MenuRowMapper();

	@Override
	protected String getTableName() {
		return "menuElement";
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<MenuElement> getRowMapper() {
		return ROW_MAPPER;
	}

	@Override
	public Map<String, MenuElement> findByMenuName(String menuName) throws JoafDatabaseException {
		try {
			Map<String, MenuElement> menuElementMap = new HashMap<>();
			String sql = "select * from menuElement where menuName = ?";
			List<MenuElement> all = jdbcTemplate.query(sql, new Object[] { menuName }, getRowMapper());
			for (MenuElement row : all) {
				menuElementMap.put(row.getName(), row);
			}
			return menuElementMap;
		} catch (Exception e) {
			throw new JoafDatabaseException("Błąd pobierania danych", e);
		}
	}

	@Override
	public void remove(String uid) throws JoafDatabaseException {

	}

	@Override
	public void insert(MenuElement element) throws JoafDatabaseException {
		String query = "INSERT INTO menuElement (uid, name, url, displayName, extraStyleClass, iconName, parentName\n" +
				"  , accessRole, menuName, ordering)" +
				" VALUES (?, ?, ?, ? ,? ,? ,? ,? ,? ,?)";
		jdbcTemplate.update(query, element.getUid(), element.getName(), element.getUrl(), element.getDisplayName()
				, element.getExtraStyleClass(), element.getIconName(), element.getActive(), element.getParentName(), element.getAccessRole()
				, element.getMenuName(), element.getOrdering());
	}

	@Override
	public void store(MenuElement element) throws JoafDatabaseException {
		String query = "UPDATE menuElement SET name =?, url =?, displayName=?, extraStyleClass=?, iconName=?, parentName=? " +
				"  , accessRole=?, menuName=?, ordering=? WHERE uid = ?";
		jdbcTemplate.update(query, element.getName(), element.getUrl(), element.getDisplayName()
				, element.getExtraStyleClass(), element.getIconName(), element.getParentName(), element.getAccessRole()
				, element.getMenuName(), element.getOrdering(), element.getUid());
	}

	@Override
	public String collectionName() {
		return getTableName();
	}

	@Override
	public Class<MenuElement> getEntityClass() {
		return MenuElement.class;
	}

	@Override
	public String prepareId() {
		return UUID.randomUUID().toString();
	}

	private static class MenuRowMapper implements RowMapper<MenuElement> {

		@Override
		public MenuElement mapRow(ResultSet resultSet, int i) throws SQLException {
			MenuElement menuElement = new MenuElement();
			menuElement.setUid(resultSet.getString("uid"));
			menuElement.setName(resultSet.getString("name"));
			menuElement.setAccessRole(resultSet.getString("accessRole"));
			menuElement.setDisplayName(resultSet.getString("displayName"));
			menuElement.setExtraStyleClass(resultSet.getString("extraStyleClass"));
			menuElement.setMenuName(resultSet.getString("menuName"));
			menuElement.setUrl(resultSet.getString("url"));
			menuElement.setOrdering(resultSet.getInt("ordering"));
			return menuElement;
		}
	}
}
