/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.menu.web.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.menu.services.MenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import net.joaf.mainextensions.menu.model.MenuElement;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

//TODO: inspect net.joaf.base.core.lib
//import net.joaf.base.core.lib.helpers.ImportExportHelper;

/**
 * Created by Cyprian.Sniegota on 08.01.14.
 */
@Controller
public class MenuController {

	@Autowired
	private MenuService service;

	//	@Autowired
	//	private ImportExportHelper importExportHelper;

	private static final Logger log = LoggerFactory.getLogger(MenuController.class);

	@RequestMapping("/administration/menu")
	public String list(HttpSession session, Model model) {
		try {
			List<MenuElement> all = service.findAll();
			model.addAttribute("command", all);
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		return "/freemarker/admin/menu/list";
	}

	@RequestMapping(value = "/administration/menu/export")
	public HttpEntity<String> export(HttpSession session) {
		//		try {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(new MediaType("application", "json"));
		header.set("Content-Disposition",
				"attachment; filename=menuelements.json");
		//			String elements = importExportHelper.exportData("menuelements");
		//			return new HttpEntity<>(elements, header);
		//		} catch (JoafDatabaseException e) {
		//			e.printStackTrace();
		//		}
		return null;
	}

	@RequestMapping("/administration/menu/edit/{id}")
	public String newItem(@PathVariable("id") String id, HttpSession session, Model model) {
		MenuElement command = null;
		if (id.equals("0")) {
			command = service.createObject();
		} else {
			try {
				command = service.findOne(id);
			} catch (JoafDatabaseException e) {
				e.printStackTrace();
			}
		}
		model.addAttribute("command", command);

		return "/freemarker/admin/menu/edit";
	}

	@RequestMapping("/administration/menu/create")
	public String create(HttpSession session, Model model) {
		MenuElement command = null;
		try {
			command = service.create();
		} catch (JoafDatabaseException e) {
			log.debug("error", e);
			return "/freemarker/admin/menu/list";
		}
		return "redirect:/administration/menu/edit/" + command.getUid() + "";
	}

	@RequestMapping(value = "/administration/menu/save", method = RequestMethod.POST)
	public String saveItem(@Valid @ModelAttribute("command") MenuElement command, HttpSession session, Model model) {
		try {
			service.store(command);
			return "redirect:/administration/menu.html";
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		return "/freemarker/admin/menu/edit";
	}

	@RequestMapping("/administration/menu/remove/{id}")
	public String remove(@PathVariable("id") String id, HttpSession session, Model model) {
		try {
			service.remove(id);
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		return "redirect:/administration/menu.html";
	}

	@RequestMapping(value = "/administration/menu/cancel", method = RequestMethod.POST)
	public String cancel(HttpSession session, Model model) {
		return "redirect:/administration/menu.html";
	}
}
