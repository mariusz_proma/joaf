/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.menu.web.widgets;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.extension.Widget;
import net.joaf.base.extension.exceptions.WidgetException;
import net.joaf.base.extension.model.WidgetMAV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import net.joaf.mainextensions.menu.helpers.MenuHelper;
import net.joaf.mainextensions.menu.model.MenuElement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by cyprian on 02.05.14.
 */
@Component
public class MainMenuWidget implements Widget {

	private static final Logger logger = LoggerFactory.getLogger(MainMenuWidget.class);

	@Autowired
	MenuHelper menuHelper;

	@Override
	public boolean availableForUserAndSubject(String username, String subjectId) {
		return true;
	}

	@Override
	public WidgetMAV process(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws WidgetException {
		String fileName = "freemarker/menu/widget/mainMenu.ftl";
		WidgetMAV widgetMAV = new WidgetMAV(fileName);
		Map<String, Object> dataModel = widgetMAV.getModel();

		List<MenuElement> menuElements = this.getMenuElements(request);
		dataModel.put("zsio_main_menu", menuElements);

		return widgetMAV;
	}

	private List<MenuElement> getMenuElements(HttpServletRequest request) {
		List<String> roles = new ArrayList<>();
		Authentication authentication = this.getAuthentication(request);
		if (authentication != null) {
			Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
			for (GrantedAuthority grantedAuthority : authorities) {
				roles.add(grantedAuthority.getAuthority());
			}
		}

		List<MenuElement> menuElements = new ArrayList<>();
		try {
			menuElements = menuHelper.getElementsTreeForMenu("Main", roles);
		} catch (JoafDatabaseException e) {
			logger.error("Error getting menu elements [{}]", e.getLocalizedMessage(), e);
		}
		for (MenuElement menuElement : menuElements) {
			if (menuElement.getUrl().equals(request.getPathInfo())) {
				menuElement.setActive(true);
			}
		}
		logger.debug("loaded menu for path: [" + request.getPathInfo() + "] and roles: [" + roles + "]");
		logger.debug(MessageFormat.format("paths: [{0},{1},{2},{3}]", request.getPathInfo(), request.getPathTranslated(),
				request.getContextPath(), request.getServletPath()));
		logger.trace("Menu elements: [" + menuElements + "]");

		return menuElements;
	}

	private Authentication getAuthentication(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Object spring_security_context = session.getAttribute("SPRING_SECURITY_CONTEXT");
		if (spring_security_context != null && spring_security_context instanceof SecurityContext) {
			SecurityContext securityContext = (SecurityContext) spring_security_context;
			Authentication authentication = securityContext.getAuthentication();
			logger.debug("found authentication: [" + authentication + "] [" + authentication.getName() + "]");
			return authentication;
		} else {
			logger.debug("authentication not found");
			return null;
		}
	}
}
