<#import "/templates/base.ftl" as page />
<@page.layout "xxx">


<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
            <a href="${rc.contextPath}/administration/menu/create.html" class="btn btn-primary">Create</a>
            <a href="${rc.contextPath}/administration/menu/export.html" class="btn btn-default">Export</a>
        </div>
        <div class="btn-group">
        ${action_buttons!}
        </div>
        <div class="pull-right">
            <a href="${rc.contextPath}/administration.html" class="btn btn-primary"><i
                    class="fa fa-gear fa-fw"></i> <@spring.message 'zsio.back' /></a>
        </div>
    </div>
    <div class="awidget-body">
        <table class="table table-bordered admin-media ">
            <thead>
            <tr>
                <th>
                    <input type='checkbox' value='check1'/>
                </th>
                <th>Name</th>
                <th>Menu Name</th>
                <th>Url</th>
                <th>Role</th>
                <th>Uid</th>
            </tr>
            </thead>
            <tbody>

                <#list command as element>
                <tr>
                    <td>
                        <input type='checkbox' value='check1'/>
                    </td>
                    <td>${element.displayName}</td>
                    <td>${element.menuName}</td>
                    <td>${element.url}</td>
                    <td>${element.accessRole}</td>
                    <td>${element.uid}</td>
                    <td>

                        <a href="${rc.contextPath}/administration/menu/edit/${element.uid}.html"
                           class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> </a>
                        <a href="${rc.contextPath}/administration/menu/remove/${element.uid}.html"
                           class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> </a>

                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>