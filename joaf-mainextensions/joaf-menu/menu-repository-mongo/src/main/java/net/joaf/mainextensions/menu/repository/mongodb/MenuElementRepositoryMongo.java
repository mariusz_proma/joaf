/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mainextensions.menu.repository.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mongo.MongoDBProvider;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import net.joaf.mainextensions.menu.model.MenuElement;
import net.joaf.mainextensions.menu.model.converters.MenuConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cyprian.Sniegota on 2014-04-01.
 */
public class MenuElementRepositoryMongo {
	private final String collection_name = "menuelements";

	//    @Autowired
	MongoDBProvider mongoDBProvider;

	public List<MenuElement> findAll() throws JoafDatabaseException {
		DBCollection collection = mongoDBProvider.getMongoDB().getCollection(collection_name);
		DBCursor dbCursor = collection.find();
		MenuConverter menuConverter = new MenuConverter();
		List<MenuElement> retList = new ArrayList<>();
		while (dbCursor.hasNext()) {
			DBObject next = dbCursor.next();
			MenuElement element = menuConverter.objectFromDBObject(next, MenuElement.class);
			retList.add(element);
		}
		dbCursor.close();
		return retList;
	}

	public MenuElement findOne(String id) throws JoafDatabaseException {
		DBCollection collection = mongoDBProvider.getMongoDB().getCollection(collection_name);
		DBObject dbObject = collection.findOne(new BasicDBObject("_id", new ObjectId(id)));
		MenuConverter menuConverter = new MenuConverter();
		MenuElement element = menuConverter.objectFromDBObject(dbObject, MenuElement.class);
		element.setUid(dbObject.get("_id").toString());
		return element;
	}

	public MenuElement store(MenuElement command) throws JoafDatabaseException {
		DBCollection dbCollection = mongoDBProvider.getMongoDB().getCollection(collection_name);
		MenuConverter menuConverter = new MenuConverter();
		DBObject object = menuConverter.dbObjectFromObject(command);
		if (StringUtils.trimToNull(command.getUid()) != null) {
			object.put("_id", new ObjectId(command.getUid()));
		}
		object.removeField("id");
		dbCollection.save(object);
		MenuElement retObject = menuConverter.objectFromDBObject(object, MenuElement.class);
		retObject.setUid(object.get("_id").toString());
		return retObject;
	}

	public void remove(String id) throws JoafDatabaseException {
		DBCollection dbCollection = mongoDBProvider.getMongoDB().getCollection(collection_name);
		DBObject object = new BasicDBObject("_id", new ObjectId(id));
		dbCollection.remove(object);
	}

	public MenuElement createObject() {
		return new MenuElement();
	}

}
