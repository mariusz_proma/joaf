<#import "/templates/base.ftl" as page />
<@page.layout "Invoices">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        <#--<a href="/remoteconnections/edit/0.html" class="btn btn-primary">Create</a>-->
            <#--<a href="/invoice/syncInfakt.html" class="btn btn-info">Infakt sync</a>-->
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post"
              action="${rc.contextPath}/administration/menu/save.html">
            <div class="form-group">
                <label class="col-lg-2 control-label">Name</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.name' 'placeholder="Name" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Menu Name</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.menuName' 'placeholder="Menu Name" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Url</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.url' 'placeholder="Url" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Display name</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.displayName' 'placeholder="Display name" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Extra style class</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.extraStyleClass' 'placeholder="Extra style class" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Icon name</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.iconName' 'placeholder="Icon name" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Access Role</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.accessRole' 'placeholder="Access Role" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Parent name</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.parentName' 'placeholder="Parent name" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Ordering</label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.ordering' 'placeholder="Ordering" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <@spring.formHiddenInput 'command.uid' '' />
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}/administration/menu/cancel.html');$(this.form).submit();"
                    class="btn btn-default">Cancel
            </button>
        </form>
    </div>

</div>
</@page.layout>