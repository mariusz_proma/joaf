<div class="awidget full-width">
    <div class="row">
        <div class="col-md-10">
            <div class="awidget-head">
                <h3><@spring.message 'joaf.subjectcard.details.main' /></h3>
            </div>
            <div class="awidget-body">
                <div class="form-group row">
                    <label class="col-lg-3 control-label"><@spring.message 'joaf.subjectcard.accesslevel' /></label>

                    <div class="col-lg-8">
                        <p class="form-control-static"><@spring.message 'joaf.subjectcard.accesslevel.owner' /></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <i class="fa fa-info"></i>
        <@spring.message 'joaf.subjectcard.details.access.info' />
        </div>
    </div>
</div>
