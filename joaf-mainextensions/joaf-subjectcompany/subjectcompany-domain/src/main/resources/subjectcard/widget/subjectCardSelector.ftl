<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-briefcase"></i>
<#if zsio_user_selected_company??>
${zsio_user_selected_company.shortName}
<#else>
    <@spring.message 'joaf.subjectcard.noselected' />
</#if>
<#--<span class="label label-success">5</span> -->
    <b class="caret"></b>
</a>
<!-- Big dropdown menu -->
<div class="dropdown-menu dropdown-big animated fadeInUp">
    <!-- Dropdown menu header -->
    <div class="dropdown-head">
        <span class="dropdown-title"><@spring.message 'joaf.subjectcard.list' /></span>
        <span class="pull-right"><a href="${rc.contextPath}/subjectcard.html"
                                    class="pull-right"><@spring.message 'joaf.subjectcard.listofsubjectcards' /></a></span>
    <#--<span class="pull-right"><a href="${rc.contextPath}/settings.html"><@spring.message 'joaf.main.setting' /></a></span>-->
    </div>
    <!-- Dropdown menu body -->
    <div class="dropdown-body">
        <ul class="list-unstyled">
        <#list zsio_user_subjects as item>
            <li><a href="${rc.contextPath}/subjectcard/switch/${item.subjectcardUid}.html"><i
                    class="icon-briefcase color"></i> ${item.subjectname}</a></li>
        </#list>
        <#if !zsio_user_subjects?has_content>
            <li><a class="" href="${rc.contextPath}/subjectcard/create.html"
                   class=""><@spring.message 'joaf.subjectcard.createnew' /></a></li>
        </#if>
        </ul>
    </div>
    <!-- Dropdown menu footer -->
    <div class="dropdown-foot">
    <#--<a href="${rc.contextPath}/subjectcard.html" class="pull-right"><@spring.message 'joaf.subjectcard.listofsubjectcards' /></a>-->
    </div>
</div>