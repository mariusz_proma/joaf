/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.subjectcompany.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.ModuleBackupContainer;
import net.joaf.base.core.services.JsonBackupService;
import net.joaf.mainextensions.subjectcompany.model.SubjectCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.joaf.mainextensions.subjectcompany.model.SubjectCardCompact;
import net.joaf.mainextensions.subjectcompany.model.SubjectCardSettings;
import net.joaf.mainextensions.subjectcompany.repository.api.SubjectCardRepository;
import net.joaf.mainextensions.subjectcompany.repository.api.SubjectCardSettingsRepository;

import java.util.List;

/**
 * Created by cyprian on 03.11.14.
 */

@Service
public class SubjectCardBackupService implements JsonBackupService {

	@Autowired
	private SubjectCardRepository repository;

	@Autowired
	private SubjectCardSettingsRepository settingsRepository;

	@Override
	public ModuleBackupContainer backupAll() throws JoafDatabaseException {
		List<SubjectCard> all = repository.findAll();
		List<SubjectCardCompact> allCompact = repository.findAllCompact();
		List<SubjectCardSettings> allSettings = settingsRepository.findAll();
		ObjectMapper mapper = new ObjectMapper();
		try {
			ModuleBackupContainer container = new ModuleBackupContainer(getModuleName(), "subjectcard", mapper.writeValueAsString(all));
			container.getData().put("usercard_subjectcard", mapper.writeValueAsString(allCompact));
			container.getData().put("subjectcardsettings", mapper.writeValueAsString(allSettings));
			return container;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ModuleBackupContainer backupSubject(String subjectUid) {
		return null;
	}

	@Override
	public String getModuleName() {
		return "subject";
	}
}
