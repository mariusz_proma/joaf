/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.subjectcompany.web.controllers;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.base.subjectcard.model.SubjectCardData;
import net.joaf.mainextensions.subjectcompany.model.SubjectCard;
import net.joaf.mainextensions.subjectcompany.model.SubjectCardCompact;
import net.joaf.mainextensions.subjectcompany.model.enums.ESubjectCardAction;
import net.joaf.mainextensions.subjectcompany.services.SubjectCardService;
import net.joaf.mainextensions.subjectcompany.web.helpers.SubjectCardControllerHelper;
import net.joaf.mainextensions.user.model.BasicUserDetails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 14.03.14.
 */

@Controller
@RequestMapping("/subjectcard")
public class SubjectCardController {

	private static final Logger logger = LoggerFactory.getLogger(SubjectCardController.class);
	private static final String EXTENSION_BASE_PATH = "/subjectcard";

	@Autowired
	private SubjectCardService service;

	@RequestMapping
	public String list(Map<String, Object> model, Principal principal, HttpSession session) {
		SubjectCardData subjectCard = this.getSubjectCard(session);
		model.put("selected_element", subjectCard);
		try {
			Authentication authentication = (Authentication) principal;
			BasicUserDetails userDetails = (BasicUserDetails) authentication.getPrincipal();
			List<SubjectCardCompact> elements = service.findForUser(userDetails.getUser().getUid());
			List<WrapperWDTO<SubjectCardCompact>> displayList = elements.stream().map(SubjectCardControllerHelper::createWrapper)
					.collect(Collectors.<WrapperWDTO<SubjectCardCompact>>toList());
			displayList.forEach((x) -> {
				x.getWebActions().add(SubjectCardControllerHelper.createActionDetails(ESubjectCardAction.DETAILS, x.getData().getSubjectcardUid()));
				x.getWebActions().add(SubjectCardControllerHelper.createActionDetails(ESubjectCardAction.EDIT, x.getData().getSubjectcardUid()));
				x.getWebActions().add(SubjectCardControllerHelper.createActionDetails(ESubjectCardAction.TRASH, x.getData().getSubjectcardUid()));
				if (subjectCard == null || !subjectCard.getUid().equals(x.getData().getSubjectcardUid())) {
					x.getWebActions().add(SubjectCardControllerHelper.createActionDetails(ESubjectCardAction.SWITCH, x.getData().getSubjectcardUid()));
				}
			});
			model.put("elements", displayList);
		} catch (JoafDatabaseException e) {
			logger.error("Error in controller", e);
		}
		return "freemarker/subjectcard/list";
	}

	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable("id") String id, HttpSession session, Model model) {
		SubjectCard command;
		try {
			command = service.findByUid(id);
			model.addAttribute("command", command);
		} catch (JoafDatabaseException e) {
			logger.error("Error in controller", e);
		}
		return getViewTemplate("/edit");
	}

	@RequestMapping("/remove/{id}")
	public String remove(@PathVariable("id") String id, HttpSession session, Model model, final RedirectAttributes redirectAttributes) {
		try {
			service.remove(id);
		} catch (JoafDatabaseException e) {
			logger.error("Error in controller", e);
		}
		redirectAttributes.addFlashAttribute("message", "Item removed successful");
		return getRedirect("");
	}

	@RequestMapping("/create")
	public String create(HttpSession session, Model model, Principal principal) {
		SubjectCard command;
		try {
			Authentication token = (Authentication) principal;
			BasicUserDetails basicUserDetails = (BasicUserDetails) token.getPrincipal();
			command = service.createNew(basicUserDetails.getUser());
			//            SessionContext sessionContext = SessionContext.loadFromSession(session);
			//            sessionContext.setSubjectCardDataId(command.getUid());
		} catch (JoafDatabaseException e) {
			logger.error("error creating command", e);
			model.addAttribute("error", "basiccustomer.error.savingCustomerForm");
			return this.getRedirect("");
		}
		return this.getRedirect("/edit/" + command.getUid());
	}

	@RequestMapping("/cancel/{id}")
	public String cancel(@PathVariable("id") String id, HttpSession session, Model model) {
		try {
			service.cancel(id);
		} catch (JoafDatabaseException e) {
			logger.error("error cancel subject", e);
		}
		return getRedirect("");
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String save(@PathVariable("id") String id, @Valid @ModelAttribute("command") SubjectCard command, BindingResult result, Model model,
			Principal principal, HttpSession session, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return getViewTemplate("/edit");
		}
		try {
			//            if (true)
			//            throw new JoafDatabaseException("test exception");
			service.save(command);
			SessionContext sessionContext = SessionContext.loadFromSession(session);
			String subjectCardDataId = sessionContext.getSubjectCardDataId();
			if (subjectCardDataId == null) {
				sessionContext.setSubjectCardDataId(command.getUid());
			}
		} catch (JoafDatabaseException e) {
			//            result.addError(new FieldError("command","id","Error saving"));
			//            redirectAttributes.addFlashAttribute("error","basiccustomer.error.1");
			WebFlashUtil.addMessage(redirectAttributes, "joaf.formsaveerror.title", "joaf.subjectcard.formsaveerror.body");
			return getViewTemplate("/edit");
		}
		WebFlashUtil.addMessage(redirectAttributes, "joaf.formsavesuccess.title", "joaf.subjectcard.formsavesuccess.body");
		return getRedirect("/details/" + id + ".html");
	}

	@RequestMapping("/switch/{uid}")
	public String switchSubject(@PathVariable("uid") String uid, @QueryParam(value = "ret") String ret, HttpSession session) {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		if (StringUtils.trimToNull(uid) != null) {
			sessionContext.setSubjectCardDataId(uid);
		}
		List<String> availableReturns = Arrays.asList("subjectcard");
		if (ret != null && availableReturns.contains(ret)) {
			return "redirect:/" + ret + ".html";
		}
		return "redirect:/subjectcard/details/" + uid + ".html";
	}

	@RequestMapping("/details/{id}")
	public String details(@PathVariable("id") String id, HttpSession session, Model model) {
		SubjectCard command;
		try {
			command = service.findByUid(id);
			model.addAttribute("command", command);
		} catch (JoafDatabaseException e) {
			logger.error("Error in controller", e);
		}
		return getViewTemplate("/details");
	}

	public SubjectCardData getSubjectCard(HttpSession session) {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		if (sessionContext != null && sessionContext.getSubjectCardData() != null) {
			return sessionContext.getSubjectCardData();
		}
		return null;
	}

	private String getRedirect(String subPath) {
		return "redirect:" + EXTENSION_BASE_PATH + "" + subPath;
	}

	private String getViewTemplate(String subPath) {
		return "freemarker" + EXTENSION_BASE_PATH + StringUtils.trimToEmpty(subPath);
	}

}
