/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.subjectcompany.services;

import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.subjectcompany.model.SubjectCard;
import net.joaf.mainextensions.user.model.UserCard;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.joaf.mainextensions.subjectcompany.model.SubjectCardCompact;
import net.joaf.mainextensions.subjectcompany.repository.api.SubjectCardRepository;

import java.text.Collator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: Cyprian.Sniegota
 * Date: 21.10.13
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */
@Service
public class SubjectCardService {

	@Autowired
	SubjectCardRepository repository;

	public List<SubjectCardCompact> findForUser(String userUid) throws JoafDatabaseException {
		List<SubjectCardCompact> forUser = repository.findForUser(userUid);
		List<SubjectCardCompact> collect = forUser.stream().filter(SubjectCardCompact::getVisible)
				.sorted((x, y) -> x.getSubjectname().compareTo(y.getSubjectname())).collect(Collectors.toList());
		Collator collator = Collator.getInstance(new Locale("pl", "PL"));
		collator.setStrength(Collator.SECONDARY);
		//TODO: FIXME: sortowanie wg nazwy z polskimi znakami
		//		Collections.sort(collect, collator);
		return collect;
	}

	public SubjectCard findByUid(String uid) throws JoafDatabaseException {
		return repository.findByUid(uid);
	}

	public Set<String> findSubjectRoles(String uid, String userCardId) throws JoafDatabaseException {
		if (uid != null && userCardId != null) {
			SubjectCardCompact compact = repository.findCompactByUid(uid, userCardId);
			if (compact != null) {
				return compact.getUserRoles();
			}
		}
		return new HashSet<>();
	}

	public void save(SubjectCard command) throws JoafDatabaseException {
		SubjectCard dbSubjectcard = this.findByUid(command.getUid());
		if (dbSubjectcard.getObjectState() == null || dbSubjectcard.getObjectState().equals(EObjectState.NEW)) {
			dbSubjectcard.setObjectState(EObjectState.ACCEPTED);
		}
		if (StringUtils.isEmpty(command.getShortName())) {
			command.setShortName(StringUtils.substring(command.getName(), 0, 20));
		}
		command.setObjectState(dbSubjectcard.getObjectState());
		repository.save(command);
	}

	public SubjectCard createNew(UserCard userCard) throws JoafDatabaseException {
		//todo: from configuration
		String defaultCompanyName = "";

		SubjectCard obj = new SubjectCard();
		obj.setObjectState(EObjectState.NEW);
		obj.setUid(repository.prepareId());
		obj.setName(defaultCompanyName);
		repository.insert(obj, userCard);
		return repository.findByUid(obj.getUid());
	}

	public void remove(String id) throws JoafDatabaseException {
		repository.remove(id);
		repository.removeCompact(id);
	}

	public void cancel(String uid) throws JoafDatabaseException {
		SubjectCard subjectCard = this.findByUid(uid);
		if (subjectCard.getObjectState() != null && subjectCard.getObjectState().equals(EObjectState.NEW)) {
			this.remove(uid);
		}
	}

	public List<SubjectCard> findAll() throws JoafDatabaseException {
		return repository.findAll();
	}

	public List<SubjectCard> findAllWithUsers() throws JoafDatabaseException {
		List<SubjectCard> all = repository.findAll();
		for (SubjectCard subjectCard : all) {
			List<SubjectCardCompact> compacts = repository.findCompactForSubject(subjectCard.getUid());
			for (SubjectCardCompact compact : compacts) {
				subjectCard.getAccessUsers().add(compact.getUsername());
			}
		}
		return all;
	}
}
