/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mainextensions.subjectcompany.model;

import net.joaf.base.core.db.StringUidEntity;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by cyprian on 28.03.14.
 */
public class SubjectCardCompact implements Serializable, StringUidEntity {
	private static final long serialVersionUID = -7071845281428735991L;

	private String usercardUid;
	private String subjectcardUid;
	private String username;
	private String subjectname = "";
	private Boolean visible;
	private Set<String> userRoles;

	public String getUid() {
		return usercardUid + "_" + subjectcardUid;
	}

	public void setUid(String uid) {
		//
	}

	public String getUsercardUid() {
		return usercardUid;
	}

	public void setUsercardUid(String usercardUid) {
		this.usercardUid = usercardUid;
	}

	public String getSubjectcardUid() {
		return subjectcardUid;
	}

	public void setSubjectcardUid(String subjectcardUid) {
		this.subjectcardUid = subjectcardUid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSubjectname() {
		return subjectname;
	}

	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}

	public Set<String> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<String> userRoles) {
		this.userRoles = userRoles;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
}
